// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// Old URL
 let baseUrl = "http://139.59.35.232";

// New URL
// let baseUrl = "http://139.59.50.52";

export const environment = {
  production: true,
    apiurls : {
        "isLiveApi" : true,
        "businessserv":     baseUrl + "/v1/business",
        "entityserv" :      baseUrl + "/v1/entity",
        "userserv" :        baseUrl + "/v1/account",
        "authserv" :        baseUrl + "/v1/auth",
        "cardserv" :        baseUrl + "/v1/device",
        "contentserv" :     baseUrl + "/v1/content",
        "swmgmtserv" :      baseUrl + "/",
        "domainserv" :      baseUrl + "/v1/domain",
        "activityserv":     baseUrl + "/v1/account",
        "deviceserv" :      baseUrl + "/v1/asset",
        "checkoutserv":     baseUrl + "/v1/checkout",
        "supportserv" :     baseUrl + "/v1/support",
        "trackingServ" :    baseUrl + "/v1/tracking",
        "mmsauthserv" :     baseUrl + "/v1/auth",
        "mmssupportserv" :  baseUrl + "/v1/support",
        "mmsuserserv" :     baseUrl + "/v1/account",
        "mmsbusinessserv":  baseUrl + "/v1/business",
        "mmsdomainserv" :   baseUrl + "/v1/domain",
        "mmscheckoutserv" : baseUrl + "/v1/checkout",
        "mmsdeviceserv" :   baseUrl + "/v1/asset"
    },
    storeInfo:  {
          "location":{},
          "consumer": {
              "accesstoken" : "",
              "refreshtoken" : "",
              "selectedcardid":"",
              "selectedcardno":""
          },
          "toll":{ //because we dont have purchase without login hence
              // "accesstoken" : "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NjYwNTQ5NTY4OTE3NzMwNjY4IiwiYnVzaW5lc3NQcm9maWxlIjp7IjExNjQ2NzgwNTMwNDI2Nzc5MzYiOjE1fSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTIzNDU2OTM5NzgyNTY5NX0.PxjNyUXyQh06RDx1HbC7x4OsnDyJLpbSQc5dLbdCdgE",
              // "accesstoken" : "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzeXMudXNlciIsImJ1c2luZXNzUHJvZmlsZSI6eyIxMjM3ODYyMzU0MjY1ODIiOjEwfSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTUxMTI1MjU3M30.rSFuLsFdUdMYTX7OelPM-TYFuXPzG-jiXMOdqWE7Kfs",
              "accesstoken" : "",
              "refreshtoken" : "",
              // "refreshtoken" : "4bb4bbf4a28f492c8f03d67adbd766f7",
              "cartid" : "",
              "usertype" : "TOLL-COMPANY",
              "companyaccesstoken" : "",
              "branchaccesstoken" : ""
          },
          "parking":{
              "accesstoken" : "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNjAxODE3NDgzOTY5NjQ1NzY2IiwiYnVzaW5lc3NQcm9maWxlIjp7IjUwOTQ0NzQxNjk4NzIxMTc1NjUiOjE1fSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTIzNDU2OTM5MTE4MzU3NH0.IcO322FZJHfbSodUjjm5UCa27PCobbjTyn0eHZBBxmI",
              "refreshtoken" : "53169500f4e64ec3828e02f03dbd872b",
              "cartid" : "",
              "usertype" : "COMPANY"
          }
      },
};

import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";
import { CommonModal } from "../../../common/modal/modal.component";


@Component({
  templateUrl: './education-reader.component.html',

})

export class EducationReaderComponent {

  device:any;
  id: any;
  deviceinfo: any;
  routeparms :any;
  deviceImages : any;

  @ViewChild(CommonModal)
  private modal : CommonModal;

  constructor(private fullpageService: MnFullpageService, private activatedroute: ActivatedRoute, private route : Router, private commonService : CommonService){
    this.device = {
      "name" : "",
      "images" : []
    };
    this.deviceImages = {};
    this.deviceImages.showImageUrl = "";
    this.initialiseState();
  }

  initialiseState(){
    this.routeparms = this.activatedroute.snapshot.params['id'];
    this.id = this.routeparms ? this.routeparms : "";
    this.getducationDevices();
    console.log("Store info here---------------------------->");
  }

  gotoEducation(page){
    switch(page){
      case 'ABOUTEDUCATION':
        if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
            this.fullpageService.moveTo(1,1);
        }else{
          this.route.navigate(['/education']);
        }
      break;
      case 'FEATURES':
        if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
            this.fullpageService.moveTo(2,1);
        }else{
          this.route.navigate(['/education'],{ queryParams: { sec: 'FEATURES' }});
        }
      break;
      case 'PRODUCTS':
        if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
            this.fullpageService.moveTo(3,1);
        }else{
          this.route.navigate(['/education'],{ queryParams: { sec: 'PRODUCTS' }});
        }
      break;
      case 'FAQ':
        if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
            this.fullpageService.moveTo(4,1);
        }else{
          this.route.navigate(['/education'],{ queryParams: { sec: 'FAQ' }});
        }
      break;
    }
  }

   getducationDevices(){
    this.commonService.apiGetEducationDevicesfromJson()
    .subscribe(
      response => {
        this.deviceinfo = response;
        this.device = this.deviceinfo[this.id];
        this.deviceImages.showImageUrl = this.device.images[0].originalImage;
        console.log("device info");
        console.log(this.device);
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  changeImage(index){
    console.log("index === > " + index);
    this.deviceImages.showImageUrl = this.device.images[index].originalImage;
  }

  ngOnInit() { }
}

import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'education-footer',
  templateUrl: './education-footer.component.html',

})

export class EducationFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoEducationMenu(type){
      switch(type){
        case 'ABOUTEDUCATION':
          if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/education']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/education'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/education'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "EducationGuestComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/education'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

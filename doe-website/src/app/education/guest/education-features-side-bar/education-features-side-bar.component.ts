import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { ParkingService } from "../../../services/parking.service";
import { CommonService } from "../../../services/common.service";


@Component({
  templateUrl: './education-features-side-bar.component.html',
    selector: 'education-features-side-bar'
})

export class EducationFeaturesSideBarComponent{
  // activatedroute : any;
content:any;
  constructor(private commonService: CommonService){
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }
}

import { NgModule }                            from '@angular/core';
import { RouterModule, Routes }                from '@angular/router';
import { EducationGuestComponent }                   from './guest/education-guest.component';
import { EducationReaderComponent }           from './guest/education-reader/education-reader.component';
import { EducationMerchantComponent }         from './guest/education-merchant/education-merchant.component';

const routes: Routes = [
  { path: '',                  component: EducationGuestComponent },
  { path: 'merchant',           component: EducationMerchantComponent },
  { path: 'reader/:id',         component:EducationReaderComponent}

];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class EducationRoutingModule {}

import { NgModule } 					             from '@angular/core';
import { FormsModule }    				         from '@angular/forms';
import { CommonModule } 				           from '@angular/common';
import { MnFullpageModule }                from "ngx-fullpage";

import { DirectivesModule }                from '../directives/directives.module';

import {MerchantModule}                    from '../merchant/merchant.module';

import { EducationRoutingModule } 			   from './education-routing.module';
import { PipesModule}             from '../pipes/pipes.module';

//guest components
import { EducationGuestComponent }               from './guest/education-guest.component';
import { EducationMerchantComponent }            from './guest/education-merchant/education-merchant.component';
import { EducationReaderComponent }              from './guest/education-reader/education-reader.component';

// side menus
import { EducationFeaturesSideBarComponent }     from './guest/education-features-side-bar/education-features-side-bar.component';
import { EducationFooterComponent }              from './guest/education-footer/education-footer.component';


@NgModule({
  imports: [
    CommonModule,
    EducationRoutingModule,
    DirectivesModule,
    FormsModule,
    MerchantModule,
    PipesModule,
    MnFullpageModule.forRoot()
  ],
  declarations: [
    //guest
    EducationGuestComponent,
    EducationFeaturesSideBarComponent,
    EducationReaderComponent,
    EducationMerchantComponent,
    EducationFooterComponent
  ],
  exports: []
})
export class EducationModule {
}

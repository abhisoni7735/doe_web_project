import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }   		from './home/home.component';
import { FindMoreComponent }   		from './find-more/find-more.component';

const routes: Routes = [
  { path: '',component: HomeComponent },
  { path: 'find-more',component: FindMoreComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class GuestRoutingModule {}

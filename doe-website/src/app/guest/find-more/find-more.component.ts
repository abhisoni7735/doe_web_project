import { Component } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';


@Component({
  templateUrl: './find-more.component.html'
})

export class FindMoreComponent {

 content:any;
 tagName:any;
 tagSection:any;

  constructor(private route : Router, private activatedroute: ActivatedRoute, private commonservice: CommonService) {



      this.activatedroute.queryParams.subscribe(routeParams => {
        console.log(routeParams);
    		this.tagName = routeParams.tag ? routeParams.tag : "TIME_TO_CHANGE"
        this.commonservice.getStaticContent()
        .subscribe(
          response =>{
            this.content = response;
            this.getTagsContent();
          },
          error => {
            console.log(error);
          }
        );


    	});


   }

   getTagsContent(){
      this.tagSection = this.content['LANDING_PAGE']['HOME'].find( section => section['TAG'] == this.tagName );
      console.log(this.tagSection);
   }

  ngOnInit() {

  }
}

import { NgModule } 					      from '@angular/core';
import { FormsModule }    				  from '@angular/forms';
import { CommonModule } 				    from '@angular/common';
import { DirectivesModule } 		    from '../directives/directives.module';
import { AgmCoreModule }            from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { CommonBaseModule } 				from '../common/common.module';
import { GuestRoutingModule } 			from './guest-routing.module';

import { HomeComponent } 				     from './home/home.component';
import { FindMoreComponent }         from './find-more/find-more.component';

import { MnFullpageModule } from "ngx-fullpage";





@NgModule({
    imports: [
    	CommonModule,
        GuestRoutingModule,
        CommonBaseModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCaptchaModule,
        DirectivesModule,
        AgmCoreModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
       HomeComponent,
       FindMoreComponent
    ],
    exports: [
        HomeComponent
    ]
})
export class GuestModule {
}

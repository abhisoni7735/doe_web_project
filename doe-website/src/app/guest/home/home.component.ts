import { Component, ViewChild, ElementRef, NgZone,  OnInit,ChangeDetectorRef,
  ChangeDetectionStrategy} from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { GmapService } from '../../services/gmap.service';
import { } from 'googlemaps';
import { TollService } from '../../services/toll.service';
import { DomSanitizer} from '@angular/platform-browser';
import { CustomerService } from '../../services/customer.service';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { ReCaptcha2Component} from 'ngx-captcha'



@Component({
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class HomeComponent {
  @ViewChild('gmap') gmap : any;

 homePage : any;
 lat = 0;
 lng = 0;
 options: any;
 zoom: number;

 storesLocations : any;
 videopopup : boolean;
 videopopupSrc:any;
 videos:any;
 socialIcons:any;
 content:any;
 faq:any;

 @ViewChild("search")
 public searchElementRef: ElementRef;

 @ViewChild('videoPlayer') videoplayer: ElementRef;


 // Joinus form Code starts
 reEnterPassword : any;
 error : any;
 sentOtp : boolean;
 otp : any;
 showRegisterPopup : any;
 userformData : any;
 errobj : any;
 signuperror : any;
 userresponse : any;

joinusForm: FormGroup;

public readonly siteKey = '6LfzIOUUAAAAAIxUSrBW2rN18LaEjw77i_xPF8tK';
public captchaIsLoaded = false;
public captchaSuccess = false;
public captchaIsExpired = false;
public captchaResponse?: string;

public theme: 'light' | 'dark' = 'light';
public size: 'compact' | 'normal' = 'normal';
public lang = 'en';
public type: 'image' | 'audio';
//public useGlobalDomain: boolean = false;

formErrors = {
    'name': '',
    "email": '',
    'textarea': '',
    'mobileNumber': '',
    'clienttype': '',
    'recaptcha': '',
    'location': '',
  };
// Form Error Object
validationMessages = {
 'name': {
   'required': 'Name required',
   'pattern' : 'Enter valid name. Name should contain only characters'
 },
 "email": {
    'pattern' : 'Enter valid mail Id',
    'required':'Please enter email'
  },

 'mobileNumber': {
   'required': 'Mobile Number required',
   'pattern' : 'Enter valid 10 digit mobile number'
 },
 'clienttype': {
   'required' : 'Please Select User Type',
 },
 'recaptcha': {
   'required' : 'Please checkmark the captcha',
 },
 'location': {
   'pattern' : 'Enter Valid location',
 }
};

initForm() {
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
this.joinusForm = this.fb.group({
   name: ['', [Validators.required, Validators.pattern('^([a-zA-Z\s ]{3,32})$')]],
   email:['',[Validators.pattern(re)]],
   mobileNumber: ['',[Validators.required, Validators.pattern('[0-9]{10}')]],
   textarea: [''],
   // location: ['', [Validators.pattern('^([a-zA-Z\s ]{3,32})$')]],
   clienttype: ['CONSUMER'],
   recaptcha: ['', Validators.required],

});


this.joinusForm.valueChanges
  .subscribe(data => this.onValueChanged(data));
this.onValueChanged(); // (re)set validation messages now
}

// Reactive form Error Detection
onValueChanged(data?: any) {
  if (!this.joinusForm) { return; }
    const form = this.joinusForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          let msg = messages[key] ? messages[key] : '';
          this.formErrors[field] += msg + ' ';
        }
      }

    }

  }

onSubmit() {
 if(!this.joinusForm.valid){
   console.log("Form Is not Valid-------->");
   console.log(this.formErrors);
   if (!this.joinusForm) { return; }
   const form = this. joinusForm;
   for (const field in this.formErrors) {
     // clear previous error message (if any)
     this.formErrors[field] = '';
     const control = form.get(field);
     if (control && !control.valid) {
       const messages = this.validationMessages[field];
       for (const key in control.errors) {
         this.formErrors[field] += messages[key] + ' ';
       }
     }
   }
 }
if(this.joinusForm.valid)
{console.log("Form Is Valid-------->");

 console.log(this.formErrors);
  this.userformData = {
    "name" : this.joinusForm.value.name,
    "email" : this.joinusForm.value.email,
    "mobile" : this.joinusForm.value.mobileNumber,
    "userType" : this.joinusForm.value.clienttype,
    "comments" : this.joinusForm.value.textarea,
  }
 this.userRegister();

}

}

@ViewChild('captchaElem') captchaElem: ReCaptcha2Component;

 // Joinus form code ends

  constructor(private fullpageService: MnFullpageService, private commonservice: CommonService, private gmapService : GmapService, private mapsAPILoader: MapsAPILoader,
  private ngZone: NgZone, private tollService : TollService, private sanitizer: DomSanitizer, private route : Router, private activatedroute: ActivatedRoute,
  private cdr: ChangeDetectorRef, private customerservice :CustomerService, private fb: FormBuilder) {

    this.videopopup = false;
    this.faq = {};
    this.videopopupSrc = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/vlDzYIIOYmM");
    this.storesLocations={
      "totalItems" : 0
      }
    this.socialIcons = {
        "twitter" : "",
        "facebook":"",
        "linkedin":""
    };


      this.videos = []

      this.commonservice.getMapStyles()
      .subscribe(
        response =>{
          this.options = response;
        },
        error => {
          console.log(error);
        });
      this.commonservice.getSocialicons()
      .subscribe(
        response =>{
          this.socialIcons = response;
        },
        error => {
          console.log(error);
        });

        this.commonservice.getStaticContent()
        .subscribe(
          response =>{
            this.content = response;
            this.videos = this.content.LANDING_PAGE['VIDEOS'];
            this.cdr.detectChanges();
            this.changefaqbyid('0');
          },
          error => {
            console.log(error);
          });
          this.afterReload = this.afterReload.bind(this)


    this.showRegisterPopup = {};
    this.showRegisterPopup.show = false;

  }

   showVideoPopup(src){
     console.log(src);
     this.videopopup = true;
     this.videopopupSrc = this.sanitizer.bypassSecurityTrustResourceUrl(src);
   }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
      this.zoom = 12;
    });
    }
  }



  redirectTo(tag){
    console.log(tag);
    this.route.navigate(['home','find-more'], { queryParams: { 'tag': tag } });

  }

    ngOnDestroy() {
        this.fullpageService.destroy('all');

        //window.removeEventListener('scroll', this.scroll, true);
    }



    afterReload(origin,destination,direction){

      if(destination == 3){
        console.log("scroll done");
        this.videoplayer.nativeElement.play();
      }
    }

      changefaqbyid (i){
        this.faq.selectedFaqTopic = ( this.content && this.content.LANDING_PAGE.FAQS[i] ) ? this.content.LANDING_PAGE.FAQS[i] : {};
        this.faq.faqs= ( this.content && this.content.LANDING_PAGE.FAQS[i] && this.content.LANDING_PAGE.FAQS[i].QNA) ?  this.content.LANDING_PAGE.FAQS[i].QNA : [];
      }


    searchMethodGo(){
      console.log("Inside the method");
     // let queryparam = "?radius=20&latlng=" + this.lat + "," + this.lng;
      // this.tollService.apiGetMerchantLocations(queryparam).subscribe((response) => {
        this.tollService.jsonGetMerchantLocations().subscribe((response) => {
        console.log(response);
        this.storesLocations = response;
      },
      (error) => {
        console.log(error);
      })
    }




    // joinus form script
    userRegister(){
      //console.log(this.userformData);
      this.customerservice.apiUserRegistration(this.userformData)
      .subscribe((response)=>{
        this.userresponse = response;
        // alert('successfully sent');
        this.showRegisterPopup.show = true;
        this.initForm();
         this.captchaElem.reloadCaptcha();
         this.cdr.detectChanges();
      },(error)=>{
        console.log('here it is error data',this.userformData);
      });
    }


    handleReset(): void {
     this.captchaSuccess = false;
     this.captchaResponse = undefined;
     this.captchaIsExpired = false;
     this.cdr.detectChanges();
   }

   handleSuccess(captchaResponse: string): void {
     this.captchaSuccess = true;
     this.captchaResponse = captchaResponse;
     this.captchaIsExpired = false;
     this.cdr.detectChanges();
   }

   handleLoad(): void {
     this.captchaIsLoaded = true;
     this.captchaIsExpired = false;
     this.cdr.detectChanges();
   }

   handleExpire(): void {
     this.captchaSuccess = false;
     this.captchaIsExpired = true;
     this.cdr.detectChanges();
   }

   ngOnInit() {
     this.initForm();
   }

 }

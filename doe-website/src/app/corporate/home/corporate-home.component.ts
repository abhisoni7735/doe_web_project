import { Component, ViewChild, ElementRef, NgZone, ChangeDetectorRef} from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { GmapService } from '../../services/gmap.service';
import { } from 'googlemaps';
import { TollService } from '../../services/toll.service';
import { DomSanitizer} from '@angular/platform-browser';



@Component({
  templateUrl: './corporate-home.component.html'
})

export class CorporateHomeComponent {
  @ViewChild('gmap') gmap : any;

 homePage : any;
 lat = 0;
 lng = 0;
 options: any;
 zoom: number;

 storesLocations : any;
 videopopup : boolean;
 videopopupSrc:any;
 videos:any;
 socialIcons:any;
 content:any;
 landingData : any;

 @ViewChild("search")
 public searchElementRef: ElementRef;

 @ViewChild('videoPlayer') videoplayer: ElementRef;

  constructor(private fullpageService: MnFullpageService, private commonservice: CommonService, private gmapService : GmapService, private mapsAPILoader: MapsAPILoader,
  private ngZone: NgZone, private tollService : TollService, private sanitizer: DomSanitizer,private ref: ChangeDetectorRef) {

    this.videopopup = false;
    this.videopopupSrc = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/vlDzYIIOYmM");
    this.storesLocations={
      "totalItems" : 0
      }
    this.socialIcons = {
        "twitter" : "",
        "facebook":"",
        "linkedin":""
    };


      this.videos = []

      this.commonservice.getMapStyles()
      .subscribe(
        response =>{
          this.options = response;
        },
        error => {
          console.log(error);
        });
      this.commonservice.getSocialicons()
      .subscribe(
        response =>{
          this.socialIcons = response;
        },
        error => {
          console.log(error);
        });

        this.commonservice.getStaticContent()
        .subscribe(
          response =>{
            this.content = response;
            this.videos = this.content.CORPORATE_LANDING_PAGE['VIDEOS'];
            this.ref.detectChanges();
          },
          error => {
            console.log(error);
          });

   }

   showVideoPopup(src){
     console.log(src);
     this.videopopup = true;
     this.videopopupSrc = this.sanitizer.bypassSecurityTrustResourceUrl(src);
   }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
      this.zoom = 12;
    });
    }
  }

  ngOnInit() {

  }

    ngOnDestroy() {
        this.fullpageService.destroy('all');
        //window.removeEventListener('scroll', this.scroll, true);
    }



    searchMethodGo(){
      console.log("Inside the method");
     // let queryparam = "?radius=20&latlng=" + this.lat + "," + this.lng;
      // this.tollService.apiGetMerchantLocations(queryparam).subscribe((response) => {
        this.tollService.jsonGetMerchantLocations().subscribe((response) => {
        console.log(response);
        this.storesLocations = response;
      },
      (error) => {
        console.log(error);
      })
    }

 }

import { NgModule } 					      from '@angular/core';
import { FormsModule }    				  from '@angular/forms';
import { CommonModule } 				    from '@angular/common';
import { DirectivesModule } 		    from '../directives/directives.module';
import { AgmCoreModule }            from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { CommonBaseModule } 				from '../common/common.module';
import { CorporateRoutingModule } 			from './corporate-routing.module';

import { CorporateHomeComponent } 				     from './home/corporate-home.component';

import { MnFullpageModule } from "ngx-fullpage";





@NgModule({
    imports: [
    	CommonModule,
        CorporateRoutingModule,
        CommonBaseModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCaptchaModule,
        DirectivesModule,
        AgmCoreModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
       CorporateHomeComponent,
    ],
    exports: [
        CorporateHomeComponent
    ]
})
export class CorporateModule {
}

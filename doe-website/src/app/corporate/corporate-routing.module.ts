import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CorporateHomeComponent } 				     from './home/corporate-home.component';

const routes: Routes = [
  { path: '',component: CorporateHomeComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CorporateRoutingModule {}

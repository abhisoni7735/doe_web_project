import { Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import { CommonService } from '../services/common.service';
import { TollService } from '../services/toll.service';



@Component({
  templateUrl: './termsOfServies.component.html'
})

export class TermsOfServicesComponent {

  content:any;

  constructor(private commonService: CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });

   }



 }

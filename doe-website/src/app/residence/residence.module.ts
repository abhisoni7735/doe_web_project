import { NgModule } 					             from '@angular/core';
import { FormsModule }    				         from '@angular/forms';
import { CommonModule } 				           from '@angular/common';
import { DirectivesModule }                from '../directives/directives.module';
import { MnFullpageModule }                from "ngx-fullpage";


import { CommonBaseModule } 				       from '../common/common.module';
import { MerchantModule }                  from '../merchant/merchant.module';
import {PipesModule}                       from '../pipes/pipes.module';

import { ResidenceRoutingModule } 			       from './residence-routing-module';

//guest components
import { ResidenceGuestComponent }             from './guest/residence.component';
import { ResidenceReaderComponent }            from './guest/residence-reader/residence-reader.component';

import { ResidenceFeaturesSideBarComponent }   from './guest/residence-features-side-bar/residence-features-side-bar.component';
import { ResidenceMerchantComponent }          from './guest/residence-merchant/residence-merchant.component';
import { ResidenceFooterComponent }            from './guest/residence-footer/residence-footer.component';



@NgModule({
    imports: [
    	  CommonModule,
        ResidenceRoutingModule,
        DirectivesModule,
        FormsModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        MnFullpageModule.forRoot()

    ],
    declarations: [
      //guest
      ResidenceGuestComponent,
      ResidenceReaderComponent,
      ResidenceMerchantComponent,
      ResidenceFeaturesSideBarComponent,
      ResidenceFooterComponent
    ],
    exports: []
})
export class ResidenceModule {
}

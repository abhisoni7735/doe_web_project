import { Component,ViewChild, HostListener, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { CommonService } from "../../services/common.service";
import { CommonModal } from "./../../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';



@Component({
  templateUrl: './residence.component.html',

})

export class ResidenceGuestComponent {

  residenceLand:any;
  device : any;
  faqInfo: any;
  faqs: any;
  content: any;

  @ViewChild(CommonModal)
  private modal : CommonModal;


  constructor(private fullpageService: MnFullpageService, private commonService: CommonService,private route : Router, private activatedroute: ActivatedRoute){
    this.residenceLand = {};
    this.residenceLand.faqTopics = [];
    this.residenceLand.faqs = [];
    this.residenceLand.faqId = "123";
    this.residenceLand.selectedFaqTopic = {};
    this.residenceLand.selectedFaqTopic.description = "";
    this.residenceLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };
    this.residenceLand.cartinfo = {};
    this.getResidenceDevices();
    this.getFaqInfo();
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }

  getResidenceDevices(){
    this.commonService.apiGetResidenceDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.residenceLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  gotodevice(item){
      this.route.navigate(['/residence/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }

  ngOnInit() {
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUTRESIDENCE":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "FAQS":
          this.fullpageService.moveTo(4,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)
    })
  }

  //FAQ questins and Answers
  getFaqInfo() {
    this.commonService.apiGetFAQInfo("TOLL")
    .subscribe(
      response => {
        this.faqInfo = response;
        this.residenceLand.faqTopics = (this.faqInfo && this.faqInfo.items) ? this.faqInfo.items : [];
        this.residenceLand.selectedFaqTopic = (this.faqInfo.items && this.faqInfo.items[0])? this.faqInfo.items[0]: {};
        this.changefaqbyid('0');
        console.log(response);
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  changefaqbyid (i){
    this.residenceLand.selectedFaqTopic = ( this.residenceLand.faqTopics && this.residenceLand.faqTopics[i] ) ? this.residenceLand.faqTopics[i] : {};
    if(this.residenceLand.selectedFaqTopic.id){
      let topicid = this.residenceLand.selectedFaqTopic.id;
      this.commonService.apiGetFAQs( topicid)
      .subscribe(
        response => {
          this.faqs = response;
          this.residenceLand.faqs= ( this.faqs &&  this.faqs.items) ?  this.faqs.items : [];
          console.log(response);

      },
      error => {
          this.modal.show();
          console.log(error);
      });

    }else{
      console.log("faq not available");
    }

  }

  ngOnDestroy() { this.fullpageService.destroy('all'); }

}

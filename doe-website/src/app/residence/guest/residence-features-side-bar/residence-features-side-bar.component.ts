import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonService } from "../../../services/common.service";

@Component({
  templateUrl: './residence-features-side-bar.component.html',
    selector: 'residence-features-side-bar'
})

export class ResidenceFeaturesSideBarComponent{
  // activatedroute : any;
    content:any;
  constructor(private commonService: CommonService){
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }
}

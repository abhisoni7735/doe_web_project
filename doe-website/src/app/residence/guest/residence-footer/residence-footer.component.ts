import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'residence-footer',
  templateUrl: './residence-footer.component.html',

})

export class ResidenceFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoResidenceMenu(type){
      switch(type){
        case 'ABOUTRESIDENCE':
          if(this.activatedroute.routeConfig.component.name == "ResidenceGuestComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/residence']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "ResidenceGuestComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/residence'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "ResidenceGuestComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/residence'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "ResidenceGuestComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/residence'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

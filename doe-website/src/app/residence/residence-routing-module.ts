import { NgModule }                            from '@angular/core';
import { RouterModule, Routes }                from '@angular/router';

import { ResidenceGuestComponent}               from './guest/residence.component';
import { ResidenceReaderComponent }             from './guest/residence-reader/residence-reader.component';
import { ResidenceMerchantComponent }           from './guest/residence-merchant/residence-merchant.component';



const routes: Routes = [
  { path: '',                   component: ResidenceGuestComponent },
  { path: 'merchant',           component: ResidenceMerchantComponent },
  { path: 'reader/:id',         component:ResidenceReaderComponent}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ResidenceRoutingModule {}

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MerchantHomeComponent }   		from './home/merchant-home.component';

const routes: Routes = [
  { path: '',component: MerchantHomeComponent }
  
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MerchantRoutingModule {}

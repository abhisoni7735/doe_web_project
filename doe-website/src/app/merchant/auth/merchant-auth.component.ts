import { Component }                         from '@angular/core';
import { RouterModule, Routes, Router }      from '@angular/router';

import { CustomerService }                   from '../../services/customer.service';
import { Base64Service }                     from '../../services/base64.service';
import { CommonModal }                       from "../../common/modal/modal.component";
import { TollService }                       from "../../services/toll.service";
import { CommonService }                       from "../../services/common.service"



@Component({
  selector: 'merchant-auth',
  templateUrl: './merchant-auth.component.html',
    providers: [CommonModal]
})

export class MerchantAuthComponent {
     login : any;
     validation : any;
     auth : any;
     merchantuserinfo : any;
     accesstoken : any;
     tokenid : any;
     serverError : any;
  constructor(private tollService :TollService,private commonservice:CommonService,private customerservice :CustomerService, private base64 : Base64Service, private route : Router, private modal: CommonModal) {

    this.login = {};
    this.validation = {};
    this.validation.passwordError = false;
    this.validation.userTypeError = false;
    this.validation.message="";
    let url = this.route.url;
    let index = url.split("/");
    console.log(index);
    let auth;
    switch (index[1]) {
      case 'toll' :
        auth = this.tollService.getStoreData('TOLL');
        break;
    };
    let accessToken = auth.accesstoken;
    if(accessToken){
      let str = accessToken.split('.');
      let decodestr = str[1];
      let userinfo = this.base64.decode(decodestr);
      console.log(userinfo);
      let s = userinfo.replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");

      s = s.replace(/[\u0000-\u0019]+/g,"");
      let obj = JSON.parse(s);
      this.redirect(obj, auth);
    }


}
userLogin(){
    this.validation.passwordError = false;
    this.validation.userTypeError = false;
    this.commonservice.apiMerchantlogin(this.login)
    .subscribe(response =>{
      this.validation.passwordError = false;
      // console.timeEnd(response);
      this.auth = response;
      let accessToken = this.auth.accessToken;
      let str = accessToken.split('.');
      let decodestr = str[1];
      let userinfo = this.base64.decode(decodestr);
      console.log(userinfo);
      let s = userinfo.replace(/\\n/g, "\\n")
         .replace(/\\'/g, "\\'")
         .replace(/\\"/g, '\\"')
         .replace(/\\&/g, "\\&")
         .replace(/\\r/g, "\\r")
         .replace(/\\t/g, "\\t")
         .replace(/\\b/g, "\\b")
         .replace(/\\f/g, "\\f");

      s = s.replace(/[\u0000-\u0019]+/g,"");
      let obj = JSON.parse(s);
      this.redirect(obj, this.auth);
      //this.setData(obj,response);
      //this.updateStore(this.auth, "TOLL-COMPANY", "TOLL");

    }, error =>{
        console.log(error);
        this.serverError = error.error.message;
         if (error.status == '400'){
          this.validation.passwordError = true;

        }
    });

}

redirect(obj, auth){
  switch(obj.userType){
      case "CONSUMER" :
        this.route.navigate(['/user',obj.sub]);
      break;
      case "BUSINESS" :
        //this.route.navigate(['/user',obj.sub]);
        let userId = obj.sub;
        let accesstoken = auth.accessToken ? auth.accessToken : auth.accesstoken;
        this.tollService.apiGetMerchantUserinfo(userId, accesstoken)
        .subscribe( response =>{
            this.merchantuserinfo = response;
            console.log("In business case -------------->");
            console.log(this.merchantuserinfo);
            let dbType = (this.merchantuserinfo && (this.merchantuserinfo.businessProfile) && this.merchantuserinfo.businessProfile && this.merchantuserinfo.businessProfile.businessLevel && this.merchantuserinfo.businessProfile.businessLevel.fullyQualifiedType) ? this.merchantuserinfo.businessProfile.businessLevel.fullyQualifiedType : "";
            let businessProfileId = Object.keys(obj.businessProfile)[0];
            console.log("businessProfileId : " + businessProfileId);
            switch (dbType) {
                case "TOLL-COMPANY":
                case "TOLL-CONTRACTOR" :
                case "MULTIPLE-CONTRACTOR" :
                case "MULTIPLE-COMPANY" :
                    this.updateStore(auth , dbType, "TOLL" );
                    this.route.navigate(['/toll/company', businessProfileId]);
                break;
                case "TOLL-COMPANY-BRANCH" :
                case "TOLL-COMPANY-TOLL_PLAZA" :
                case "TOLL-CONTRACTOR-TOLL_PLAZA" :
                  this.updateStore(auth , dbType, "TOLL" );
                    this.route.navigate(['/toll/branch', businessProfileId]);
                break;
            }
        }, error =>{
            console.log(error);
        });
      break;
      case "DOE_ADMIN" :
            console.log("Doe-Admin Case -------------->");
           console.log(obj);
       // this.route.navigate(['/user',obj.sub]);
      break;
      default:
        console.log(obj);
      break;
  }

}
updateStore (auth, type , service){
  let data = {
    "accesstoken" : auth.accessToken ? auth.accessToken : auth.accesstoken ,
    "refreshtoken" : auth.refreshToken ? auth.refreshToken : auth.refreshtoken,
    "cartid" : "",
    "usertype" : type
  };
  this.tollService.setStoreData(data, service);
}
redirectToDB (){
    let url = this.route.url;
    let index = url.split("/");
    console.log(index);
    let storeData = "";
    switch (index[1]){
      case "toll" :
         storeData = this.tollService.getStoreData("TOLL");
      break;
      case "parking" :
         storeData = this.tollService.getStoreData("PARKING");
      break;

    default :
         storeData = "";
    break;
    }
      this.tokenid = storeData;
    if(this.tokenid.accesstoken){
          let accessToken = this.tokenid.accesstoken;
          let str = accessToken.split('.');
          let decodestr = str[1];
          let userinfo = this.base64.decode(decodestr);
          console.log(userinfo);
          let s = userinfo.replace(/\\n/g, "\\n")
             .replace(/\\'/g, "\\'")
             .replace(/\\"/g, '\\"')
             .replace(/\\&/g, "\\&")
             .replace(/\\r/g, "\\r")
             .replace(/\\t/g, "\\t")
             .replace(/\\b/g, "\\b")
             .replace(/\\f/g, "\\f");

          s = s.replace(/[\u0000-\u0019]+/g,"");
          let obj = JSON.parse(s);
          console.log(obj);
          let auth = {
            "accessToken" : this.tokenid.accesstoken,
            "refreshToken" : this.tokenid.refreshtoken
          };
          this.redirect(obj, auth);
    }
}

  ngOnInit() { }
  scrollToLocation() { }
}

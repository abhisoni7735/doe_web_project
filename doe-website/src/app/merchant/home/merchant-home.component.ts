import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../services/common.service";



@Component({
  templateUrl: './merchant-home.component.html'
})

export class MerchantHomeComponent {
  content:any;


  constructor(private fullpageService: MnFullpageService,private commonService: CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

  ngOnDestroy() {
      this.fullpageService.destroy('all');
      //window.removeEventListener('scroll', this.scroll, true);
  }

}

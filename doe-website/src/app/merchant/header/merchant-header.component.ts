import { Component }                         from '@angular/core';
import { Observable }                        from 'rxjs/Observable';
import { RouterModule, Routes, Router }      from '@angular/router';
import { CommonService }                     from "../../services/common.service";
import { Base64Service }                     from '../../services/base64.service';


@Component({
  selector : 'merchant-header',
  templateUrl: './merchant-header.component.html'
})

export class MerchantHeaderComponent {
    merchant : any;

  constructor(private commonservice:CommonService, private base64 : Base64Service, private route : Router) {
    //this.activatedroute = activatedroute;
    this.merchant = {};
    this.merchant.name = "TOLL"
    this.setMerchantName();
  }

  setMerchantName (){
    let url = this.route.url;
      let urlIndex = url.split("/");
      let baseUrlIndex = urlIndex[1].split("?");
      switch (baseUrlIndex[0]){
        case "toll" :
          this.merchant.name = "TOLL";
        break;

        case "parking" :
          this.merchant.name = "PARKING";
        break;

        case "retail" :
          this.merchant.name = "RETAIL";
        break;

        case "metro" :
          this.merchant.name = "METRO";
        break;

        case "railways" :
          this.merchant.name = "RAILWAYS";
        break;

        case "bus" :
          this.merchant.name = "BUS";
        break;

        case "education" :
          this.merchant.name = "EDUCATION";
        break;

        case "residence" :
          this.merchant.name = "RESIDENCE";
        break;
        case "office" :
          this.merchant.name = "OFFICE";
        break;
        case "access" :
          this.merchant.name = "ACCESS";
        break;
        case "loyality" :
          this.merchant.name = "LOYALITY";
        break;

      }
  }

  ngOnInit() { }

}

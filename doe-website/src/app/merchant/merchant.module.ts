import { NgModule }                   from '@angular/core';
import { FormsModule }                from '@angular/forms';
import { CommonModule }               from '@angular/common';

import { CommonBaseModule }         from '../common/common.module';

import { DirectivesModule }           from './../directives/directives.module';

import { MerchantHomeComponent }       from './home/merchant-home.component';
import { MerchantHeaderComponent }      from './header/merchant-header.component';
import { MerchantAuthComponent }      from './auth/merchant-auth.component';

import { MerchantRoutingModule }     from './merchant-routing.module';
import { MnFullpageModule } from "ngx-fullpage";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DirectivesModule,
        MerchantRoutingModule,
        CommonBaseModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
       MerchantHomeComponent,
       MerchantHeaderComponent,
       MerchantAuthComponent
    ],
    exports: [
      MerchantHeaderComponent,
      MerchantAuthComponent
    ]
})
export class MerchantModule {
}

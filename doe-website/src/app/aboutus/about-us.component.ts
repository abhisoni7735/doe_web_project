import { Component } from '@angular/core';
import { CommonService } from "../services/common.service";


@Component({
  templateUrl: './about-us.component.html'
})

export class AboutUsComponent {



devices:any;
content:any;

  constructor(private commonService: CommonService) {
    this.devices = {};
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
    this.commonService.apiGetDevicesfromJson()
    .subscribe(
      response =>{
        this.devices = response;
        console.log(this.devices);
      },
      error => {
        console.log(error);
      });

   }

  ngOnInit() { }
}

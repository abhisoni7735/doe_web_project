import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonService } from "../../../services/common.service";


@Component({
  templateUrl: './metro-features-side-bar.component.html',
    selector: 'metro-features-side-bar'
})

export class MetroFeaturesSideBarComponent{
  // activatedroute : any;
  content:any;
  constructor(private commonService: CommonService){
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }
}

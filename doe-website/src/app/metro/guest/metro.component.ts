import { Component,ViewChild, HostListener, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { CommonService } from "../../services/common.service";
import { CommonModal } from "./../../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';



@Component({
  templateUrl: './metro.component.html',

})


export class MetroGuestComponent {

  metroLand:any;
  device : any;
  faqInfo: any;
  faqs: any;
  content:any;

  @ViewChild(CommonModal)
  private modal : CommonModal;


  constructor(private fullpageService: MnFullpageService, private commonService: CommonService,private route : Router, private activatedroute: ActivatedRoute){
    this.metroLand = {};
    this.metroLand.faqTopics = [];
    this.metroLand.faqs = [];
    this.metroLand.faqId = "123";
    this.metroLand.selectedFaqTopic = {};
    this.metroLand.selectedFaqTopic.description = "";
    this.metroLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };
    this.metroLand.cartinfo = {};
    this.getRetailDevices();
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
        this.changefaqbyid('0');
      },
      error => {
        console.log(error);
      });
  }


  getRetailDevices(){
    this.commonService.apiGetRetailDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.metroLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  gotodevice(item){
      this.route.navigate(['/metro/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }


  ngOnInit() {
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUTMETRO":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "FAQS":
          this.fullpageService.moveTo(4,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)
    })
  }

  changefaqbyid (i){
    this.metroLand.selectedFaqTopic = ( this.content && this.content.METRO.FAQS[i] ) ? this.content.METRO.FAQS[i] : {};
    this.metroLand.faqs= ( this.content && this.content.METRO.FAQS[i] && this.content.METRO.FAQS[i].QNA) ?  this.content.METRO.FAQS[i].QNA : [];
  }



  ngOnDestroy() { this.fullpageService.destroy('all'); }
}

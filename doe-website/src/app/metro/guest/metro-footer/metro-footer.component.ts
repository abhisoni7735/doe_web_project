import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'metro-footer',
  templateUrl: './metro-footer.component.html',

})

export class MetroFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoMetroMenu(type){
      switch(type){
        case 'ABOUTMETRO':
          if(this.activatedroute.routeConfig.component.name == "MetroGuestComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/metro']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "MetroGuestComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/metro'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "MetroGuestComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/metro'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "MetroGuestComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/metro'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

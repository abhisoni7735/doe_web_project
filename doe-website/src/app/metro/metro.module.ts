import { NgModule } 					             from '@angular/core';
import { FormsModule }    				         from '@angular/forms';
import { CommonModule } 				           from '@angular/common';
import { DirectivesModule }                from './../directives/directives.module';
import { MnFullpageModule }                from "ngx-fullpage";


import { CommonBaseModule } 				       from './../common/common.module';
import { MerchantModule }                  from './../merchant/merchant.module';
import {PipesModule}                       from '../pipes/pipes.module';

import { MetroRoutingModule } 			       from './metro-routing-module';

//guest components
import { MetroGuestComponent }             from './guest/metro.component';
import { MetroReaderComponent }            from './guest/metro-reader/metro-reader.component';

import { MetroFeaturesSideBarComponent }   from './guest/metro-features-side-bar/metro-features-side-bar.component';
import { MetroMerchantComponent }          from './guest/metro-merchant/metro-merchant.component';
import {MetroFooterComponent}                      from './guest/metro-footer/metro-footer.component';



@NgModule({
    imports: [
    	CommonModule,
        MetroRoutingModule,
        DirectivesModule,
        FormsModule,
        MerchantModule,
        PipesModule,
        MnFullpageModule.forRoot()

    ],
    declarations: [
      //guest
      MetroGuestComponent,
      MetroReaderComponent,
      MetroMerchantComponent,
      MetroFeaturesSideBarComponent,
      MetroFooterComponent
    ],
    exports: []
})
export class MetroModule {
}

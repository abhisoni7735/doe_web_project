import { NgModule }                            from '@angular/core';
import { RouterModule, Routes }                from '@angular/router';

import { MetroGuestComponent}               from './guest/metro.component'
import { MetroReaderComponent }             from './guest/metro-reader/metro-reader.component';
import { MetroMerchantComponent }           from './guest/metro-merchant/metro-merchant.component';



const routes: Routes = [
  { path: '',                   component: MetroGuestComponent },
  { path: 'merchant',           component: MetroMerchantComponent },
  { path: 'reader/:id',         component:MetroReaderComponent}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MetroRoutingModule {}

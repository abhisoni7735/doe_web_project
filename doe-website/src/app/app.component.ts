import { Component, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, Event as NavigationEvent } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements AfterViewInit {
  	title = 'DOE - WEBSITE';
  	loading:any;
  	constructor(
        private router: Router
    ) {
        this.loading = true;
    }

    ngAfterViewInit(){


    		this.router.events
            .subscribe((event) => {
                if(event instanceof NavigationStart) {
                    this.loading = true;
                }
                else if (
                    event instanceof NavigationEnd || 
                    event instanceof NavigationCancel
                    ) {
                    this.loading = false;
                }
            });


    }
  	
}

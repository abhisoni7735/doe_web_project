import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'parking-features',
  templateUrl: './parking-features.component.html'

})

export class ParkingFeaturesComponent {

   content:any;
  constructor( private commonService: CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

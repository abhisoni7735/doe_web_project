import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'parking-footer',
  templateUrl: './parking-footer.component.html',

})

export class ParkingFooterComponent {

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoParkingMenu(type){
      switch(type){
        case 'ABOUTPARKING':
          if(this.activatedroute.routeConfig.component.name == "ParkingComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/parking']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "ParkingComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/parking'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "ParkingComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/parking'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "ParkingComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/parking'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

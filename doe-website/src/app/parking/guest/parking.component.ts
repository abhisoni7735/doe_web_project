import { Component,ViewChild, HostListener, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { CommonService } from "../../services/common.service";
import { CommonModal } from "./../../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({
  templateUrl: './parking.component.html'
})

export class ParkingComponent {
   parkingLand : any;
   device : any;
   faqInfo: any;
   faqs: any;
   content:any;

   @ViewChild(CommonModal)
   private modal : CommonModal;


  constructor (private fullpageService: MnFullpageService, private commonService: CommonService,private route : Router, private activatedroute: ActivatedRoute) {

    this.parkingLand = {};
    this.parkingLand.faqTopics = [];
    this.parkingLand.faqs = [];
    this.parkingLand.faqId = "123";
    this.parkingLand.selectedFaqTopic = {};
    this.parkingLand.selectedFaqTopic.description = "";
    this.parkingLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };
    this.parkingLand.cartinfo = {};
    this.getParkingDevices();
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
        this.changefaqbyid('0');
      },
      error => {
        console.log(error);
      });

  }

  getParkingDevices(){
    this.commonService.apiGetParkingDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.parkingLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  gotodevice(item){
      this.route.navigate(['/parking/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }


  ngOnInit() {
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUTPARKING":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "FAQS":
          this.fullpageService.moveTo(4,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)
    })
  }

  changefaqbyid (i){
    this.parkingLand.selectedFaqTopic = ( this.content && this.content.PARKING.FAQS[i] ) ? this.content.PARKING.FAQS[i] : {};
    this.parkingLand.faqs= ( this.content && this.content.PARKING.FAQS[i] && this.content.PARKING.FAQS[i].QNA) ?  this.content.PARKING.FAQS[i].QNA : [];
  }



  ngOnDestroy() { this.fullpageService.destroy('all'); }
}

import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';



// guest
import{ ParkingComponent }            from './guest/parking.component';
import{ ParkingBeMerchantComponent }  from './guest/be-merchant/be-merchant.component';
import { ParkingReaderComponent }     from './guest/parking-reader/parking-reader.component';





const parkroutes: Routes = [
  { path: '', component: ParkingComponent},
  { path: 'be-merchant',         component: ParkingBeMerchantComponent },
  { path: 'reader/:id',         component: ParkingReaderComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(parkroutes) ],
  exports: [ RouterModule ]
})
export class ParkingRoutingModule {}

import { NgModule}                from '@angular/core';
import { FormsModule }            from '@angular/forms';
import { CommonModule }           from '@angular/common';
import { MnFullpageModule }                from "ngx-fullpage";


import { DirectivesModule }       from './../directives/directives.module';
import { CommonBaseModule }       from './../common/common.module';
import { PipesModule}             from '../pipes/pipes.module';
import { MerchantModule }         from "../merchant/merchant.module"

import { ParkingRoutingModule }   from './parking-routing-module';

// guest
import { ParkingComponent }       from './guest/parking.component';
import { ParkingBeMerchantComponent } from './guest/be-merchant/be-merchant.component';

import { ParkingReaderComponent }   from './guest/parking-reader/parking-reader.component';
import { ParkingFooterComponent }   from './guest/parking-footer/parking-footer.component';
import { ParkingFeaturesComponent } from './guest/features/parking-features.component';



@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        PipesModule,
        CommonBaseModule,
        ParkingRoutingModule,
        MerchantModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      ParkingComponent,
      ParkingReaderComponent,
      ParkingBeMerchantComponent,
      ParkingFooterComponent,
      ParkingFeaturesComponent
    ],
    exports: [
    ]
})
export class ParkingModule {

}

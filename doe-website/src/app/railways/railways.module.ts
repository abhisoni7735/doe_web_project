import { NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { DirectivesModule }     from './../directives/directives.module';
import { MerchantModule     }         from './../merchant/merchant.module';
import { CommonBaseModule }     from './../common/common.module';

import { CommonModal }            from "./../common/modal/modal.component";


import {RailwaysComponent}                  from './guest/railways.component';
import {RailwaysFeaturesComponent}          from './guest/features/railways-features.component';
import {RailwaysReaderComponent}            from './guest/railways-reader/railways-reader.component';
import {RailwaysFooterComponent}            from './guest/railways-footer/railways-footer.component';


import {RailwaysRoutingModule}               from './railways-routing-module';
import {PipesModule}                     from '../pipes/pipes.module';
import { ReactiveFormsModule }               from '@angular/forms';

import { MnFullpageModule } from "ngx-fullpage";




@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        RailwaysRoutingModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        ReactiveFormsModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      RailwaysComponent,
      RailwaysFeaturesComponent,
      RailwaysReaderComponent,
      RailwaysFooterComponent
    ],
    exports: [
    ]
})
export class RailwaysModule {

}

import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({
  selector: 'railways-footer',
  templateUrl: './railways-footer.component.html',

})

export class RailwaysFooterComponent {
  tollmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoRailwaysComponentMenu(type){
      switch(type){
        case 'ABOUT':
          if(this.activatedroute.routeConfig.component.name == "RailwaysComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/railways']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "RailwaysComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/railways'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "RailwaysComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/railways'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "RailwaysComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/railways'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

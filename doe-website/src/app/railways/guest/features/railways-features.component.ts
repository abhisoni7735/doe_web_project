import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'railways-features',
  templateUrl: './railways-features.component.html'

})

export class RailwaysFeaturesComponent {

  content:any;
  constructor(private commonService : CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { RailwaysComponent }   		      from './guest/railways.component';
import { RailwaysReaderComponent }   		from './guest/railways-reader/railways-reader.component';


const routes: Routes = [
  { path: '',                    component: RailwaysComponent },
  { path: 'reader/:id',          component: RailwaysReaderComponent },


];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class RailwaysRoutingModule {}

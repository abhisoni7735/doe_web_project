import { NgModule } 					             from '@angular/core';
import { FormsModule }    				         from '@angular/forms';
import { CommonModule } 				           from '@angular/common';
import { DirectivesModule }                from './../directives/directives.module';

import { CommonBaseModule } 				       from './../common/common.module';
import { MerchantModule }                    from './../merchant/merchant.module';
import { BusRoutingModule } 			         from './bus-routing-module';

//guest components
import { BusGuestComponent }               from './guest/bus-guest.component';
import { BusMerchantComponent }            from './guest/bus-merchant/bus-merchant.component';
import { BusReaderComponent }              from './guest/bus-reader/bus-reader.component';
import { BusFeaturesSideBarComponent }     from './guest/bus-features-side-bar/bus-features-side-bar.component';

import { BusFooterComponent }                 from './guest/bus-footer/bus-footer.component';
import { MnFullpageModule } from "ngx-fullpage";
import {PipesModule}                     from '../pipes/pipes.module';




@NgModule({
    imports: [
    	CommonModule,
        BusRoutingModule,
        DirectivesModule,
        FormsModule,
        MerchantModule,
        PipesModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      //guest
      BusGuestComponent,
      BusFeaturesSideBarComponent,
      BusFooterComponent,
      BusReaderComponent,
      BusMerchantComponent

    ],
    exports: []
})
export class BusModule {
}

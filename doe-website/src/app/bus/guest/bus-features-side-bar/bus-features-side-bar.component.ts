import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonService } from "../../../services/common.service";
import { ParkingService } from "../../../services/parking.service";


@Component({
  templateUrl: './bus-features-side-bar.component.html',
    selector: 'bus-features-side-bar'
})

export class BusFeaturesSideBarComponent{
   content : any;

  constructor(private commonService: CommonService){
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }
}

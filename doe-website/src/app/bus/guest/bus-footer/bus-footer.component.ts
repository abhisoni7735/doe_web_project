import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'bus-footer',
  templateUrl: './bus-footer.component.html',

})

export class BusFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoBusMenu(type){
      switch(type){
        case 'ABOUTBUS':
          if(this.activatedroute.routeConfig.component.name == "BusGuestComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/bus']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "BusGuestComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/bus'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "BusGuestComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/bus'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "BusGuestComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/bus'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

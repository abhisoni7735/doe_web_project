import { Component, ViewChild, HostListener, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { CommonService } from "../../services/common.service";
import { CommonModal } from "./../../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';



@Component({
  templateUrl: './bus-guest.component.html',

})

export class BusGuestComponent {

  busLand:any;
  device : any;
  faqInfo: any;
  faqs: any;
  content:any;

  @ViewChild(CommonModal)
  private modal : CommonModal;

  constructor(private fullpageService: MnFullpageService, private commonService: CommonService,private route : Router, private activatedroute: ActivatedRoute){
    this.busLand = {};
    this.busLand.faqTopics = [];
    this.busLand.faqs = [];
    this.busLand.faqId = "123";
    this.busLand.selectedFaqTopic = {};
    this.busLand.selectedFaqTopic.description = "";
    this.busLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };
    this.busLand.cartinfo = {};
    this.getBusDevices();
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
        this.changefaqbyid('0');
      },
      error => {
        console.log(error);
      });
  }
  getBusDevices(){
    this.commonService.apiGetBusDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.busLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  gotodevice(item){
      this.route.navigate(['/bus/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }


  ngOnInit() {
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUTBUS":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "SERVICES":
          this.fullpageService.moveTo(4,1);
          break;
          case "FAQS":
          this.fullpageService.moveTo(5,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)
    })
  }


  changefaqbyid (i){
    this.busLand.selectedFaqTopic = ( this.content && this.content.BUS.FAQS[i] ) ? this.content.BUS.FAQS[i] : {};
    this.busLand.faqs= ( this.content && this.content.BUS.FAQS[i] && this.content.BUS.FAQS[i].QNA) ?  this.content.BUS.FAQS[i].QNA : [];
  }



  ngOnDestroy() { this.fullpageService.destroy('all'); }
}

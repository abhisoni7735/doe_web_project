import { NgModule }                            from '@angular/core';
import { RouterModule, Routes }                from '@angular/router';

import { BusGuestComponent }                   from './guest/bus-guest.component';
import { BusReaderComponent }             from './guest/bus-reader/bus-reader.component';
import { BusMerchantComponent }             from './guest/bus-merchant/bus-merchant.component';

const routes: Routes = [
  { path: '',                  component: BusGuestComponent },
  { path: 'be-merchant',         component: BusMerchantComponent },
  { path: 'reader/:id',         component: BusReaderComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class BusRoutingModule {}

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TermsOfServicesComponent } from '../termsOfServices/termsOfServices.component';
import { PrivacyPolicyComponent } from '../privacyPolicy/privacyPolicy.component';
import { AboutUsComponent } from '../aboutus/about-us.component';
import { PartnersComponent } from '../partners/partners.component';
import { CustomerContactComponent } from '../register/customer-register.component';
import { ConsumerJoinusComponent }   from '../consumer-joinus/consumer-joinus.component';
import { MerchantJoinusComponent }   from '../merchant-joinus/merchant-joinus.component';
import { ContactusComponent }   from '../contactus/contactus.component';

import { PageNotFoundComponent }              from '../404/404.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', loadChildren: 'app/guest/guest.module#GuestModule' },
    { path: 'page-not-found', component: PageNotFoundComponent},
  //{ path: 'home', loadChildren: 'app/guest/guest.module#GuestModule'  },
   	{ path: 'toll', loadChildren: 'app/toll/toll.module#TollModule' },
    { path: 'retail', loadChildren: 'app/retail/retail.module#RetailModule' },
    { path: 'metro', loadChildren: 'app/metro/metro.module#MetroModule' },
    { path: 'bus', loadChildren: 'app/bus/bus.module#BusModule' },
    { path: 'residence', loadChildren: 'app/residence/residence.module#ResidenceModule' },
    { path: 'office', loadChildren: 'app/office/office.module#OfficeModule' },
    { path: 'parking', loadChildren: 'app/parking/parking.module#ParkingModule' },
    { path: 'education', loadChildren: 'app/education/education.module#EducationModule' },
    { path: 'terms', component: TermsOfServicesComponent},
    { path: 'privacy-policy', component: PrivacyPolicyComponent},
    { path: 'consumer-joinus', component: ConsumerJoinusComponent },
    { path: 'merchant-joinus', component: MerchantJoinusComponent },
    { path: 'joinus', component: CustomerContactComponent },
    { path: 'contactus', component: ContactusComponent },
    { path: 'aboutus', component: AboutUsComponent },
    { path: 'partners', component: PartnersComponent },
    { path: 'merchant', loadChildren: 'app/merchant/merchant.module#MerchantModule' },
    { path: 'corporate', loadChildren: 'app/corporate/corporate.module#CorporateModule'  },
    { path: 'access', loadChildren: 'app/access/access.module#AccessModule' },
    { path: 'loyality', loadChildren: 'app/loyality/loyality.module#LoyalityModule'  },
    { path: 'railways', loadChildren: 'app/railways/railways.module#RailwaysModule'  },
   	{ path: '**', redirectTo: '/page-not-found'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { RetailComponent } from './retail.component';
import { RetailBeMerchantComponent } from './guest/retail-be-merchant/retail-be-merchant.component';
import { RetailReaderComponent } from './guest/retail-reader/retail-reader.component';

const routes: Routes = [
  { path: '',                    component: RetailComponent },
  { path: 'retail-be-merchant',         component: RetailBeMerchantComponent },
  { path: 'reader/:id',         component: RetailReaderComponent },

];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class RetailRoutingModule {}

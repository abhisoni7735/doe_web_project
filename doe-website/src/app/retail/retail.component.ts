import { Component, Output, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { CommonService } from "../services/common.service";
import { CommonModal } from "./../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({

  templateUrl: './retail.component.html',

})

export class RetailComponent {

 retailLand:any;
 device : any;
 faqInfo: any;
 faqs: any;
 content:any;

 @ViewChild(CommonModal)
 private modal : CommonModal;

 @Output() public options: MnFullpageOptions = new MnFullpageOptions({
       navigation: true,
       keyboardScrolling: true
   });

  constructor(private fullpageService: MnFullpageService, private commonService: CommonService, private route : Router, private activatedroute: ActivatedRoute) {
    this.retailLand = {};
    this.retailLand.faqTopics = [];
    this.retailLand.faqs = [];
    this.retailLand.faqId = "123";
    this.retailLand.selectedFaqTopic = {};
    this.retailLand.selectedFaqTopic.description = "";
    this.retailLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };
    this.retailLand.cartinfo = {};

    this.getRetailDevices();

    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
        this.changefaqbyid('0');
      },
      error => {
        console.log(error);
      });
  }

  getRetailDevices(){
    this.commonService.apiGetRetailDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.retailLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  gotodevice(item){
      this.route.navigate(['/retail/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }

  ngOnInit(){
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUTRETAIL":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "FAQ":
          this.fullpageService.moveTo(4,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)

    })

  }

  changefaqbyid (i){
    this.retailLand.selectedFaqTopic = ( this.content && this.content.RETAIL.FAQS[i] ) ? this.content.RETAIL.FAQS[i] : {};
    this.retailLand.faqs= ( this.content && this.content.RETAIL.FAQS[i] && this.content.RETAIL.FAQS[i].QNA) ?  this.content.RETAIL.FAQS[i].QNA : [];
  }

  ngOnDestroy() { this.fullpageService.destroy('all'); }

}

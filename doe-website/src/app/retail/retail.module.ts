import { NgModule}                                  from '@angular/core';
import { FormsModule }                              from '@angular/forms';
import { CommonModule }                             from '@angular/common';

import { DirectivesModule }                         from './../directives/directives.module';
import { MerchantModule     }                       from './../merchant/merchant.module';
import { CommonBaseModule }                         from './../common/common.module';

import { RetailRoutingModule }                      from './retail-routing-module';

import { RetailComponent }                          from './retail.component';
import { RetailBeMerchantComponent }                from './guest/retail-be-merchant/retail-be-merchant.component';
import { RetailMenuComponent }                      from './guest/retail.menu/retail.menu.component';
import { RetailReaderComponent }                    from './guest/retail-reader/retail-reader.component';

import {PipesModule}                                from '../pipes/pipes.module';
import { MnFullpageModule } from "ngx-fullpage";

import {RetailFooterComponent}                      from './guest/retail-footer/retail-footer.component';
import {RetailFeaturesComponent}                    from './guest/features/retail-features.component';


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        RetailRoutingModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      RetailComponent,
      RetailBeMerchantComponent,
      RetailMenuComponent,
      RetailReaderComponent,
      RetailFooterComponent,
      RetailFeaturesComponent

    ],
    exports: []
})
export class RetailModule {

}

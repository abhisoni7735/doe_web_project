import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonService } from "../../../services/common.service";
import { CommonModal } from "../../../common/modal/modal.component";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({

    templateUrl: './retail-reader.component.html'

})

export class RetailReaderComponent {

  device:any;
  id: any;
  deviceinfo: any;
  routeparms :any;
  deviceImages : any;

  @ViewChild(CommonModal)
  private modal : CommonModal;

  constructor(private fullpageService: MnFullpageService, private activatedroute: ActivatedRoute, private route : Router, private commonService : CommonService) {
    this.device = {
      "name" : "",
      "images" : []
    };
    this.deviceImages = {};
    this.deviceImages.showImageUrl = "";
    this.initialiseState();
  }

  initialiseState(){
    this.routeparms = this.activatedroute.snapshot.params['id'];
    this.id = this.routeparms ? this.routeparms : "";
    this.getRetailDevices();
    console.log("Store info here---------------------------->");
  }

  gotoRetail(page){
    switch(page){
      case 'ABOUTRETAIL':
        if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
            this.fullpageService.moveTo(1,1);
        }else{
          this.route.navigate(['/retail']);
        }
      break;
      case 'FEATURES':
        if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
            this.fullpageService.moveTo(2,1);
        }else{
          this.route.navigate(['/retail'],{ queryParams: { sec: 'FEATURES' }});
        }
      break;
      case 'PRODUCTS':
        if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
            this.fullpageService.moveTo(3,1);
        }else{
          this.route.navigate(['/retail'],{ queryParams: { sec: 'PRODUCTS' }});
        }
      break;
      case 'FAQ':
        if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
            this.fullpageService.moveTo(4,1);
        }else{
          this.route.navigate(['/retail'],{ queryParams: { sec: 'FAQ' }});
        }
      break;
    }
  }

   getRetailDevices(){
    this.commonService.apiGetRetailDevicesfromJson()
    .subscribe(
      response => {
        this.deviceinfo = response;
        this.device = this.deviceinfo[this.id];
        this.deviceImages.showImageUrl = this.device.images[0].originalImage;
        console.log("device info");
        console.log(this.device);
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  changeImage(index){
    console.log("index === > " + index);
    this.deviceImages.showImageUrl = this.device.images[index].originalImage;
  }

  ngOnInit() { }

}

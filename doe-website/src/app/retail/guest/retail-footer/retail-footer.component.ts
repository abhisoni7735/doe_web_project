import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'retail-footer',
  templateUrl: './retail-footer.component.html',

})

export class RetailFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoRetailMenu(type){
      switch(type){
        case 'ABOUTRETAIL':
          if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/retail']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/retail'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/retail'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "RetailComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/retail'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";



@Component({
  selector: 'retail-features',
  templateUrl: './retail-features.component.html'

})

export class RetailFeaturesComponent {


  content:any;
  constructor( private commonService: CommonService) {

    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

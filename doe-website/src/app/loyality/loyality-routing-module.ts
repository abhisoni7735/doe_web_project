import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { LoyalityComponent }   		      from './guest/loyality.component';
import { LoyalityReaderComponent }   		from './guest/loyality-reader/loyality-reader.component';


const routes: Routes = [
  { path: '',                    component: LoyalityComponent },
  { path: 'reader/:id',          component: LoyalityReaderComponent },


];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class LoyalityRoutingModule {}

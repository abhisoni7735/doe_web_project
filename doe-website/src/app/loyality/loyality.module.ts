import { NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { DirectivesModule }     from './../directives/directives.module';
import { MerchantModule     }         from './../merchant/merchant.module';
import { CommonBaseModule }     from './../common/common.module';

import { CommonModal }            from "./../common/modal/modal.component";


import {LoyalityComponent}                  from './guest/loyality.component';
import {LoyalityFeaturesComponent}          from './guest/features/loyality-features.component';
import {LoyalityReaderComponent}            from './guest/loyality-reader/loyality-reader.component';
import {LoyalityFooterComponent}            from './guest/loyality-footer/loyality-footer.component';


import {LoyalityRoutingModule}               from './loyality-routing-module';
import {PipesModule}                     from '../pipes/pipes.module';
import { ReactiveFormsModule }               from '@angular/forms';

import { MnFullpageModule } from "ngx-fullpage";




@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        LoyalityRoutingModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        ReactiveFormsModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      LoyalityComponent,
      LoyalityFeaturesComponent,
      LoyalityReaderComponent,
      LoyalityFooterComponent
    ],
    exports: [
    ]
})
export class LoyalityModule {

}

import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'loyality-features',
  templateUrl: './loyality-features.component.html'

})

export class LoyalityFeaturesComponent {

  content:any;
  constructor(private commonService : CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

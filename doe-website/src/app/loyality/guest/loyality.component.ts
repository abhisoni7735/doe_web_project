
import { Component, ViewChild, HostListener, Output, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MerchantGuestLoginComponent } from '../../common/merchant-guest-login/merchant-guest-login.component';
import { Base64Service } from "../../services/base64.service";
import { CommonModal } from "./../../common/modal/modal.component";
import { CommonService } from "../../services/common.service";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';

@Component({
  templateUrl: './loyality.html'

})

export class LoyalityComponent{

  loyalityLand : any;
  device : any;
  faqInfo: any;
  faqs: any;
  merchantuserinfo: any;
  content:any;

  @ViewChild(MerchantGuestLoginComponent)
   merchantGuestLoginComponent : MerchantGuestLoginComponent;

  @ViewChild(CommonModal)
  private modal : CommonModal;

  @Output() public options: MnFullpageOptions = new MnFullpageOptions({
        navigation: true,
        keyboardScrolling: true
    });

  constructor(private fullpageService: MnFullpageService,  private commonService : CommonService, private route : Router, private base64:Base64Service, private activatedroute: ActivatedRoute) {
    this.loyalityLand = {};
    this.loyalityLand.faqTopics = [];
    this.loyalityLand.faqs = [];
    this.loyalityLand.faqId = "123";
    this.loyalityLand.selectedFaqTopic = {};
    this.loyalityLand.selectedFaqTopic.description = "";
    this.loyalityLand.deviceinfo = {
      "items" : {},
      "totalItems" : 0,
      "totalPages" : 0,
      "currentPage" : 1
    };

    this.loyalityLand.cartinfo = {};

    this.getDevices();
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
        this.changefaqbyid('0');
      },
      error => {
        console.log(error);
      });
  }

  getDevices(){
    this.commonService.apiGetDevicesfromJson()
    .subscribe(
      response => {
        console.log("toll devices from json======>")
        console.log(response);
        this.loyalityLand.deviceinfo.items = response;
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  ngOnInit(){
    this.activatedroute.queryParams
    .subscribe(params => {

      setTimeout(()=>{
        console.log("params ===================>>");
        console.log(params);
        let section = params.sec ? params.sec : "";
        switch(section){
          case "ABOUT":
          this.fullpageService.moveTo(1,1);
          break;
          case "FEATURES":
          this.fullpageService.moveTo(2,1);
          break;
          case "PRODUCTS":
          this.fullpageService.moveTo(3,1);
          break;
          case "SERVICES":
          this.fullpageService.moveTo(4,1);
          break;
          case "FAQ":
          this.fullpageService.moveTo(5,1);
          break;
          default:
          console.log("do nothing");
          break;
        }
      },0)

    })

  }


  ngOnDestroy() { this.fullpageService.destroy('all'); }

  disableHomePageScroll(sm){
    console.log("Entered Method--------->");
    switch(sm){
      case 'on': this.fullpageService.setAllowScrolling(false);
        console.log("scrolling setted false--");

       break;
      case 'off': this.fullpageService.setAllowScrolling(true);
      console.log("scrolling setted true--");

      break;
    }
  }



  changefaqbyid (i){
    this.loyalityLand.selectedFaqTopic = ( this.content && this.content.TOLL.FAQS[i] ) ? this.content.TOLL.FAQS[i] : {};
    this.loyalityLand.faqs= ( this.content && this.content.TOLL.FAQS[i] && this.content.TOLL.FAQS[i].QNA) ?  this.content.TOLL.FAQS[i].QNA : [];
  }



  gotodevice(item){
      this.route.navigate(['/loyality/reader/' + item]);
      console.log("Id value---------------------------------------------------->");
  }




}

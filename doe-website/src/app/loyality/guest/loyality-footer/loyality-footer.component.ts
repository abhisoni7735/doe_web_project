import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({
  selector: 'loyality-footer',
  templateUrl: './loyality-footer.component.html',

})

export class LoyalityFooterComponent {
  tollmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoLoyalityComponentMenu(type){
      switch(type){
        case 'ABOUTACCESS':
          if(this.activatedroute.routeConfig.component.name == "LoyalityComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/loyality']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "LoyalityComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/loyality'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "LoyalityComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/loyality'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "LoyalityComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/loyality'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

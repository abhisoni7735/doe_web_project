import { Injectable } 				from '@angular/core';
import { TollService }        from "./../services/toll.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";


@Injectable()
export class GmapService {
  constructor(private tollService: TollService, private http: HttpClient){
  }

  gapiSearchPlaces(searchText) {
            return this.http
            .get("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + searchText + "&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyBrjTZIGsRLPJp_83Xr39DMADWJsP5HwIQ", {
                headers: {
                  "Content-Type" : "application/json",
                }

            });
        }
  }

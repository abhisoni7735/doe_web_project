import { Injectable } 				from '@angular/core';
import { HttpClient, HttpHeaders } 	from '@angular/common/http';

import { Observable } 				from 'rxjs/Observable';
import { environment } 				from  '../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Cache-Control': 'no-cache'
 })
};

@Injectable()
export class CommonService {

	private apiurl = environment.apiurls;

    userData : any;
    userActivityData : any;
    userCardsData : any;
    userSelectedCard : any;
    userSelectedCardId : any;
    userSelectedCardNumber : any;

  	constructor(private http: HttpClient) {
      this.userData = {};
      this.userActivityData = {};
      this.userCardsData = {};
      this.userSelectedCard = {};
      this.userSelectedCardId = "";
      this.userSelectedCardNumber = "";
    }

  	private extractData(res: Response) {

        return res || { };
    }

    private handleError(error: any) {

        return Observable.throw(error);
    }

  	apiQuickRecharge (reqobj){
        if(this.apiurl.isLiveApi){

             return this.http.post( this.apiurl.activityserv + '/activities', reqobj, {headers: httpOptions.headers} )
	        .map(this.extractData);

        }else{
            console.log('live data false');
        }

    }

    ///common function using
    setUserSelectedCardId(id){
        let store = localStorage.getItem('store');
        let storeInfo = JSON.parse(store);
        storeInfo.consumer.selectedcardid = id ? id : "";
        localStorage.setItem('store', JSON.stringify(storeInfo));
    }
    getUserSelectedCardId(){
        let store = localStorage.getItem('store');
        let storeInfo = JSON.parse(store);
        this.userSelectedCardId = storeInfo && storeInfo.consumer && storeInfo.consumer.selectedcardid ? storeInfo.consumer.selectedcardid : "";
        return this.userSelectedCardId;
    }
    setUserSelectedCardNumber(cardno){
        let store = localStorage.getItem('store');
        let storeInfo = JSON.parse(store);
        storeInfo.consumer.selectedcardno = cardno ? cardno : "";
        localStorage.setItem('store', JSON.stringify(storeInfo));
    }
    getUserSelectedCardNumber(){
        let store = localStorage.getItem('store');
        let storeInfo = JSON.parse(store);
        this.userSelectedCardNumber = storeInfo && storeInfo.consumer && storeInfo.consumer.selectedcardno ? storeInfo.consumer.selectedcardno : "";
        return this.userSelectedCardNumber;
    }

    getObjLength(obj){
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
        }

    getMapStyles(){
            return this.http.get('assets/json/data/map-styles.json')
            .map(this.extractData)
        }

    getSocialicons(){
            return this.http.get('assets/json/data/social-icons.json')
            .map(this.extractData)
        }


    getStaticContent(){
            return this.http.get('assets/json/static-content.json')
            .map(this.extractData)
        }


    apiMerchantlogin(reqObj){
        if(this.apiurl.isLiveApi){
            var txt = "username="+ reqObj.phoneNumber+ "&password="+ reqObj.password +"&grant_type=password";
            var headers = {
                "Content-Type" : "application/x-www-form-urlencoded",
                'Cache-Control': 'no-cache'
            }
            return this.http.post(this.apiurl.mmsauthserv + '/token', txt, {headers: headers})
                .map(this.extractData)
                .catch(this.handleError);
        }else{
              return this.http.get('app/assets/json/login.json')
              .map(this.extractData)
        }
    }

    apiGetDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    //Retail devices from JSON
    apiGetRetailDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }


    apiGetMetroDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    apiGetBusDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    apiGetParkingDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    apiGetEducationDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    apiGetResidenceDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    apiGetOfficeDevicesfromJson(){
      let headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      return this.http
          .get('assets/json/data/toll-devices.json', { headers: headers })
          .map(this.extractData);
    }

    //FAQ Service
    apiGetFAQInfo(moduleName) {
      switch (moduleName) {
        case "TOLL":
          if (this.apiurl.isLiveApi) {
            return this.http
              .get(this.apiurl.contentserv + "/faq-topics?category=ONBOARDING", {
                headers: httpOptions.headers
              })
              .map(this.extractData);
          } else {
            console.log("live data false");
          }
          break;
      }
    }

    apiGetFAQs(id) {
      if (this.apiurl.isLiveApi) {
        return this.http
          .get(
            this.apiurl.contentserv + "/faqs?topic_id=" + id + "&statuses=ACTIVE",
            {
              headers: httpOptions.headers
            }
          )
          .map(this.extractData);
      } else {
        console.log("live data false");
      }
    }





}

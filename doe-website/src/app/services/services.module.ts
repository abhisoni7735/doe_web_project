import {NgModule} from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { ParkingService } from './parking.service';
import { CommonService }     from './common.service';
import { TollService }     from './toll.service';
import { CustomerService }  from './customer.service';
import { Base64Service } from './base64.service';
import { TollAuthGuard } from './toll-auth-guard.guard.service';
import { GmapService } from './gmap.service';



@NgModule({
    imports: [
      FormsModule
    ],
    declarations: [
    ],
    providers: [
      CommonService,
      CustomerService,
      ParkingService,
      Base64Service,
      TollService,
      TollAuthGuard,
      GmapService
    ],
    exports: [

    ]
})
export class ServicesModule {
}

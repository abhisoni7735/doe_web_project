import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "Cache-Control": "no-cache",
    "Pragma" : "no-cache",
    "If-Modified-Since" : 'Mon, 26 Jul 1997 05:00:00 GMT'
  })
};

@Injectable()
export class TollService {
  private apiurl = environment.apiurls;
  private storeinfodata = environment.storeInfo;

  constructor(private http: HttpClient) {}

  private extractData(res: Response) {
    return res || {};
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
  getStoreData(moduleName: string) {
    switch (moduleName) {
      case "TOLL":
        let store = localStorage.getItem("store");
        if (store) {
          let storeInfo = JSON.parse(store);
          let tollstoreinfodata = storeInfo.toll
            ? storeInfo.toll
            : this.storeinfodata.toll;
          return tollstoreinfodata;
        } else {
          return this.storeinfodata.toll;
        }
    } //switch closed here
  }

  setStoreData(data, moduleName) {
    switch (moduleName){
      case "TOLL":
        let store = localStorage.getItem("store");
        if (store) {
          let storeInfo = JSON.parse(store);
          storeInfo.toll = data ? data : this.storeinfodata.toll;
          localStorage.setItem("store", JSON.stringify(storeInfo));
        } else {
          let tolldata = data ? data : this.storeinfodata.toll;
          localStorage.setItem("store", JSON.stringify(this.storeinfodata));
        }

        break;
    }
  }

  setSignupstoredata(data, moduleName, callback){
      let store = localStorage.getItem("store");
        if (store) {

          let storeInfo = JSON.parse(store);
          storeInfo.toll = data ? data : this.storeinfodata.toll;
          let setval = JSON.stringify(storeInfo);
          localStorage.setItem("store", setval );
          let updatestore = localStorage.getItem("store");
            let updatedstoreInfo = JSON.parse(updatestore);

            console.log("with store");

            if(updatedstoreInfo.toll.accesstoken){
                callback();
            }else{
              console.log("he hehe i stopped here");
            }

        } else {
          let storeval = this.storeinfodata;
          storeval.toll = data ? data : this.storeinfodata.toll;

          let stvalw = JSON.stringify(storeval);
          localStorage.setItem("store", stvalw);
          let updatestore = localStorage.getItem("store");
            let updatedstoreInfo = JSON.parse(updatestore);


            console.log("with out store");
            if(updatedstoreInfo.toll.accesstoken){
                callback();
            }else{
              console.log("he hehe i stopped here");
            }
        }
  }

  apiGetCartInfo(id, moduleName) {
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let url = id ? "/carts/" + id : "/carts";
          return this.http
            .get(this.apiurl.mmscheckoutserv + url, {
              headers: httpOptions.headers
            })
            .map(this.extractData);
        } else {
          console.log("live data false");
        }
    }
  }



  apiGetTollDevicesfromJson(){
    let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
    return this.http
        .get('assets/json/data/toll-devices.json', { headers: headers })
        .map(this.extractData);
  }




  apiGetDevicesInfo(id, obj) {
    if (this.apiurl.isLiveApi) {
      let url = id ? "/device-products/" + id : "/device-products?statuses=AVAILABLE,UNAVAILABLE";
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
      return this.http
        .get(this.apiurl.mmsdeviceserv + url, { headers: headers })
        .map(this.extractData);
    } else {
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
      return this.http
        .get('assets/json/devices.json', { headers: headers })
        .map(this.extractData);
    }
  }

  //FAQ Service
  apiGetFAQInfo(moduleName) {
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          return this.http
            .get(this.apiurl.contentserv + "/faq-topics?category=ONBOARDING", {
              headers: httpOptions.headers
            })
            .map(this.extractData);
        } else {
          console.log("live data false");
        }
        break;
    }
  }

  apiGetFAQs(id) {
    if (this.apiurl.isLiveApi) {
      //var tollStoreData = this.getStoreData("TOLL");
      return this.http
        .get(
          this.apiurl.contentserv + "/faqs?topic_id=" + id + "&statuses=ACTIVE",
          {
            headers: httpOptions.headers
          }
        )
        .map(this.extractData);
    } else {
      console.log("live data false");
    }
  }

  apiUpdateCart(reqobj, id, moduleName) {

    console.log("is service method");
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
          };

          return this.http
            .put(this.apiurl.mmscheckoutserv + "/carts/" + id, reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          console.log("this is live data");
        }

        break;
    }
  }

  apiUpdateCartUser(reqobj, id, moduleName) {

    console.log("is service method");
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Authorization": "Bearer " + tollStoreData.accesstoken
          };

          return this.http
            .patch(this.apiurl.mmscheckoutserv + "/carts/" + id, reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          console.log("this is live data");
        }

        break;
    }
  }



  apiGuestUpdateCart(reqobj, id, moduleName) {
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
            // "Authorization": tollStoreData.accesstoken
          };

          return this.http
            .patch(this.apiurl.mmscheckoutserv + "/carts/" + id, reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          console.log("this is live data");
        }

        break;
    }
  }
  // apiGetSkuDevices(skus) {
  //     if (this.apiurl.isLiveApi && false) {
  //         return this.http.get(this.apiurl.deviceserv + '/device-products?skus=' + skus, {
  //                 headers: httpOptions.headers
  //             })
  //             .map(this.extractData)
  //     } else {
  //         return this.http.get('./../assets/json/cards-info-guest.json')
  //             .map(this.extractData)
  //     }
  // }

  //guest login
  // guestCreateCart(id, moduleName) {
  //   switch (moduleName) {
  //     case "TOLL":
  //       return this.http
  //         .get("./../assets/json/guest-cart.json")
  //         .map(this.extractData);
  //   }
  // }

  //file upload API
  jsonGetStatecodes(){
    return this.http
           .get("./../assets/json/data/state-codes.json")
           .map(this.extractData);
  }


  jsonErrorMsgs(){
    return this.http
    .get("./../assets/json/error_msgs_toll.json")
           .map(this.extractData);
  }


  apiFileUpload(reqobj) {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "image/jpeg",
        "Cache-Control": "no-cache"
      };
      return this.http
        .post(this.apiurl.contentserv + "/uploads", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  //toll register API's stars here

  apiFileIDPatch(id, CompanyID) {
    let tollStoreData = this.getStoreData("TOLL");

    if (this.apiurl.isLiveApi)

      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer "+tollStoreData.accesstoken
      };

    return this.http
      .patch(
        this.apiurl.mmsbusinessserv + "/business-levels/" + CompanyID,
        id,
        {
          headers: headers
        }
      )
      .map(this.extractData);
  }

  apiBussinessCategory() {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
      return this.http
        .get(this.apiurl.domainserv + "/product-specs")
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info-guest.json")
        .map(this.extractData);
    }
  }

  apirequestCards(reqobj) {
    if (this.apiurl.isLiveApi) {

       let tollStoreData = this.getStoreData("TOLL");
       let headers = {};
       if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer "+tollStoreData.accesstoken
            };
       }else{
          headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
          };
       }

      return this.http
        .post(this.apiurl.mmssupportserv + "/tickets", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiRegisterCompany(reqObj) {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
      return this.http
        .post(
          this.apiurl.mmsbusinessserv + "/register-business-level",
          reqObj,
          {
            headers: headers
          }
        )
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiRegisterBranch(reqObj) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      var headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + tollStoreData.accesstoken,
        "Cache-Control": "no-cache"
      };
      return this.http
        .post(this.apiurl.domainserv + "/register-toll", reqObj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  //company-dashboard API's

  apiGetBusinesslevelMessages(queryparam) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.trackingServ + "/messages" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiUpdateMessage(reqobj, messageid) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .patch(this.apiurl.trackingServ + "/messages/" + messageid, reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetBusinesslevelIssues(queryparam) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.supportserv + "/tickets" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetReportData(queryparam) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.activityserv + "/reports" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetReadersSummary(queryparam) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.businessserv + "/summarize" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetBranchList(id, queryparam) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.businessserv + "/business-levels/" + id +'/children'+ queryparam , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetCompanyDetails(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.businessserv + "/business-levels/" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }
  apiUpdateCompanyDetails(id,reqobj) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .put(this.apiurl.businessserv + "/business-levels/" + id , reqobj,{
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetDoecardList(queryparam) {
    //in toll service
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.activityserv + "/activities" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetSkuDevices(skus) {
    if (this.apiurl.isLiveApi) {
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };
      return this.http
        .get(this.apiurl.mmsdeviceserv + "/device-products?skus=" + skus, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info-guest.json")
        .map(this.extractData);
    }
  }

  //toll branch services
  apiGetCardSales(queryparam) {
    //in toll service
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.activityserv + "/activities" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiCreateTicket(reqobj) {
    // in toll service
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .post(this.apiurl.supportserv + "/tickets", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetTollFaqTopics() {
    // in tol service
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.contentserv + "/faq-topics?" , { headers: headers })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiCreateOrderRequest(reqobj) {
     if (this.apiurl.isLiveApi) {
         let headers  = {
         "Content-Type": "application/json",
         "Cache-Control": "no-cache"
       };

       return this.http
        .post(this.apiurl.mmssupportserv + "/tickets", reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });

     }else{
        return this.http
                .get("app/assets/json/cards-info.json")
                .map(function(response) {
                  return response;
                });
     }
  }


  apiGetOrderData(id) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
      if(tollStoreData.accesstoken){
         headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache",
          "Authorization": "Bearer " + tollStoreData.accesstoken
        };
      }else{
         headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };
      }

      return this.http
        .get(this.apiurl.mmscheckoutserv + "/orders/" + id, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetOrderRequestData(id) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;

         headers = {
          "Content-Type": "application/json",
          "Cache-Control": "no-cache"
        };

      return this.http
        .get(this.apiurl.mmssupportserv + "/tickets/"+ id, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiReplaceCart(reqobj, moduleName) {
    if (this.apiurl.isLiveApi) {
      var tollStoreData = this.getStoreData("TOLL"); //this.getTollStoreData;
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      //  "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .patch(this.apiurl.mmscheckoutserv + "/carts/" + reqobj.id, reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetTollBranch(id) {
    if (this.apiurl.isLiveApi) {
      var tollStoreData = this.getStoreData("TOLL"); //this.getTollStoreData;
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
        // "Authorization": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzeXMudXNlciIsImJ1c2luZXNzUHJvZmlsZSI6eyIxMjM3ODYyMzU0MjY1ODIiOjEwfSwidXNlclR5cGUiOiJET0VfQURNSU4iLCJleHAiOjE1MTM1ODEwMjh9.zcLem0M-nLSc6ZpnkqDZ3h8JjGYLo8vw9c5vFEhO30o"//tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.mmsbusinessserv + "/business-levels/" + id, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiRegisterReader(reqobj) {
    let tollStoreData = this.getStoreData("TOLL");

    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      console.log(reqobj);

      return this.http
        .post(this.apiurl.mmsbusinessserv + "/register-business-level", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      console.log("this is live data");
    }
  }

    apiGetMerchantUserinfo (userid , accesstoken){ //to movie too service
           if(this.apiurl.isLiveApi){
               var headers = {
                   "Content-Type" : "application/json",
                   "Cache-Control": "no-cache",
                   "Authorization" : "Bearer " + accesstoken
               }
               return this.http.get(this.apiurl.userserv + '/users/' + userid, {headers: headers})
               .map(this.extractData)
           }else{
               return this.http.get('app/assets/json/user-info.json')
               .map(this.extractData)
           }
       }

      apiGetBranches(companyaccesstoken){
          if(this.apiurl.isLiveApi){
              var headers = {
                  "Content-Type" : "application/json",
                  "Cache-Control": "no-cache",
                  "Authorization" : "Bearer " + companyaccesstoken
              }
              return this.http.get(this.apiurl.mmsdomainserv + '/branch-names/', {headers: headers})
              .map(this.extractData)
          }else{
              return this.http.get('app/assets/json/user-info.json')
              .map(this.extractData)
          }

      }



       apiPatchCart(reqobj, id, moduleName) {
          switch (moduleName) {
            case "TOLL":
              if (this.apiurl.isLiveApi) {
                let tollStoreData = this.getStoreData("TOLL");
                 let headers;
                if(tollStoreData.accesstoken){
                    headers = {
                  "Content-Type": "application/json",
                  "Cache-Control": "no-cache",
                  "Authorization": "Bearer " + tollStoreData.accesstoken
                };
                }else{
                    headers = {
                      "Content-Type": "application/json"
                    };
                }

                return this.http
                  .patch(this.apiurl.mmscheckoutserv + "/carts/" + id, reqobj, {
                    headers: headers
                  })
                  .map(this.extractData);
              } else {
                console.log("this is live data");
              }

              break;
          }
        }


         //CURD APIs for cart
  apiCreateCart(reqobj, moduleName) {
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers;
                if(tollStoreData.accesstoken){
                    headers = {
                  "Content-Type": "application/json",
                  "Cache-Control": "no-cache",
                  "Authorization": "Bearer " + tollStoreData.accesstoken
                };
                }else{
                    headers = {
                      "Content-Type": "application/json"
                    };
                }

          return this.http
            .post(this.apiurl.mmscheckoutserv + "/carts", reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          // return this.http.get('app/assets/json/cards-info.json')
          //     .map(function(response) {
          //         return response.json();
          //     })
          console.log("this is live data");
        }
        break;
    }
  }

  apiGetUserNameAvailability(username) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .post(this.apiurl.userserv + "/check-availability", username, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

    apiCreateOrder(reqobj) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .post(this.apiurl.mmscheckoutserv + "/orders", reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }



 apiGetMerchant(reqobj) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .get(this.apiurl.mmsbusinessserv + "/business-levels/" + reqobj.id, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apiCreateOperator(reqobj){
      if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .post(this.apiurl.mmsuserserv + "/users" , reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }


  apiGetOrdersData(reqobj){
      if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .get(this.apiurl.mmscheckoutserv + "/orders" + "?page=" + reqobj.pagenumber +"&page_size=" + reqobj.pagesize , {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }


   apiGetNotices(queryparam){
      if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .get(this.apiurl.mmssupportserv + "/tickets"+ queryparam + "&categories=NOTICE&sub_categories=PROFILE_UPDATE" , {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apiUpdateNotice(reqobj){
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json"
            };
        }


      return this.http
        .post(this.apiurl.mmssupportserv + "/notices" , reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apiCancelOrder(reqobj){
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache"
            };
        }


      return this.http
        .patch(this.apiurl.mmscheckoutserv + "/orders/"+ reqobj.id ,reqobj.data, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apiallocateCards(reqobj){
     if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache"
            };
        }


      return this.http
        .post(this.apiurl.cardserv + "/update-status",reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apigetinactiveCards(reqobj){
     if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers;
        if(tollStoreData.accesstoken){
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "Authorization": "Bearer " + tollStoreData.accesstoken
            };
        }else{
            headers = {
              "Content-Type": "application/json",
              "Cache-Control": "no-cache"
            };
        }


      return this.http
        .get(this.apiurl.cardserv + "/cards?allocated_business_level_id=" + reqobj.id + "&page=" + reqobj.pagenumber +"&page_size=" + reqobj.pagesize, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }


  apiGetDevicesForCompany(queryparam){
    let test = false;
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + tollStoreData.accesstoken

      };
      return this.http
        .get(this.apiurl.deviceserv + "/devices?status=ALLOCATED" + queryparam, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("assets/json/devices.json")
        .map(this.extractData);
    }

  }

  apiRegisterHandheld(reqobj){

    let tollStoreData = this.getStoreData("TOLL");

    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      console.log(reqobj);

      return this.http
        .post(this.apiurl.mmsdeviceserv + "/assign-devices", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      console.log("this is live data");
    }

  }

  //get branch details in company
  apiGetBranchDetails(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.businessserv + "/business-levels/" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetTollsDetails(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.domainserv + "/tolls?business_level_id=" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }


  apiGetTollDetails(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.domainserv + "/tolls/" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiUpdateCompanyBranchDetails(id,reqobj) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .put(this.apiurl.businessserv + "/business-levels/" + id  , reqobj,{
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiUpdateCompanyBranchDetailsPatch(id,reqobj) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .patch(this.apiurl.businessserv + "/business-levels/" + id  , reqobj,{
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetDomainData(queryparam){
          if (this.apiurl.isLiveApi) {
              var tollStoreData = this.getStoreData("TOLL");
              var headers = {
                  "Content-Type": "application/json",
                  "Authorization": "Bearer " + tollStoreData.accesstoken
              }
              return this.http.get(this.apiurl.domainserv + "/tolls" + queryparam, {
                      headers: headers
                  })
                  .map(this.extractData);
          } else {
              return this.http.get('app/assets/json/cards-info.json')
                  .map(this.extractData);
          }
  }

      apiUpdateTollDetails(id, reqObj){
        if (this.apiurl.isLiveApi) {
        let tollStoreData = this.getStoreData("TOLL");
            let headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Authorization": "Bearer " + tollStoreData.accesstoken
          };
          return this.http
            .put(this.apiurl.domainserv + "/tolls/" + id  , reqObj,{
              headers: headers
            })
            .map(this.extractData);
        } else {
          return this.http
            .get("app/assets/json/cards-info.json")
            .map(this.extractData);
        }

      }

  apiGetTollDomainDetails(id){
          if (this.apiurl.isLiveApi) {
              var tollStoreData = this.getStoreData("TOLL");
              var headers = {
                  "Content-Type": "application/json",
                  "Authorization": "Bearer " + tollStoreData.accesstoken
              }
              return this.http.get(this.apiurl.domainserv + "/tolls/" + id, {
                      headers: headers
                  })
                  .map(this.extractData);
          } else {
              return this.http.get('app/assets/json/cards-info.json')
                  .map(this.extractData);
          }
  }

  apiCreateNotice(reqObj){

    if (this.apiurl.isLiveApi) {
        var tollStoreData = this.getStoreData("TOLL");
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + tollStoreData.accesstoken
        }
        return this.http.post(this.apiurl.mmssupportserv + "/tickets", reqObj, {
                headers: headers
            })
            .map(this.extractData);
    } else {
      console.log("this is live data");

    }

  }

  apiCreateMessage(reqObj){

    if (this.apiurl.isLiveApi) {
        var tollStoreData = this.getStoreData("TOLL");
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + tollStoreData.accesstoken
        }
        return this.http.post(this.apiurl.trackingServ + "/messages", reqObj, {
                headers: headers
            })
            .map(this.extractData);
    } else {
      console.log("this is live data");

    }

  }

  apiGetAllOperatorsUnderBranch(queryparam){

    if (this.apiurl.isLiveApi) {
        var tollStoreData = this.getStoreData("TOLL");
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + tollStoreData.accesstoken
        }
        return this.http.get(this.apiurl.activityserv + "/users?" + queryparam, {
                headers: headers
            })
            .map(this.extractData);
    } else {
        return this.http.get('app/assets/json/cards-info.json')
            .map(this.extractData);
    }
}

  apiGetOperatorDetails(id){

    if (this.apiurl.isLiveApi) {
        var tollStoreData = this.getStoreData("TOLL");
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + tollStoreData.accesstoken
        }
        return this.http.get(this.apiurl.activityserv + "/users/" + id, {
                headers: headers
            })
            .map(this.extractData);
    } else {
        return this.http.get('app/assets/json/cards-info.json')
            .map(this.extractData);
    }

  }

  apiUpdateOperatorDetails(id, reqobj){

    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .put(this.apiurl.activityserv + "/users/" + id  , reqobj,{
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }

  }

  apiGetfeeCodes(queryparams){
      if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.domainserv + "/fee-codes" + queryparams ,{
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetLegalAgreements(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.businessserv + "/business-levels/" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }


  apiDeleteBranch(id) {
    if (this.apiurl.isLiveApi) {
    let tollStoreData = this.getStoreData("TOLL");
        let headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Authorization": "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .delete(this.apiurl.businessserv + "/business-levels/" + id  , {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }


      apiupdateFeestructure(reqobj){
          if (this.apiurl.isLiveApi) {
            let tollStoreData = this.getStoreData("TOLL");
                let headers = {
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
                "Authorization": "Bearer " + tollStoreData.accesstoken
              };
              return this.http
                .put(this.apiurl.domainserv + "/fee-revisions/" + reqobj.id  ,reqobj, {
                  headers: headers
                })
                .map(this.extractData);
            } else {
              return this.http
                .get("app/assets/json/cards-info.json")
                .map(this.extractData);
            }
          }

        jsonPendingActions(){
            return this.http
            .get("./../assets/json/data/jsonDataPendingActions.json")
                   .map(this.extractData);
          }

        jsonGetMessages(){
            return this.http
            .get("./../assets/json/data/messagesJsonData.json")
                   .map(this.extractData);
          }


          apiGetMerchantLocations(queryparam) {
            if (this.apiurl.isLiveApi) {
                let headers = {
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
              };
              return this.http
                .get(this.apiurl.businessserv + "/stores" + queryparam  , {
                  headers: headers
                })
                .map(this.extractData);
            } else {
              return this.http
                .get("app/assets/json/cards-info.json")
                .map(this.extractData);
            }
          }

          jsonGetMerchantLocations(){
              return this.http
              .get("./../assets/json/data/storeLocation.json")
              .map(this.extractData);
            }

  }

import { Injectable } 				from '@angular/core';
import { Subject }            from 'rxjs/Subject';
import { HttpClient, HttpHeaders } 	from '@angular/common/http';

import { Observable } 				from 'rxjs/Observable';
import { of } 						from 'rxjs/observable/of';
import { environment } 				from  '../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache'
   })
};

@Injectable()
export class CustomerService {

	  private apiurl = environment.apiurls;

    private extractData(res: Response) {

        return res || { };
    }

    private handleError(error: any) {

        return Observable.throw(error);
    }

    // select card communication
    private parentSelectCardChanged = new Subject<any>();
    private childSelectCardChanged = new Subject<any>();
    private rechargeSectionUpdate = new Subject<any>();

    // select card observables
    parentselected$ = this.parentSelectCardChanged.asObservable();
    childselected$ =  this.childSelectCardChanged.asObservable();
    rechargeUpdation$ = this.rechargeSectionUpdate.asObservable();


    updateParent(card){
       this.childSelectCardChanged.next(card);
    }

    updateChild(card: any){
      this.parentSelectCardChanged.next(card);
    }

    updateRechargeSection(){
      this.rechargeSectionUpdate.next();
    }

  	constructor(private http: HttpClient) { }



  	apiQuickRecharge (reqobj){
        if(this.apiurl.isLiveApi){

             return this.http.post( this.apiurl.activityserv + '/activities', reqobj, {headers: httpOptions.headers} )
	        .map(this.extractData);

        }else{
            console.log('live data false');
        }

    }

    auth : any;

    getToken (type){
				var store = localStorage.getItem('store');
				var storeInfo = JSON.parse(store);
            this.auth = {};
						this.auth.accessToken = (storeInfo && storeInfo.consumer && storeInfo.consumer.accesstoken) ? storeInfo.consumer.accesstoken : "";
						this.auth.refreshToken = (storeInfo && storeInfo.consumer && storeInfo.consumer.refreshtoken) ? storeInfo.consumer.refreshtoken : "";
						return this.auth[type];
		}

    setToken (auth){
			let store = localStorage.getItem('store');
      let storeInfo;
			if(store){
				 storeInfo = JSON.parse(store);
			}else{
				 //var storeInfo = this.apiServiceConfig.storeInfo;
          storeInfo = {
            "consumer" : {
              "accesstoken" : "",
              "refreshtoken" : ""
            }
          };
			}
				storeInfo.consumer.accesstoken = auth.accessToken ? auth.accessToken : "";
				storeInfo.consumer.refreshtoken = auth.refreshToken ? auth.refreshToken : "";

				localStorage.setItem('store', JSON.stringify(storeInfo));
      }

      apiAuthLogin (reqObj){
  				if(this.apiurl.isLiveApi){
  						var txt = "username="+ reqObj.phoneNumber+ "&password="+ reqObj.password +"&grant_type=password";
  						var headers = {
  								"Content-Type" : "application/x-www-form-urlencoded",
                  "Cache-Control": "no-cache"
  						}
  						return this.http.post(this.apiurl.authserv + '/token', txt, {headers: headers})
               .map(this.extractData)
               .catch(this.handleError);
  				}else{
  						return this.http.get('app/assets/json/login.json')
  						.map(this.extractData)
  				}

  		}

      apiSendOtp (reqobj, flag){
          let headers;
          if(this.apiurl.isLiveApi){
              if(flag){
                  var accessToken = this.getToken('accessToken');
                  headers = {
                      "Content-Type" : "application/json",
                      "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                      "Authorization" : "Bearer " + accessToken
                  }
              }else{
                  headers = {
                      "Content-Type" : "application/json",
                      'Cache-Control': 'no-cache'
                  }
              }

              return this.http.post(this.apiurl.authserv + '/send-otp', reqobj, {headers:headers})
              .map(this.extractData)
              .catch(this.handleError);
          }else{
              return this.http.get('./../assets/json/validate-otp.json')
              .map(this.extractData)
          }

      }


      apiGetGuestauthtoken (number, otp, type){
  				if(this.apiurl.isLiveApi){
  						if(type == "MOBILE"){
  								var txt = "username="+ number+ "&password="+ otp +"&grant_type=password";
  						}else{
  								var txt = "username="+ number+ "@doecards&password="+ otp +"&grant_type=password";
  						}

  						var headers = {
  								"Content-Type" : "application/x-www-form-urlencoded",
                  "Cache-Control": "no-cache"
  						}
  						return this.http.post(this.apiurl.authserv + '/token', txt, {headers: headers})
  						.map(function(response){ return response; })
  				}else{
  						return this.http.get('app/assets/json/cards-info.json')
  						.map(function(response){ return response; })
  				}
  		}

      apiForgotPassword (accessToken, reqobj){
  				if(this.apiurl.isLiveApi){
  						var headers = {
  								"Content-Type" : "application/json",
                 "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
  								"Authorization" : "Bearer " + accessToken
  						}
  						return this.http.post(this.apiurl.userserv + '/reset-password', reqobj,{headers:headers})
  						.map(function(response){ return response; })
  				}else{
  						return this.http.get('app/assets/json/register-user.json')
  						.map(function(response){ return response })
  				}
  		}

       apiGetCardsInfo(){
            if(this.apiurl.isLiveApi){
                var accessToken = this.getToken('accessToken');
                var headers = {
                    "Content-Type" : "application/json",
                    "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                    "Authorization" : "Bearer " + accessToken
                }
                return this.http.get(this.apiurl.cardserv + '/cards?statuses=LOST,ACTIVE', {headers:headers})
                .map(this.extractData)
            }

        }

      apiRegisterUser (reqobj, otp){
  				if(this.apiurl.isLiveApi){
  						var headers = {
  								"Content-Type" : "application/json",
                  "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
  								"X-DOE-OTP" : otp
  						}
  						return this.http.post(this.apiurl.userserv + '/register-carduser', reqobj,{headers:headers})
  						.map(this.extractData)
  				}else{
  						return this.http.get('app/assets/json/register-user.json')
  						.map(this.extractData)
  				}

  		}

      apiGetUserinfo(userid){
            if(this.apiurl.isLiveApi){
                var accessToken = this.getToken('accessToken');
                var headers = {
                    "Content-Type" : "application/json",
                    "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                    "Authorization" : "Bearer " + accessToken
                }
                return this.http.get(this.apiurl.userserv + '/users/' + userid, {headers: headers})
                .map(this.extractData)
            }else{
                return this.http.get('app/assets/json/user-info.json')
                .map(this.extractData)
            }

        }

        apiUpdateUserInfo (reqobj, password, id){
     			 var accessToken = this.getToken('accessToken');
     			 if(this.apiurl.isLiveApi){
     					 if(password){
     							 var headers = {
     									 "Content-Type" : "application/json",
                      "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
     									 "X-DOE-PASSWORD" : password,
     									 "Authorization" : "Bearer " + accessToken
     							 }
     							 return this.http.put(this.apiurl.userserv + '/users/' + id, reqobj,{headers:headers})
     							 .map(this.extractData)
     					 }
     			 }else{
     					 return this.http.get('app/assets/json/register-user.json')
     					 .map(this.extractData)
     			 }
     	 }

       apiChangePassword (reqobj){
    			 if(this.apiurl.isLiveApi){
    					 var accessToken = this.getToken('accessToken');
    					 var headers = {
    							 "Content-Type" : "application/json",
                   "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
    							 "Authorization" : "Bearer " + accessToken
    					 }
    					 return this.http.post(this.apiurl.userserv + '/change-password', reqobj,{headers:headers})
    					 .map(this.extractData)
    			 }else{
    					 return this.http.get('app/assets/json/quick-recharge.json')
    					 .map(this.extractData)
    			 }

    	 }
        //lostcard
        apiLostCard (reqobj, otp){
    				if(this.apiurl.isLiveApi){
    						var accessToken = this.getToken('accessToken');
    						var headers = {
    								"Content-Type" : "application/json",
                    "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
    								"X-DOE-OTP" : otp,
    								"Authorization" : "Bearer " + accessToken
    						}
    						return this.http.post(this.apiurl.cardserv + '/block-card', reqobj,{headers:headers})
    						.map(this.extractData)
    				}else{
    						return this.http.get('app/assets/json/lost-card.json')
    						.map(this.extractData)
    				}

    		}
      apiGetTransactionInfo(number){
        if(this.apiurl.isLiveApi){
            var accessToken = this.getToken('accessToken');
            var headers = {
                "Content-Type" : "application/json",
                "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                "Authorization" : "Bearer " + accessToken
            }

                return this.http.get(this.apiurl.activityserv + '/activities?card_number=' + number, {headers:headers})
                .map(this.extractData)

        }
      }

      apiUpdateCard(reqobj, xdoesec){
        if(this.apiurl.isLiveApi){
            var accessToken = this.getToken('accessToken');
            var headers = {
                "Content-Type" : "application/json",
                "Cache-Control": "no-cache",
                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                "Authorization" : "Bearer " + accessToken,
                "X-DOE-SECURITY-CONTEXT" : xdoesec
            }
            return this.http.post(this.apiurl.cardserv + '/update-card-profile',reqobj, {headers:headers})
            .map(this.extractData)
        }else{
            return this.http.get('app/assets/json/cards-info.json')
            .map(this.extractData)
        }

    }

    apiLinkCard(reqObj, otp){
        if(this.apiurl.isLiveApi){
            var accessToken = this.getToken('accessToken');
            var headers = {
                "Content-Type" : "application/json",
                "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                "Authorization" : "Bearer " + accessToken,
                "X-DOE-OTP" : otp
            }
            return this.http.post(this.apiurl.cardserv + '/link-card',reqObj, {headers:headers})
            .map(this.extractData)
        }else{
            return this.http.get('app/assets/json/cards-info.json')
            .map(this.extractData)
        }
    }

    apiUnlinkCard(reqObj){
        if(this.apiurl.isLiveApi){
            var accessToken = this.getToken('accessToken');
            var headers = {
                "Content-Type" : "application/json",
               "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                "Authorization" : "Bearer " + accessToken,
            }
            return this.http.post(this.apiurl.cardserv + '/unlink-card',reqObj, {headers:headers})
            .map(this.extractData)
        }else{
            return this.http.get('app/assets/json/cards-info.json')
            .map(this.extractData)
        }
    }

    apiUpdateMobileNumber (reqobj, otp){
             let accessToken = this.getToken('accessToken');
             if(this.apiurl.isLiveApi){
                     var headers = {
                             "Content-Type" : "application/json",
                             "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                             "X-DOE-OTP" : otp,
                             "Authorization" : "Bearer " + accessToken
                     }
                     return this.http.post(this.apiurl.userserv + '/update-mobile', reqobj,{headers:headers})
                     .map(this.extractData)
             }else{
                     return this.http.get('app/assets/json/register-user.json')
                     .map(this.extractData)
             }
     }

      apiGetGuestCardsInfo (accessToken, type, number){
            if(this.apiurl.isLiveApi){
                var headers = {
                    "Content-Type" : "application/json",
                    "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                    "Authorization" : "Bearer " + accessToken
                }
                if(type == "CARD"){
                    var url = "linked_user_statuses=ACTIVE,INACTIVE&card_number=" + number;
                }else{
                    var url = "linked_user_statuses=ACTIVE,INACTIVE&linked_user_types=PRIMARY";
                }
                return this.http.get(this.apiurl.cardserv + '/cards?' + url, {headers:headers})
                .map(this.extractData)
            }else{
                return this.http.get('app/assets/json/cards-info.json')
                .map(this.extractData)
            }

        }

         apiGuestBlockcard(accessToken, reqobj){
             if(this.apiurl.isLiveApi){
                var headers = {
                    "Content-Type" : "application/json",
                    "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                    "Authorization" : "Bearer " + accessToken
                }
                return this.http.post(this.apiurl.cardserv + '/block-card', reqobj,{headers:headers})
                .map(this.extractData)
            }else{
                return this.http.get('app/assets/json/cards-info.json')
                .map(this.extractData)
            }
        }

          apiFileUpload (reqobj){ //tol service // in parking service
            if(this.apiurl.isLiveApi){
                var headers = {
                    "Content-Type" : "image/jpeg",
                    "Cache-Control" : "no-cache"
                }
                return this.http.post(this.apiurl.contentserv + '/uploads', reqobj,{headers:headers})
                .map(this.extractData);
            }else{
                return this.http.get('app/assets/json/cards-info.json')
                .map(this.extractData);
            }
        }

        apiUpdateUserprofilePic(reqobj){
           if(this.apiurl.isLiveApi){
               var accessToken = this.getToken('accessToken');
               var headers = {
                   "Content-Type" : "application/json",
                  "Cache-Control": "no-cache",

                    "Pragma": "no-cache",
                    "Expires": "Sat, 01 Jan 2000 00:00:00 GMT",
                   "Authorization" : "Bearer " + accessToken,
               }
               return this.http.post(this.apiurl.userserv + '/update-profile-picture',reqobj, {headers:headers})
               .map(this.extractData);
           }else{
               return this.http.get('app/assets/json/cards-info.json')
               .map(this.extractData);
           }

       }

       jsonErrorMsgs(){
        return this.http
        .get("./../assets/json/error_msgs_customer.json")
               .map(this.extractData);
      }


      // user registration
      apiUserRegistration(reqObj){
      var headers = {
          "Content-Type" : "application/json",
          "Content-Security-Policy": "upgrade-insecure-requests",
      }
        return this.http.post("http://13.127.165.254/doe/support", reqObj, {headers:headers})
         .map(this.extractData);
      }






}

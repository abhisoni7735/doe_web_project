import { Injectable } 				from '@angular/core';
import { HttpClient, HttpHeaders } 	from '@angular/common/http';

import { Observable } 				from 'rxjs/Observable';
import { of } 						from 'rxjs/observable/of';
import { environment } 				from  '../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache'})
};

@Injectable()
export class Base64Service {

	private apiurl = environment.apiurls;
  Base64 : any;


  	constructor(private http: HttpClient) {
      var c1;
      var c2;
      var c3;
      var _this = this;
      this.Base64 = {
          _keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
          encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=this.Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this.Base64._keyStr.charAt(s)+this.Base64._keyStr.charAt(o)+this.Base64._keyStr.charAt(u)+this.Base64._keyStr.charAt(a)}return t},
          decode:function(e){
            let t="";
            let n,r,i;
            let s,o,u,a;
            let f=0;
            e=e.replace(/[^A-Za-z0-9+/=]/g,"");
            while(f<e.length){
              s=this._keyStr.indexOf(e.charAt(f++));
              o=this._keyStr.indexOf(e.charAt(f++));
              u=this._keyStr.indexOf(e.charAt(f++));
              a=this._keyStr.indexOf(e.charAt(f++));
              n=s<<2|o>>4;
              r=(o&15)<<4|u>>2;
              i=(u&3)<<6|a;
              t=t+String.fromCharCode(n);
              if(u!=64){t=t+String.fromCharCode(r)}
              if(a!=64){
                t=t+String.fromCharCode(i)
              }}
              t=_this.Base64._utf8_decode(t);
              return t
          },
          _utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},
          _utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};


    }

    decode (val){
         let userinfo = this.Base64.decode(val);
         return userinfo;
    }

}

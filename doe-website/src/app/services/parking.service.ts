import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { environment } from "../../environments/environment";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class ParkingService {
  private apiurl = environment.apiurls;
  private storeinfodata = environment.storeInfo;
  private auth: any;

  private extractData(res: Response) {
    return res || {};
  }

  private handleError(error: any) { 
    return Observable.throw(error);
  }

  private storeInfo: any;

  constructor(private http: HttpClient) {
    this.storeInfo = {
      toll: {
        //because we dont have purchase without login hence
        // "accesstoken" : "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NjYwNTQ5NTY4OTE3NzMwNjY4IiwiYnVzaW5lc3NQcm9maWxlIjp7IjExNjQ2NzgwNTMwNDI2Nzc5MzYiOjE1fSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTIzNDU2OTM5NzgyNTY5NX0.PxjNyUXyQh06RDx1HbC7x4OsnDyJLpbSQc5dLbdCdgE",
        accesstoken: "",
        refreshtoken: "",
        // "refreshtoken" : "4bb4bbf4a28f492c8f03d67adbd766f7",
        cartid: "",
        usertype: "TOLL-COMPANY"
      },

      parking: {
        accesstoken:
          "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNjAxODE3NDgzOTY5NjQ1NzY2IiwiYnVzaW5lc3NQcm9maWxlIjp7IjUwOTQ0NzQxNjk4NzIxMTc1NjUiOjE1fSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTIzNDU2OTM5MTE4MzU3NH0.IcO322FZJHfbSodUjjm5UCa27PCobbjTyn0eHZBBxmI",
        refreshtoken: "53169500f4e64ec3828e02f03dbd872b",
        cartid: "",
        usertype: "COMPANY"
      }
    };
  }

  getStoreData(moduleName) {
    switch (moduleName) {
      case "PARKING":
        let store = localStorage.getItem("store");
        //let store = localStorage.getItem('store');
        if (store) {
          let storeInfo = JSON.parse(store);
          let parkingstoreInfo = storeInfo.parking
            ? storeInfo.parking
            : this.storeInfo.parking;
          return parkingstoreInfo;
        } else {
          return this.storeInfo.parking;
        }
    } //switch closed here
  }

  /* Parking Guest flow starts here */

  apiFileUpload(reqobj) {
    //tol service // in parking service
    if (this.apiurl.isLiveApi) {
      let headers = {
        "Content-Type": "image/jpeg",
        "Cache-Control": "no-cache"
      };
      return this.http
        .post(this.apiurl.contentserv + "/uploads", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiRegisterParking(reqObj, type) {
    if (this.apiurl.isLiveApi) {
      if (type == "MAIN_BRANCH") {
        var headers = {
          "Content-Type": "application/json",
          Authorization: ""
        };
      } else {
        var accessToken = this.getToken("accessToken");
        var headers = {
          "Content-Type": "application/json",
          Authorization: "Bearer " + accessToken
        };
      }

      return this.http
        .post(this.apiurl.domainserv + "/register-toll", reqObj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  getToken(type) {
    let store = localStorage.getItem("store");
    let storeInfo = JSON.parse(store);
    this.auth = {};
    this.auth.accessToken =
      storeInfo && storeInfo.consumer && storeInfo.consumer.accesstoken
        ? storeInfo.consumer.accesstoken
        : "";
    this.auth.refreshToken =
      storeInfo && storeInfo.consumer && storeInfo.consumer.refreshtoken
        ? storeInfo.consumer.refreshtoken
        : "";
    return this.auth[type];
  }

  //  apiGetCartInfo (id, moduleName){ // in toll service

  // switch (moduleName) {
  //     case "TOLL":
  //     if(this.apiServiceConfig.serverUrls[this.apiServiceConfig.serverUrls.serverType].isLiveApi){
  //             var tollStoreData = this.getStoreData("TOLL");
  //             var url = id ? ('/carts/'+ id) : '/carts';
  //             var headers = {
  //                     "Content-Type" : "application/json",
  //                     "Authorization" : "Bearer " + tollStoreData.accesstoken
  //             }
  //             return this.http.get(this.apiServiceConfig.serverUrls[this.apiServiceConfig.serverUrls.serverType].checkoutserv + url, {headers:headers})
  //             .map(this.extractData)
  //     }else{
  //             return this.http.get('app/assets/json/cards-info.json')
  //             .map(this.extractData)
  //     }
  //         break;

  //         case "PARKING":
  //         if(this.apiServiceConfig.serverUrls[this.apiServiceConfig.serverUrls.serverType].isLiveApi){
  //                 var parkingStoreData = this.getStoreData("PARKING");
  //                 var url = id ? ('/carts/'+ id) : '/carts';
  //                 var headers = {
  //                         "Content-Type" : "application/json",
  //                         "Authorization" : "Bearer " + parkingStoreData.accesstoken
  //                 }
  //                 return this.http.get(this.apiServiceConfig.serverUrls[this.apiServiceConfig.serverUrls.serverType].checkoutserv + url, {headers:headers})
  //                 .map(this.extractData)
  //         }else{
  //                 return this.http.get('app/assets/json/cards-info.json')
  //                 .map(this.extractData)
  //         }
  //             break;
  // }
  //  }

  apiGetDevicesInfo(id, obj) {
    //in toll service
    if (this.apiurl.isLiveApi) {
      var url = id ? "/device-products/" + id : "/device-products";
      var headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .get(this.apiurl.deviceserv + url, { headers: headers })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetFAQInfo(moduleName) {
    // in toll
    switch (moduleName) {
      case "TOLL":
        if (this.apiurl.isLiveApi) {
          var tollStoreData = this.getStoreData("TOLL");
          var headers = {
            "Content-Type": "application/json"
          };
          return this.http
            .get(this.apiurl.contentserv + "/faq-topics?category=ONBOARDING", {
              headers: headers
            })
            .map(this.extractData);
        } else {
          return this.http
            .get("app/assets/json/cards-info.json")
            .map(this.extractData);
        }

      //      break;

      case "PARKING":
        if (this.apiurl.isLiveApi) {
          var tollStoreData = this.getStoreData("PARKING");
          var headers = {
            "Content-Type": "application/json"
          };
          return this.http
            .get(this.apiurl.contentserv + "/faq-topics?category=ONBOARDING", {
              headers: headers
            })
            .map(this.extractData);
        } else {
          return this.http
            .get("app/assets/json/cards-info.json")
            .map(this.extractData);
        }

      //break;
    }
  }

  apiGetFAQs(id) {
    if (this.apiurl.isLiveApi) {
      //var tollStoreData = this.getStoreData("TOLL");
      var headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .get(
          this.apiurl.contentserv + "/faqs?topic_id=" + id + "&statuses=ACTIVE",
          { headers: headers }
        )
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiGetCartInfo(id, moduleName) {
    // in toll service

    switch (moduleName) {
      case "PARKING":
        if (this.apiurl.isLiveApi) {
          var parkingStoreData = this.getStoreData("PARKING");
          var url = id ? "/carts/" + id : "/carts";
          var headers = {
            "Content-Type": "application/json"
          };
          return this.http
            .get(this.apiurl.checkoutserv + url, { headers: headers })
            .map(this.extractData);
        } else {
          return this.http
            .get("app/assets/json/cards-info.json")
            .map(this.extractData);
        }
      //break;
    }
  }

  getObjLength(obj) {
    let size = 0,
      key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  }

  apiUpdateCart(reqobj, id, moduleName) {
    switch (moduleName) {
      case "PARKING":
        if (this.apiurl.isLiveApi) {
          var tollStoreData = this.getStoreData("TOLL");
          var headers = {
            "Content-Type": "application/json"
          };
          return this.http
            .patch(this.apiurl.checkoutserv + "/carts/" + id, reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          console.log("this is live data");
        }

        break;
    }
  }

  apiCreateCart(reqobj, moduleName) {
    switch (moduleName) {
      case "PARKING":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json"
          };
          return this.http
            .post(this.apiurl.checkoutserv + "/carts", reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          // return this.http.get('app/assets/json/cards-info.json')
          //     .map(function(response) {
          //         return response.json();
          //     })
          console.log("this is live data");
        }
        break;
    }
  }

  setStoreData(data, moduleName) {
    switch (moduleName) {
      case "PARKING":
        let store = localStorage.getItem("store");

        if (store) {
          let storeInfo = JSON.parse(store);
          storeInfo.toll = data ? data : this.storeinfodata.toll;
          localStorage.setItem("store", JSON.stringify(storeInfo));
        } else {
          let tolldata = data ? data : this.storeinfodata.toll;
          localStorage.setItem("store", JSON.stringify(this.storeinfodata));
        }

        break;
    }
  }

  apiGetSkuDevices(skus) {
    if (this.apiurl.isLiveApi) {
      let headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .get(this.apiurl.deviceserv + "/device-products?skus=" + skus, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info-guest.json")
        .map(this.extractData);
    }
  }

  apiCreateOrder(reqobj) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .post(this.apiurl.checkoutserv + "/orders", reqobj, {
          headers: headers
        })
        .map(function(response) {
          return response;
        });
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(function(response) {
          return response;
        });
    }
  }

  apiGetOrderData(id) {
    if (this.apiurl.isLiveApi) {
      let tollStoreData = this.getStoreData("TOLL");
      let headers = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + tollStoreData.accesstoken
      };
      return this.http
        .get(this.apiurl.checkoutserv + "/orders/" + id, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  apiReplaceCart(reqobj, moduleName) {
    //in toll service

    if (this.apiurl.isLiveApi) {
      var parkingStoreData = this.getStoreData("PARKING"); //this.getTollStoreData;
      var headers = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + parkingStoreData.accesstoken
      };
      return this.http
        .put(this.apiurl.checkoutserv + "/carts/" + reqobj.id, reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }
  


  // Shan ---------------------------------------

  apiBussinessCategory() {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .get(this.apiurl.domainserv + "/product-specs")
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info-guest.json")
        .map(this.extractData);
    }
  }

  apiRegisterCompany(reqObj) {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .post(
          this.apiurl.mmsbusinessserv + "/register-business-level",
          reqObj,
          {
            headers: headers
          }
        )
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }


  apicheckusernameavailability(reqobj) {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json",
        username: reqobj.username
      };
      return this.http
        .get(this.apiurl.mmsuserserv + "/check-availability", {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }

  
  // checkusername() {
  //   if (
  //     this.company.login.details.primaryUser.loginIdentifiers.username
  //       .identifier
  //   ) {
  //     let reqobj = {
  //       username: this.company.login.details.primaryUser.loginIdentifiers
  //         .username.identifier
  //     };

  //     this.tollService.apicheckusernameavailability(reqobj).subscribe(
  //       response => {
  //         console.log(response);
  //       },
  //       error => {
  //         console.log(error);
  //       }
  //     );
  //   } else {
  //     console.log("kindly enter the username");
  //   }
  // }





  apiGuestCreateCart(reqobj, moduleName) {
    switch (moduleName) {
      case "PARKING":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json"
            // "Authorization": "tollStoreData.accesstoken"
          };
          return this.http
            .post(this.apiurl.mmscheckoutserv + "/carts", reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          // return this.http.get('app/assets/json/cards-info.json')
          //     .map(function(response) {
          //         return response.json();
          //     })
          console.log("this is live data");
        }
        break;
    }
  }

  apiGuestUpdateCart(reqobj, id, moduleName) {
    switch (moduleName) {
      case "PARKING":
        if (this.apiurl.isLiveApi) {
          let tollStoreData = this.getStoreData("TOLL");
          let headers = {
            "Content-Type": "application/json"
            // "Authorization": tollStoreData.accesstoken
          };

          return this.http
            .patch(this.apiurl.mmscheckoutserv + "/carts/" + id, reqobj, {
              headers: headers
            })
            .map(this.extractData);
        } else {
          console.log("this is live data");
        }

        break;
    }
  }

  apiFileIDPatch(id, CompanyID) {
    if (this.apiurl.isLiveApi)
      var headers = {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache"
      };

    return this.http
      .patch(
        this.apiurl.mmsbusinessserv + "/business-levels/" + CompanyID,
        id,
        {
          headers: headers
        }
      )
      .map(this.extractData);
  }



  apirequestCards(reqobj) {
    if (this.apiurl.isLiveApi) {
      var headers = {
        "Content-Type": "application/json"
      };
      return this.http
        .post(this.apiurl.mmssupportserv + "/tickets", reqobj, {
          headers: headers
        })
        .map(this.extractData);
    } else {
      return this.http
        .get("app/assets/json/cards-info.json")
        .map(this.extractData);
    }
  }
  
}
//

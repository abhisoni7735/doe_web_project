import { Injectable } 				from '@angular/core';
import {CanActivate, Router}  from "@angular/router";
import { TollService }        from "./../services/toll.service";

@Injectable()
export class TollAuthGuard implements CanActivate {
  constructor(private tollService: TollService, private route: Router){
  }

  canActivate() {
    let tollStoreData = this.tollService.getStoreData("TOLL");
    if(tollStoreData.accesstoken){
    return true;
    }else{
          this.route.navigate(['./toll/login']);
    }
  }

}

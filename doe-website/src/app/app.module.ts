import { NgModule }             from '@angular/core';
import { BrowserModule}        from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule }          from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule }     from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { NgxCaptchaModule } from 'ngx-captcha';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { DirectivesModule }     from './directives/directives.module';
import { CommonBaseModule }         from './common/common.module';
import { AppRoutingModule }     from './routes/app-routing.module';
import { ServicesModule }       from './services/services.module';

import { AppComponent }         from './app.component';
import { TermsOfServicesComponent }         from './termsOfServices/termsOfServices.component';
import { PrivacyPolicyComponent }         from './privacyPolicy/privacyPolicy.component';
import { AboutUsComponent }         from './aboutus/about-us.component';
import { ContactusComponent }         from './contactus/contactus.component';
import { PartnersComponent }         from './partners/partners.component';
import { CustomerContactComponent }  from './register/customer-register.component';
import { ConsumerJoinusComponent }   from './consumer-joinus/consumer-joinus.component';
import { MerchantJoinusComponent }   from './merchant-joinus/merchant-joinus.component';
import { PageNotFoundComponent }              from './404/404.component';



import { CommonService }        from './services/common.service';
import {PipesModule}             from './pipes/pipes.module';

import { MnFullpageModule } from "ngx-fullpage";


import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { AgmCoreModule } from '@agm/core';

import 'chart.js/dist/Chart.bundle.js';

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'doe-website' }),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBrjTZIGsRLPJp_83Xr39DMADWJsP5HwIQ',
      libraries: ["places"]

    }),
    MnFullpageModule.forRoot(),
    DirectivesModule,
    CommonBaseModule,
    ServicesModule,
    NoopAnimationsModule,
    PipesModule,
    NgxCaptchaModule
    ],
  declarations: [
    AppComponent,
    TermsOfServicesComponent,
    PrivacyPolicyComponent,
    AboutUsComponent,
    CustomerContactComponent,
    PartnersComponent,
    ConsumerJoinusComponent,
    ContactusComponent,
    MerchantJoinusComponent,
    PageNotFoundComponent
    ],
  providers: [CommonService],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'on the server' : 'in the browser';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}

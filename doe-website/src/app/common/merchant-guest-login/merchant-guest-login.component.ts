import { Component, ViewChild, EventEmitter, Output}              from '@angular/core';
import { Observable }                        from 'rxjs/Observable';
import { ActivatedRoute }                    from '@angular/router';
import { RouterModule, Routes, Router }      from '@angular/router';
import { TollService }                       from "../../services/toll.service";
import { Base64Service }                     from '../../services/base64.service';
import { CustomerService }                   from '../../services/customer.service';
import { CommonService }                     from '../../services/common.service';

@Component({
  selector: 'merchant-guest-login',
  template: `<div class="modal_popup modal_popup_guestLogin"  data-backdrop="false" id="loginpopup" *ngIf = "loginpopup">
    <button type="button" class=" guest_lgoin_close" data-dismiss="modal" aria-label="Close" (click) = "loginpopup = false">
      <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal__wrapper">
      <div class="modal__header">
      </div>
      <div class="modal__content modal__content__guest_page">
        <div class="guest_login_popup">
          <div class="guest_login_popup_form">
            <form class="guest_login_form_section" (ngSubmit)="userLogin()">
              <h3>LOGIN</h3>
              <div class="guest_login_popup_fields">
              <input type="text" name="" value="" [(ngModel)]="login.phoneNumber" [ngModelOptions]="{standalone: true}" placeholder="Username">
                <span>Forgot?</span>
              </div>
              <div class="guest_login_popup_fields">
              <input type="text" name="" value="" [(ngModel)]="login.password" [ngModelOptions]="{standalone: true}" placeholder="Password">
                <span>Forgot?</span>
              </div>
              <div class="guest_login_radio_button">
                <input type="radio" name="radio-group" id="stayedin" value="">
                <label for="stayedin">Stay Logged in</label>
              </div>
              <button type="submit" name="button" class="guest_login_popup_btn" >Login</button>
              <p class="no_account">Not Account Yet? <span>Sign up</span> here</p>
            </form>
          </div>
          <div class="or_section">
            <span>or</span>
          </div>
          <div class="guest_login_popup_countinue" *ngIf="!createdetails">
            <h3>CONTINUE AS GUEST</h3>
            <button class="guestlogin_continue_btn" type="button" name="button" (click)=guestCreateCartTriggerer()>Continue</button>
          </div>
          <div class="guest_login_popup_countinue" *ngIf="createdetails">
            <p class="same__login_details">It Seems that your details are already avilable in our system.Please <span>login</span> to proceed.</p>
          </div>
        </div>
      </div>
    </div>
  </div>`
})



export class MerchantGuestLoginComponent {
  // @HostBinding('style.display') display = 'absolute';
  // @HostBinding('left')

  @Output() userCreateCart: EventEmitter<any>  = new EventEmitter();
  @Output() guestCreateCart: EventEmitter<any>  = new EventEmitter();
  @Output() userCreateOrder: EventEmitter<any>  = new EventEmitter();

  guestCreateCartTriggerer(){
    this.guestCreateCart.emit(null);
    this.hide();
  }

  userCreateCartTriggerer(pagetype){
    switch(pagetype) {
      case "cart":
      this.userCreateCart.emit(null);
      break;
      case "order":
      this.userCreateOrder.emit(null);
      break;
    }
  }


  login : any;
  tollLand : any;
  device : any;
  error: any;
  tollstoreInfo: any;
  id: any;
  getdevice: any;
  deviceinfo: any;
  cartinfodata: any;
  cartcreate:any;
  loginpopup : any;
  routeparms :any;
  validation : any;
  auth : any;
  merchantuserinfo : any;
  accesstoken : any;
  tokenid : any;
  testFlag: any;
  createdetails : any;

  constructor(private tollService :TollService, private commonservice :CommonService, private customerservice :CustomerService, private activateroute: ActivatedRoute, private route : Router, private base64 : Base64Service,) {

    this.loginpopup = false;
    this.createdetails = false;
    this.tollstoreInfo = this.tollService.getStoreData("TOLL");
    this.login = {
      "phoneNumber" : "",
      "password" : ""

    };
    this.device = {
    "cartData" : {}
    };
    this.tollLand = {};
    this.tollLand.userinfo = {};
    this.device.deviceinfo = {};
    this.device.deviceinfo.items = [];
    this.device.cartcount = 0;
    this.device.quantity = 1;
    this.error = {};
    this.error.quantity = false;
    this.deviceinfo = {};
    this.deviceinfo.images = {};
   }

  show(type) {
    this.loginpopup = true;
    console.log("I am in geust login --------->");
    console.log(this.loginpopup);
    switch(type) {
      case "cart":
      this.createdetails = false;
      break;
      case "order":
      this.createdetails = true;
      break;
    }
  }

  hide() {
    this.loginpopup = false;
  }

  updateStore (auth, type , service){
    let data = {
      "accesstoken" : auth.accessToken,
      "refreshtoken" : auth.refreshToken,
      "cartid" : "",
      "usertype" : type
    };
    this.tollService.setStoreData(data, service);
  }

  userLogin(){
      // this.validation.passwordError = false;
      // this.validation.userTypeError = false;
      this.commonservice.apiMerchantlogin(this.login)
      .subscribe(response =>{
        // this.validation.passwordError = false;
        // console.timeEnd(response);
        this.auth = response;
        let accessToken = this.auth.accessToken;
        let refreshToken = this.auth.refreshToken;

        let str = accessToken.split('.');
        let decodestr = str[1];
        let userinfo = this.base64.decode(decodestr);
        console.log(userinfo);
        let s = userinfo.replace(/\\n/g, "\\n")
           .replace(/\\'/g, "\\'")
           .replace(/\\"/g, '\\"')
           .replace(/\\&/g, "\\&")
           .replace(/\\r/g, "\\r")
           .replace(/\\t/g, "\\t")
           .replace(/\\b/g, "\\b")
           .replace(/\\f/g, "\\f");

        s = s.replace(/[\u0000-\u0019]+/g,"");
        let obj = JSON.parse(s);
         console.log("After Login -------------->");
         console.log(obj);
         console.log(this.auth);
         this.tollstoreInfo = this.tollService.getStoreData("TOLL");
         console.log(this.tollstoreInfo);
         let authobj = {
           "accesstoken" : accessToken,
           "refreshtoken" : refreshToken,
           "cartid" : this.tollstoreInfo.cartid,
           "usertype" : this.tollstoreInfo.usertype

         };
         this.tollService.setStoreData(authobj,"TOLL");
         console.log("after setting the accesstoken");
         this.tollstoreInfo = this.tollService.getStoreData("TOLL");
         console.log(this.tollstoreInfo);

        setTimeout(this.userCreateCartTriggerer("cart"));


        //this.setData(obj,response);
        //this.updateStore(this.auth, "TOLL-COMPANY", "TOLL");
        this.loginpopup = false;

      }, error =>{
          console.log(error);
           if (error.status == '400'){
            // this.validation.passwordError = true;
          }
      });

  }

  redirect(dbType, businessProfileId){
    switch (dbType) {
        case "TOLL-COMPANY":
        case "TOLL-CONTRACTOR" :
        case "MULTIPLE-COMPANY":
            this.route.navigate(['/toll/company', businessProfileId]);
        break;
        case "TOLL-COMPANY-BRANCH" :
        case "TOLL-CONTRACTOR-BRANCH" :
        case "TOLL-COMPANY-TOLL_PLAZA":
            this.route.navigate(['/toll/branch', businessProfileId]);
        break;
   
    }
  }


  gotoDB(){
    this.tollLand.userinfo = this.tollService.getStoreData("TOLL");
    if(this.tollLand.userinfo && this.tollLand.userinfo.accesstoken && this.tollLand.userinfo.usertype){
         let businesslevelid = this.getbusinessuserid('BUSINESSID');
        this.redirect(this.tollLand.userinfo.usertype,businesslevelid);
    }else if(this.tollLand.userinfo && this.tollLand.userinfo.accesstoken){
        let userId = this.getbusinessuserid('USERID');
        this.tollService.apiGetMerchantUserinfo(userId, this.tollLand.userinfo.accesstoken)
        .subscribe( response =>{
            this.merchantuserinfo = response;
            console.log("In business case -------------->");
            console.log(this.merchantuserinfo);
            let dbType = (this.merchantuserinfo && (this.merchantuserinfo.businessProfile) && this.merchantuserinfo.businessProfile && this.merchantuserinfo.businessProfile.businessLevel && this.merchantuserinfo.businessProfile.businessLevel.fullyQualifiedType) ? this.merchantuserinfo.businessProfile.businessLevel.fullyQualifiedType : "";
            let businesslevelid = this.getbusinessuserid('BUSINESSID');
             this.redirect(dbType,businesslevelid);
          })
    }
  }

  getbusinessuserid(type){
      let accessToken = this.tollLand.userinfo.accesstoken;
      let str = accessToken.split('.');
      let decodestr = str[1];
      let userinfo = this.base64.decode(decodestr);
      console.log(userinfo);
      let s = userinfo.replace(/\\n/g, "\\n")
         .replace(/\\'/g, "\\'")
         .replace(/\\"/g, '\\"')
         .replace(/\\&/g, "\\&")
         .replace(/\\r/g, "\\r")
         .replace(/\\t/g, "\\t")
         .replace(/\\b/g, "\\b")
         .replace(/\\f/g, "\\f");

      s = s.replace(/[\u0000-\u0019]+/g,"");
      let obj = JSON.parse(s);
      let userId = obj.sub;
      let businesslevelid = Object.keys(obj.businessProfile)[0];

      switch(type){
        case 'USERID':
          return userId;
        case "BUSINESSID":
          return businesslevelid;
      }

  }



  ngOnInit() { }

}

import { Component, Inject, PLATFORM_ID } from '@angular/core';
declare var require: any;
import { isPlatformBrowser, isPlatformServer } from '@angular/common';


@Component({
  selector: 'modal',
  templateUrl: './modal.component.html'
})

export class CommonModal {

   onlineStatus : boolean;
   popupShow : boolean
    constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    	this.onlineStatus = navigator.onLine;

    }

    ngOnInit() { }

    show(){
        this.onlineStatus = navigator.onLine
         console.log(this.onlineStatus);
          if (isPlatformBrowser(this.platformId)) {
           //Client only code.
            let $ = require('jquery');
            $('#serverDownErrorPopup').show();
          }
        
    }

    close(){
      if (isPlatformBrowser(this.platformId)) {
           //Client only code.
            let $ = require('jquery');
            $('#serverDownErrorPopup').hide();
          }
    	

    }
}

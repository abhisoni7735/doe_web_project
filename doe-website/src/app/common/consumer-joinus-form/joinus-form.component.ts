import { Component, ViewChild, OnInit,ChangeDetectorRef,
  ChangeDetectionStrategy} from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonModal } from "../modal/modal.component";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { ReCaptcha2Component} from 'ngx-captcha'


@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'consumer-joinus-form',
    templateUrl: './joinus-form.html',
    providers: [CommonModal]
})
  export class ConsumerJoinusFormComponent  implements OnInit {

   reEnterPassword : any;
   error : any;
   sentOtp : boolean;
   otp : any;
   showRegisterPopup : any;
   userformData : any;
   errobj : any;
   signuperror : any;
   userresponse : any;

  joinusForm: FormGroup;

  public readonly siteKey = '6LfzIOUUAAAAAIxUSrBW2rN18LaEjw77i_xPF8tK';
  public captchaIsLoaded = false;
  public captchaSuccess = false;
  public captchaIsExpired = false;
  public captchaResponse?: string;

  public theme: 'light' | 'dark' = 'light';
  public size: 'compact' | 'normal' = 'normal';
  public lang = 'en';
  public type: 'image' | 'audio';
  //public useGlobalDomain: boolean = false;

  formErrors = {
      'name': '',
      "email": '',
      'textarea': '',
      'mobileNumber': '',
      'clienttype': '',
      'recaptcha': '',
      'location': '',
    };
  // Form Error Object
 validationMessages = {
   'name': {
     'required': 'Name required',
     'pattern' : 'Enter valid name. Name should contain only characters'
   },
   "email": {
      'pattern' : 'Enter valid mail Id',
      'required':'Please enter email'
    },

   'mobileNumber': {
     'required': 'Mobile Number required',
     'pattern' : 'Enter valid 10 digit mobile number'
   },
   'clienttype': {
     'required' : 'Please Select User Type',
   },
   'recaptcha': {
     'required' : 'Please checkmark the captcha',
   },
   'location': {
     'pattern' : 'Enter Valid location',
   }
 };

initForm() {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  this.joinusForm = this.fb.group({
     name: ['', [Validators.required, Validators.pattern('^([a-zA-Z\s ]{3,32})$')]],
     email:['',[Validators.pattern(re)]],
     mobileNumber: ['',[Validators.required, Validators.pattern('[0-9]{10}')]],
     textarea: [''],
     // location: ['', [Validators.pattern('^([a-zA-Z\s ]{3,32})$')]],
     clienttype: ['CONSUMER'],
     recaptcha: ['', Validators.required],

  });


  this.joinusForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
  this.onValueChanged(); // (re)set validation messages now
 }

 // Reactive form Error Detection
  onValueChanged(data?: any) {
    if (!this.joinusForm) { return; }
      const form = this.joinusForm;
      for (const field in this.formErrors) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            let msg = messages[key] ? messages[key] : '';
            this.formErrors[field] += msg + ' ';
          }
        }

      }

    }

 onSubmit() {
   if(!this.joinusForm.valid){
     console.log("Form Is not Valid-------->");
     console.log(this.formErrors);
     if (!this.joinusForm) { return; }
     const form = this. joinusForm;
     for (const field in this.formErrors) {
       // clear previous error message (if any)
       this.formErrors[field] = '';
       const control = form.get(field);
       if (control && !control.valid) {
         const messages = this.validationMessages[field];
         for (const key in control.errors) {
           this.formErrors[field] += messages[key] + ' ';
         }
       }
     }
   }
  if(this.joinusForm.valid)
  {console.log("Form Is Valid-------->");

   console.log(this.formErrors);
    this.userformData = {
      "name" : this.joinusForm.value.name,
    	"email" : this.joinusForm.value.email,
    	"mobile" : this.joinusForm.value.mobileNumber,
    	"userType" : this.joinusForm.value.clienttype,
    	"comments" : this.joinusForm.value.textarea,
    }
   this.userRegister();

 }

}

@ViewChild('captchaElem') captchaElem: ReCaptcha2Component;


  constructor ( private cdr: ChangeDetectorRef, private customerservice :CustomerService, private route : Router, private fb: FormBuilder) {
    // this.userformData = {
    //   "name" : " ",
    //   "email" : " ",
    //   "mobile" : " ",
    //   "userType" : " ",
    //   "comments" : " "
    // }
    this.showRegisterPopup = {};
    this.showRegisterPopup.show = false;
   }

   userRegister(){
     //console.log(this.userformData);
     this.customerservice.apiUserRegistration(this.userformData)
     .subscribe((response)=>{
       this.userresponse = response;
       // alert('successfully sent');
       this.showRegisterPopup.show = true;
       this.initForm();
        this.captchaElem.reloadCaptcha();
        this.cdr.detectChanges();
     },(error)=>{
       console.log('here it is error data',this.userformData);
     });
   }


   handleReset(): void {
    this.captchaSuccess = false;
    this.captchaResponse = undefined;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
  }

  handleSuccess(captchaResponse: string): void {
    this.captchaSuccess = true;
    this.captchaResponse = captchaResponse;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
  }

  handleLoad(): void {
    this.captchaIsLoaded = true;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
  }

  handleExpire(): void {
    this.captchaSuccess = false;
    this.captchaIsExpired = true;
    this.cdr.detectChanges();
  }


   ngOnInit() {
     this.initForm();
   }

}

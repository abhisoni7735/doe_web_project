import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';


@Component({
	selector: 'guest-header',
  templateUrl: './header.component.html'
})

export class GuestHeaderComponent {
	showPopup : any;
 header : any;
 serviceName: any;
  constructor(private activatedroute: ActivatedRoute, private route : Router) {
  		this.header = {
  			"currentCityName" : "Banglore"
  		};
			this.showPopup = false;
   }

	 clickedService(servicename){
		 this.serviceName = servicename;
		 console.log('this is clicked', servicename);
		 this.showPopup = true;
	 }

	 serviceyse(type){
		 console.log('this is clicked type', type);
		 switch(type){
			 case 'RETAIL':
			 this.route.navigate(['/retail']);
			 break;
			 case 'TOLL':
			 this.route.navigate(['/toll']);
			 break;
			 case 'METRO':
			 this.route.navigate(['/metro']);
			 break;
			 case 'PARKING':
			 this.route.navigate(['/parking']);
			 break;
			 case 'BUS':
			 this.route.navigate(['/bus']);
			 break;
			 case 'RESIDENCE':
			 this.route.navigate(['/residence']);
			 break;
			 case 'EDUCATION':
			 this.route.navigate(['/education']);
			 break;
			 case 'OFFICE':
			 this.route.navigate(['/office']);
			 break;
		 }
	 }

  ngOnInit() { }

  scrollToLocation(){

  }
}

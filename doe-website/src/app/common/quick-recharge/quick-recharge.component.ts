import { Component,OnInit } from '@angular/core';
import { Observable }         from 'rxjs/Observable';
import { CommonService } from "../../services/common.service";
import { CommonModal } from "../modal/modal.component";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'quick-recharge',
  templateUrl: './quick-recharge.component.html',
  providers: [CommonModal]
})

export class QuickRechargeComponent implements OnInit {

  isfivehActive : boolean;
  isthousandActive : boolean;
  istwothousandActive : boolean;
  onrechargepage : boolean;
  showrechargePopup : any;
  recharge : any;
  error : any;
  quickRechargeFg: FormGroup;

  formErrors = {
      'quickRechargeAmmount': '',
      'quickRechargeCardNumber': '',
      'quickRechargeCardNumberConfirm': ''
    };
  // Form Error Object
 validationMessages = {
   'quickRechargeAmmount': {
     'required': 'Amount required',
     'pattern' : 'Enter valid Amount. Amount should contain only numbers'
   },
   'quickRechargeCardNumber': {
     'required': 'Card Number Required',
     'pattern' : 'Enter valid card number'
   },
   'quickRechargeCardNumberConfirm': {
     'required': 'Confirm Card Number Required',
     'pattern' : 'Enter valid card number'
   }
 };

ngOnInit() {
  this.initForm();
}

initForm() {
  this.quickRechargeFg = this.fb.group({
     quickRechargeAmmount: ['', [Validators.required, Validators.pattern('^((?!(0))[0-9]{1,6})$')]],
     quickRechargeCardNumber: ['', [Validators.required, Validators.pattern('[0-9]{16}')]],
     quickRechargeCardNumberConfirm: ['',[Validators.required, Validators.pattern('[0-9]{16}')]],

  });

  this.quickRechargeFg.valueChanges
    .subscribe(data => this.onValueChanged(data));
  this.onValueChanged(); // (re)set validation messages now
 }

 // Reactive form Error Detection
  onValueChanged(data?: any) {
      if (!this.quickRechargeFg) { return; }
      const form = this. quickRechargeFg;
      for (const field in this.formErrors) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }

      }

    }

 onSubmit() {

   if(!this.quickRechargeFg.valid){
     console.log("Form Is not Valid-------->");
     if (!this.quickRechargeFg) { return; }
     const form = this. quickRechargeFg;
     for (const field in this.formErrors) {
       // clear previous error message (if any)
       this.formErrors[field] = '';
       const control = form.get(field);
       if (control && !control.valid) {
         const messages = this.validationMessages[field];
         for (const key in control.errors) {
           this.formErrors[field] += messages[key] + ' ';
         }
       }
     }
   }
  if(this.quickRechargeFg.valid)
  {console.log("Form Is Valid-------->");

   console.log(this.formErrors);

       this.recharge.amount = this.quickRechargeFg.value.quickRechargeAmmount;
       this.recharge.cardNumber = this.quickRechargeFg.value.quickRechargeCardNumber;
       this.recharge.confirmcardNumber = this.quickRechargeFg.value.quickRechargeCardNumberConfirm;
       this.quickRecharge();

 }

}


  constructor(private commonservice :CommonService , private modal: CommonModal, private fb: FormBuilder) {
    this.isfivehActive = false;
    this.isthousandActive = false;
    this.istwothousandActive = false;
    this.setDefaults();
  }

  setDefaults(){
    this.onrechargepage = false;
    this.isfivehActive = false;
    this.isthousandActive = false;
    this.istwothousandActive = false;
    this.recharge = {
      "amount" : "",
      "cardNumber" : "",
      "confirmcardNumber" : ""
    };
    this.error = {
      "showMessage" : false,
      "message" : ""
    };

    this.initForm();
  }


  selectAmount(value){
        switch(value){
           case "500" :
            this.isfivehActive = true;
            this.isthousandActive = false;
            this.istwothousandActive = false;
            this.recharge.amount = value;
            this.quickRechargeFg.get('quickRechargeAmmount').setValue( this.recharge.amount, {emitEvent: false});

           break;
           case "1000" :
           this.isfivehActive = false;
            this.isthousandActive = true;
            this.istwothousandActive = false;
            this.recharge.amount = value;
            this.quickRechargeFg.get('quickRechargeAmmount').setValue( this.recharge.amount, {emitEvent: false});
           break;
           case "2000" :
            this.isfivehActive = false;
            this.isthousandActive = false;
            this.istwothousandActive = true;
            this.recharge.amount = value;
            this.quickRechargeFg.get('quickRechargeAmmount').setValue( this.recharge.amount, {emitEvent: false});
           break;
        }
  }

   quickRecharge(){
        if(this.recharge.cardNumber == this.recharge.confirmcardNumber){
            this.error.showMessage = false;
            console.log(this.recharge.amount, this.recharge.cardNumber);
            let dt = new Date();
            let date1 = dt.toISOString();
            let reqObj = {
              "cardNumber": this.recharge.cardNumber,
              "serviceCode": "CTOPUP",
              "chargeId": "3477",
              "createdBy": "4578507363558053213",
              "readerId": "19472",
              "activityTime": date1,
              "amount": {
                  "currency" : "INR",
                  "value" : this.recharge.amount
              },
              "mode": "CONSUMER"
            }

            this.commonservice.apiQuickRecharge(reqObj)
            .subscribe(
                response => {
                    this.showrechargePopup = true;
                    this.initForm();
                },
                error => {
                    console.log(error)
                    if(error.error.name== "SOURCE_CARD_NOT_FOUND" ||error.error.name == "ACTIVE_CARD_NOT_FOUND" ||error.error.name =="INVALID_ACTION_MODE" || error.error.name =="DEVICE_NOT_FOUND" || error.error.name == "DEBIT_LIMIT_EXCEEDED" || error.error.name == "BUSINESS_MIN_BALANCE_AMOUNT_VIOLATED"){
                      this.error.message = error.error.message ? error.error.message : "Kindly provide valid information.";
                      this.error.showMessage = true;

                    }else{
                      this.modal.show();
                      console.log(error._body);
                    }
                });
        }else{
            this.error.message = "Card number and confirm card number mismatch";
            this.error.showMessage = true;
        }

      }

      deselectPredifinedValues(){
        this.isfivehActive = false;
        this.isthousandActive = false;
        this.istwothousandActive = false;
      }

}

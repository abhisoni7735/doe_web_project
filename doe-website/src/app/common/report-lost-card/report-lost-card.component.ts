import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../services/common.service';
import { CustomerService } from '../../services/customer.service';
import { CommonModal } from "../modal/modal.component";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';




@Component({
  selector: 'lost-card',
  templateUrl: './report-lost-card.component.html',
  providers: [CommonModal]
})

export class ReportLostCardComponent implements OnInit{

    selectedcard : any;
    blockCard : any;
    lostcard : any;
    userCards :any;
    error : any;
    card: any;
    accesstoken : any;
    authtokenresponse: any;
    authtokenerror:any;
    guestcardsresponse: any;
    guestcardserror:any;
    sendotperror : any;
    blockcardresponse : any;
    blockcarderror:any;
    lost_card : FormGroup;
    confomationdata:any;

    formErrors = {
        'mobilenumber': '',
        'otp':''
      };
    // Form Error Object
    validationMessages = {
     'mobilenumber': {
       'required': 'Mobile Number or Card number required',
       'pattern' : 'Enter valid mobile Number/Card Number'
     },
     'otp': {
       'required': 'OTP required',
       'pattern' : 'Please enter a valid OTP'
      },

    };


    initForm() {
    if(!this.lostcard.isotpSent){
    this.lost_card = this.fb.group({
       mobilenumber: ['', [Validators.required, Validators.pattern('^([0-9]{10,16})$')]],
    });
    }

    if(this.lostcard.isotpSent){
      this.lost_card = this.fb.group({
         mobilenumber: [this.lostcard.mobileNumber, [Validators.required, Validators.pattern('^([0-9]{10,16})$')]],
         otp: ['', [Validators.required]],

      });

    }
    this.lost_card.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
    }


    // Reactive form Error Detection
      onValueChanged(data?: any) {
        if (!this.lost_card) { return; }
        const form = this. lost_card;
        for (const field in this.formErrors) {
          // clear previous error message (if any)
          this.formErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }

      onlostcardSubmit() {

       if(!this.lost_card.valid){
         console.log("Form Is not Valid-------->");
         if (!this.lost_card) { return; }
         const form = this. lost_card;
         for (const field in this.formErrors) {
           // clear previous error message (if any)
           this.formErrors[field] = '';
           const control = form.get(field);
           if (control && !control.valid) {
             const messages = this.validationMessages[field];
             for (const key in control.errors) {
               this.formErrors[field] += messages[key] + ' ';
             }
           }
         }
       }
      if(this.lost_card.valid)
      {console.log("Form Is Valid-------->");

       console.log(this.formErrors);
          if(!this.lostcard.isotpSent) {
            this.lostcard.mobileNumber = this.lost_card.value.mobilenumber;
          }
           if(this.lostcard.isotpSent){
             this.lostcard.otp = this.lost_card.value.otp;
           }
           this.initiateOtp();

      }

      }


  constructor(private commonService: CommonService, private customerservice: CustomerService, private modal: CommonModal, private fb: FormBuilder) {
    this.selectedcard = {};
    this.blockCard = {};
    this.blockCard.code = "";
    this.card = {};
    this.lostcard = {
      "mobileNumber" : "",
      "otp" : "",
      "isotpSent" : false,
      "showcards" : false,
      "inputtype" : "MOBILE"
    };
    this.accesstoken ="";
    this.userCards =[];
    this.error = {
      "message" : "",
      "showMessage" : false,
      "showselectcardMessage" :false,
      "selectcardmessage" : ""
    };
    this.confomationdata = false;
  }



  setNormal(){
    this.card = {};
      this.blockCard = {};
      this.blockCard.code = "";
      this.error.showMessage = false;
      this.lostcard = {
        "mobileNumber" : "",
        "otp" : "",
        "isotpSent" : false,
        "showcards" : false
      };
      this.userCards =[];
      this.initForm();
      this.confomationdata = false;
      this.selectedcard = {};

  }
  // conformationdatamessage(){
  //   this.confomationdata = true;
  //   this.lostcard.showcards = false;
  //   this.lostcard.isotpSent = false;
  //   this.lost_card.valid = false;
  // }

  initiateOtp(){
    if(this.lostcard.isotpSent){
       this.error.showMessage = false;
      this.customerservice.apiGetGuestauthtoken(this.lostcard.mobileNumber, this.lostcard.otp, this.lostcard.inputtype)
          .subscribe( response => {
            console.log(response);
                this.authtokenresponse = response;
                this.accesstoken = this.authtokenresponse.accessToken;
                this.customerservice.apiGetGuestCardsInfo(this.accesstoken, this.lostcard.inputtype, this.lostcard.mobileNumber)
                .subscribe(response  => {
                  this.guestcardsresponse = response;
                  if(this.lostcard.inputtype == 'CARD' && this.guestcardsresponse.cards.length == 1 && this.guestcardsresponse.cards[0].status && this.guestcardsresponse.cards[0].status == 'LOST'){
                      this.error.message = "Given card is already Blocked.";
                       this.error.showMessage = true;
                       this.lostcard.isotpSent = false;
                       this.initForm();
                  }else{
                    this.userCards = this.guestcardsresponse.cards;
                    this.lostcard.showcards = true;
                    this.error.showMessage = false;
                    if(this.userCards.length == 0){
                        this.error.message = "No Cards linked to this account.";
                       this.error.showMessage = true;
                    }
                  }

                }, error =>{
                  this.guestcardserror = error;
                  this.error.message = this.guestcardserror.error.message ?  this.guestcardserror.error.message : "Kindly provide valid information.";
                   this.error.showMessage = true;
                })
        },err => {
            this.authtokenerror = err;

            if (this.authtokenerror.error.name == "INVALID_CREDENTIALS" || this.authtokenerror.error.name == "INVALID_CREDENTIALS") {
                this.error.message = this.authtokenerror.error.message ? this.authtokenerror.error.message : "Kindly provide valid information.";
                 this.error.showMessage = true;
            }else {
                this.modal.show();
            }
        });
    }else{
      let reqObj;
      if(this.lostcard.mobileNumber.length == "10"){
         reqObj = {
            "mobileNumber" : this.lostcard.mobileNumber,
            "requestType" : "USER"
        }
        this.lostcard.inputtype ="MOBILE";
      }else{
         reqObj = {
          "cardNumber" : this.lostcard.mobileNumber,
          "requestType" : "CARD"
        }
        this.lostcard.inputtype = "CARD";
      }

       this.error.showMessage = false;
      this.customerservice.apiSendOtp(reqObj, false)
      .subscribe(response =>{
              console.log("otp sent successfully");
              this.error.showMessage = false;
              this.lostcard.isotpSent = true;
              this.initForm();
      }, error => {
        this.sendotperror = error;
        if(this.sendotperror.status == 200){
          console.log("otp sent successfully");
              this.error.showMessage = false;
              this.lostcard.isotpSent = true;
              this.initForm();
        }else if(this.sendotperror.error.name == "UNAUTHORIZED" || this.sendotperror.error.name == "VALIDATION_ERROR" || this.sendotperror.error.name == "ACTIVE_USER_NOT_FOUND" || this.sendotperror.error.name == "USER_NOT_FOUND" || this.sendotperror.error.name == "USER_EXISTS" || this.sendotperror.error.name == "LINKED_USER_LIMIT_EXCEEDED" ||
        this.sendotperror.error.name == "ACTIVE_CARD_NOT_FOUND" || this.sendotperror.error.name == "CARD_NOT_FOUND"|| this.sendotperror.error.name == "CARD_INVALID_CODE" || this.sendotperror.error.name == "CARD_LINK_EXISTS" || this.sendotperror.error.name == "LINK_CARD_LIMIT_EXCEEDED"){
          this.error.message = this.sendotperror.error.message ? this.sendotperror.error.message : "Kindly provide valid information.";
          this.error.showMessage = true;
        console.log("replacecard otp" + error);
      }
      else {
        this.modal.show();
      }

      });
    }
  }
  initiateblockCard(card :object, flag){
    if(flag){

          if(this.selectedcard.cardNumber){
              this.error.showselectcardMessage = false;
              let reqObj = {
                  "cardNumber" : this.selectedcard.cardNumber
              };
              this.customerservice.apiGuestBlockcard(this.accesstoken, reqObj)
              .subscribe(response => {
                this.blockcardresponse = response;
                  this.blockCard.code =  this.blockcardresponse.code ?  this.blockcardresponse.code : "";
              },error =>{
                this.blockcarderror = error;
                  if (this.blockcarderror.error.name == "INVALID_CREDENTIALS" || this.blockcarderror.error.name == "INVALID_CREDENTIALS" ||this.blockcarderror.error.name == "CARD_ALREADY_BLOCKED") {
                    this.error.selectcardmessage = this.blockcarderror.error.message ? this.blockcarderror.error.message : "Kindly provide valid information.";
                     this.error.showselectcardMessage = true;
                  }else {
                      this.modal.show();
                  }
              });
          }else{
            this.error.selectcardmessage = "Kindly provide valid Information";
            this.error.showselectcardMessage = true;
          }

    }else{
      this.selectedcard = card;
    }
  }


  dontblock(){
    this.confomationdata = false;
     this.selectedcard = {};
  }


  ngOnInit() {
    this.initForm();

   }

}

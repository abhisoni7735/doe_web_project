import { NgModule }                     from '@angular/core';
import { FormsModule }                  from '@angular/forms';
import { CommonModule }                 from '@angular/common';
import { DirectivesModule }             from './../directives/directives.module';
import { RouterModule, Routes } from '@angular/router';
import { NgxCaptchaModule } from 'ngx-captcha';


import { GuestFooterComponent }         from './footer/footer.component';
import { GuestHeaderComponent }         from './header/header.component';
import { QuickRechargeComponent }       from './quick-recharge/quick-recharge.component';
import { ReportLostCardComponent }      from './report-lost-card/report-lost-card.component';
import { CommonModal }                  from './modal/modal.component';
import { MerchantGuestLoginComponent }  from './merchant-guest-login/merchant-guest-login.component';
import  { ConsumerJoinusFormComponent } from './consumer-joinus-form/joinus-form.component';
import { ReactiveFormsModule }               from '@angular/forms';




@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DirectivesModule,
        ReactiveFormsModule,
        RouterModule,
        NgxCaptchaModule
    ],
    declarations: [
       CommonModal,
       GuestFooterComponent,
       GuestHeaderComponent,
       QuickRechargeComponent,
       ReportLostCardComponent,
       MerchantGuestLoginComponent,
       ConsumerJoinusFormComponent
    ],
    exports: [
	    GuestFooterComponent,
      GuestHeaderComponent,
      QuickRechargeComponent,
      ReportLostCardComponent,
      CommonModal,
      MerchantGuestLoginComponent,
      ConsumerJoinusFormComponent
        ]
})
export class CommonBaseModule {
}

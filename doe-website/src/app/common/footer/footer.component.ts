import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';

@Component({
  selector: 'guest-footer',
  templateUrl: './footer.component.html'
})

export class GuestFooterComponent {
  

  	constructor(private route :Router, private activatedRoute : ActivatedRoute, private fullpageService : MnFullpageService) { 

  	}

  	ngOnInit(){
	    this.activatedRoute.queryParams
	    .subscribe(params => {
	      setTimeout(()=>{
	        let section = params.sec ? params.sec : "";
	        switch(section){
	          case "FEATURES":
	          this.fullpageService.moveTo(3,1);
	          break;
	          case "SERVICES":
	          this.fullpageService.moveTo(4,1);
	          break;
	          case "CONTACTUS":
	          this.fullpageService.moveTo(5,1);
	          break;
	          default:
	          console.log("Do nothing");
	          break;
	        }
	      },0)

	    })
  	}

  	gotoHomeMenu(type) {
  		console.log(this.route);
	  	switch(type){
	  		case 'HOME':
			  	if(this.activatedRoute.routeConfig.component.name == "HomeComponent"){
			        this.fullpageService.moveTo(1,1);
			    }else{
			    	this.route.navigate(['/home']);
			    }
	  		break;
	  		case 'FEATURES':
			  	if(this.activatedRoute.routeConfig.component.name  == "HomeComponent"){
			        this.fullpageService.moveTo(3,1);
			    }else{
			    	this.route.navigate(['/home'],{ queryParams: { sec: 'FEATURES' } });
			    }
	  		break;
	  		case 'SERVICES':
			  	if(this.activatedRoute.routeConfig.component.name == "HomeComponent"){
			        this.fullpageService.moveTo(4,1);
			    }else{
			    	this.route.navigate(['/home'],{ queryParams: { sec: 'SERVICES' } });
			    }
	  		break;
	  		case 'CONTACTUS':
			  	if(this.activatedRoute.routeConfig.component.name  == "HomeComponent"){
			        this.fullpageService.moveTo(5,1);
			    }else{
			    	this.route.navigate(['/home'],{ queryParams: { sec: 'CONTACTUS' } });
			    }
	  		break;
	  	}
  	}

}







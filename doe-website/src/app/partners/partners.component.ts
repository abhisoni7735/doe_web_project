import { Component } from '@angular/core';
import { CommonService } from "../services/common.service";

@Component({
  templateUrl: './partners.component.html'
})

export class PartnersComponent {


  content;
  selectedSection;

  constructor(private commonService: CommonService) {
    this.selectedSection = "PARTNERS"; //MEMBERS,CUSTOMERS
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
   }

  ngOnInit() { }

  changeSelection(sectionName){
    this.selectedSection = sectionName;
  }
}

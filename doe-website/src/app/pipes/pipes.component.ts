import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}


@Pipe({name: 'displayurl'})

export class Commonpipe implements PipeTransform {

      transform(value) {
        console.log(value);
        let url ="";
        let obj = value;
        if(obj && Object.keys(obj).length){
          url = obj[Object.keys(obj)[0]].url ? obj[Object.keys(obj)[0]].url : "assets/images/common/layer-6.png";
        }else{
          url = "assets/images/common/layer-6.png";
        }
        console.log(url);
        return url;
      }


}

@Pipe({name: 'displayCardNumber'})

export class DidisplayCardNumberPipe implements PipeTransform {

      transform(value) {
        console.log(value);
        let url ="";
        let obj = value;
        let cnend = value.substring(value.length-4, value.length);
        let res = "**** **** **** " + cnend;
        return res;
      }

}


@Pipe({name:'statecode'})
export class DisplayStateCodePipe implements PipeTransform {
   transform (value){
       let statecodes = [
              {
                "code": "AN",
                "value": "Andaman and Nicobar Islands"
              },
              {
                "code": "AR",
                "value": "Arunachal Pradesh"
              },
              {
                "code": "AP",
                "value": "Andhra Pradesh"
              },
              {
                "code": "AS",
                "value": "Assam"
              },
              {
                "code": "CH",
                "value": "Chandigarh"
              },
              {
                "code": "CT",
                "value": "Chhattisgarh"
              },
              {
                "code": "DN",
                "value": "Dadra and Nagar Haveli"
              },
              {
                "code": "DD",
                "value": "Daman and Diu"
              },
              {
                "code": "DL",
                "value": "Delhi"
              },
              {
                "code": "GA",
                "value": "Goa"
              },
              {
                "code": "GJ",
                "value": "Gujarat"
              },
              {
                "code": "HR",
                "value": "Haryana"
              },
              {
                "code": "HP",
                "value": "Himachal Pradesh"
              },
              {
                "code": "JK",
                "value": "Jammu and Kashmir"
              },
              {
                "code": "JH",
                "value": "Jharkhand"
              },
              {
                "code": "KA",
                "value": "Karnataka"
              },
              {
                "code": "KL",
                "value": "Kerala"
              },
              {
                "code": "LD",
                "value": "Lakshadweep"
              },
              {
                "code": "MP",
                "value": "Madhya Pradesh"
              },
              {
                "code": "MH",
                "value": "Maharashtra"
              },
              {
                "code": "MN",
                "value": "Manipur"
              },
              {
                "code": "ME",
                "value": "Meghalaya"
              },
              {
                "code": "MI",
                "value": "Mizoram"
              },
              {
                "code": "NL",
                "value": "Nagaland"
              },
              {
                "code": "OR",
                "value": "Odisha"
              },
              {
                "code": "PY",
                "value": "Puducherry"
              },
              {
                "code": "PB",
                "value": "Punjab"
              },
              {
                "code": "RJ",
                "value": "Rajasthan"
              },
              {
                "code": "SK",
                "value": "Sikkim"
              },
              {
                "code": "TN",
                "value": "Tamil Nadu"
              },
              {
                "code": "TS",
                "value": "Telangana"
              },
              {
                "code": "TR",
                "value": "Tripura"
              },
              {
                "code": "UP",
                "value": "Uttar Pradesh"
              },
              {
                "code": "UT",
                "value": "Uttarakhand"
              },
              {
                "code": "WB",
                "value": "West Bengal"
              }
            ];



            for(let i=0; i< statecodes.length;i++){
                if(statecodes[i].code == value){
                    return statecodes[i].value;
                }
            }

   }
}













@Pipe({name: 'num'})

export class NumberInput implements PipeTransform {

      transform(value) {
        console.log(value);

        return parseInt(value);
      }


}

@Pipe({name: 'orderByPipe'})

export class sortArrayPipe implements PipeTransform {
  transform(array, args){

    if(!array || array === undefined || array.length === 0){
      return null;
    } else{
        array.sort((a, b)=>{
          if (a.date < b.date) {
            return -1;
          } else if (a.date > b.date) {
            return 1;
          } else {
            return 0;
          }
        });
      return array;
    }
  }
}

@Pipe({name: "formatVehicleTypes"})
export class ReplacePipe implements PipeTransform {
    transform(value: string, expr:[string], arg2: string): any {
        if (!value)
            return value;
            let result = value.replace(new RegExp(expr[0], 'gi'), arg2);
            result = result.replace(new RegExp(expr[1], 'gi'), arg2);
         return result;
    }
}

//
//
//
//app.DidisplayCardNumberPipe = ng.core.Pipe({
//     name: "displayCardNumber"
// }).Class({
//
//     // Match class name
//     constructor: function DisplayKeystrokePipe () {},
//
//     // or simply using function() {}
//     //constructor: function() {},
//
//     transform: function(value) {
//       console.log(value);
//       var cnend = value.substring(value.length-4, value.length);
//       var res = "**** **** " + cnend;
//         return res;
//     }
// });
//
//
//   app.sortArrayPipe = ng.core.Pipe({
//     name: 'orderByPipe'
//   }).Class({
//     constructor: function DisplayKeystrokePipe() {},
//
//       transform: function(array, args){
//
//         if(!array || array === undefined || array.length === 0){
//           return null;
//         } else{
//             array.sort(function(a, b){
//               if (a.date < b.date) {
//                 return -1;
//               } else if (a.date > b.date) {
//                 return 1;
//               } else {
//                 return 0;
//               }
//             });
//           return array;
//         }
//
//
//       }
//     });
@Pipe({ name: 'keys',  pure: false })
export class KeysPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
        return Object.keys(value)//.map(key => value[key]);
    }
}

@Pipe({name: 'absolute'})
export class Absolute implements PipeTransform {
    /**
     *
     * @param value
     * @returns {number}
     */
    transform(value: number): number {
        return Math.abs(value);
    }
}

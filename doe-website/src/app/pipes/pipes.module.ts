import {NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { DirectivesModule }     from './../directives/directives.module';
import { Commonpipe, DidisplayCardNumberPipe, sortArrayPipe, NumberInput, DisplayStateCodePipe, ReplacePipe, Absolute, SafePipe, KeysPipe }     from './pipes.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule

    ],
    declarations: [
      Commonpipe,
      DidisplayCardNumberPipe,
      sortArrayPipe,
      NumberInput,
      DisplayStateCodePipe,
      ReplacePipe,
      Absolute,
      SafePipe,
      KeysPipe
    ],
    exports: [
      Commonpipe,
      DidisplayCardNumberPipe,
      sortArrayPipe,
      NumberInput,
      DisplayStateCodePipe,
      ReplacePipe,
      Absolute,
      SafePipe,
      KeysPipe
    ]
})
export class PipesModule {

}

declare var require: any;
import {Directive, ElementRef, HostListener, Input, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';



@Directive({
  selector: '[boxfocus]',
})
export class Boxfocus {

  @HostListener('click', ['$event']) onClick() {
     this.onClickFunction(window.event);
       console.log('clicked');
 }

  constructor(private elem : ElementRef, @Inject(PLATFORM_ID) private platformId: Object) {

         
  }

  onClickFunction(event){
     if (isPlatformBrowser(this.platformId)) {
         let $ = require('jquery');
          console.log(event);
          console.log(this.elem);
         let element = this.elem.nativeElement;
          //$(elem).focus();
          let inputelement = $(element).find('input');
          console.log(inputelement);
          $(inputelement).focus();
     }
    
  }

}

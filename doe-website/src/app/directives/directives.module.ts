import { NgModule }               from '@angular/core';
import { FormsModule }            from '@angular/forms';


import { SideMenu }               from "./side-menu.directive";
import { CommonSideMenu }         from "./common-side-menu.directive";
import { DisableControlDirective } from "./disbleControll.directive";
import { Tollredirection }         from "./tollredirection.directive";
import { NumberField }             from "./onlynumber.directive";
import { FocusNext }               from "./focusNextField.directive";
import { EmmitCustomEvent }        from "./triggerCustomEvent.directive";
import { Boxfocus }                from "./boxfocus.directive";
import { InputNumber }             from "./inputnumber.directive";
import { AutofocusDirective }      from "./autofocus.directive";
import { RetailSideMenu }          from "./retail-side-menu.directive";
import { MetroSideMenu }           from "./metro-side-menu.directive";
import { BusSideMenu }             from "./bus-side-menu.directive";
import { ParkingSideMenu }         from "./parking-side-menu.directive";
import { EducationSideMenu }       from "./education-side-menu.directive";
import { ResidenceSideMenu }       from "./residence-side-menu.directive";
import { OfficeSideMenu }          from "./office-side-menu.directive";



@NgModule({
    imports: [
        FormsModule
    ],
    declarations: [
       FocusNext,
      AutofocusDirective,
       SideMenu,
       CommonSideMenu,
       DisableControlDirective,
       Tollredirection,
       NumberField,
       EmmitCustomEvent,
       Boxfocus,
       InputNumber,
       RetailSideMenu,
       MetroSideMenu,
       BusSideMenu,
       ParkingSideMenu,
       EducationSideMenu,
       ResidenceSideMenu,
       OfficeSideMenu
         ],
    exports: [
      FocusNext,
      AutofocusDirective,
      SideMenu,
      CommonSideMenu,
      DisableControlDirective,
      Tollredirection,
      NumberField,
      EmmitCustomEvent,
      Boxfocus,
      InputNumber,
      RetailSideMenu,
      MetroSideMenu,
      BusSideMenu,
      ParkingSideMenu,
      EducationSideMenu,
      ResidenceSideMenu,
      OfficeSideMenu
      ]
})
export class DirectivesModule {
}

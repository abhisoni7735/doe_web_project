import {Directive, ElementRef, HostListener, Input, Output, Inject, PLATFORM_ID} from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';



declare var require: any;

import * as $ from 'jquery';

@Directive({
  selector: '[RetailSideMenu]'
})
export class RetailSideMenu {
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private route :Router, private activatedRoute : ActivatedRoute, private fullpageService : MnFullpageService){

  }

  @Input('type') type : string;


  @HostListener('click', ['$event']) onClick($event) {
   //this.highlight(this.highlightColor || 'red');
     this.onClickFunction($event);
       console.log('clicked');
 }

  private onClickFunction(event) {
    if (isPlatformBrowser(this.platformId)) {
       //Client only code.
      let $ = require('jquery');
      let scrollify = require('jquery-scrollify');


      event.stopPropagation();
      // this.fullpageService.destroy('all');

      switch(this.type){

        case 'F&B':
        console.log("here------->retail");
          document.getElementById("food").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'H&B':
        console.log("here------->");
          document.getElementById("health").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'retailgoods':
        console.log("here------->");
          document.getElementById("retailgoods").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'servicerepairs':
        console.log("here------->");
          document.getElementById("servicerepairs").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'others':
        console.log("here------->");
          document.getElementById("others").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'interstatetoll':
          console.log("here------->");
          document.getElementById("interstatetoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

         case 'retail-restuarent':
           console.log("here------->");
           document.getElementById("retail-restuarent").style.width = "40%";
             $("#overlay").removeClass('hide');
             $('.right-overlay-close').removeClass('hide');
             $('.left-overlay-close').addClass('hide');
             document.getElementById("right-o-close").style.right= "42%";
          break;
          case 'retail-fuelstation':
            console.log("here------->");
            document.getElementById("retail-fuelstation").style.width = "40%";
              $("#overlay").removeClass('hide');
              $('.right-overlay-close').removeClass('hide');
              $('.left-overlay-close').addClass('hide');
              document.getElementById("right-o-close").style.right= "42%";
           break;
           case 'retail-taxi':
             console.log("here------->");
             document.getElementById("retail-taxi").style.width = "40%";
               $("#overlay").removeClass('hide');
               $('.right-overlay-close').removeClass('hide');
               $('.left-overlay-close').addClass('hide');
               document.getElementById("right-o-close").style.right= "42%";
            break;
            case 'retail-supermarket':
              console.log("here------->");
              document.getElementById("retail-supermarket").style.width = "40%";
                $("#overlay").removeClass('hide');
                $('.right-overlay-close').removeClass('hide');
                $('.left-overlay-close').addClass('hide');
                document.getElementById("right-o-close").style.right= "42%";
             break;
             case 'retail-vendingmachine':
               console.log("here------->");
               document.getElementById("retail-vendingmachine").style.width = "40%";
                 $("#overlay").removeClass('hide');
                 $('.right-overlay-close').removeClass('hide');
                 $('.left-overlay-close').addClass('hide');
                 document.getElementById("right-o-close").style.right= "42%";
              break;
              case 'retail-smallshop':
                console.log("here------->");
                document.getElementById("retail-smallshop").style.width = "40%";
                  $("#overlay").removeClass('hide');
                  $('.right-overlay-close').removeClass('hide');
                  $('.left-overlay-close').addClass('hide');
                  document.getElementById("right-o-close").style.right= "42%";
               break;

      }
    }

  }

}

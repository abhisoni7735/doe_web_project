
import {Directive, ElementRef, HostListener, Input, EventEmitter} from '@angular/core';

declare var require: any;

@Directive({
  selector: '[inputnumber]'
})
export class InputNumber {

    constructor(private el: ElementRef) {

    }

  @HostListener('input', ['$event']) onInput(event: Event) {

      let curval = (<HTMLInputElement>event.currentTarget).value;
      let val = (<HTMLInputElement>event.currentTarget).value.replace(/[^0-9\.]/g, '');
      if (val.charAt(0) === '.') {
          val = val.slice(1);
      }    

      let pos = val.indexOf(".") + 1;
      if(pos >= 0){
        val = val.substr(0, pos) + val.slice(pos).replace('.', '');
      }

      this.el.nativeElement.value = val;
         

                //remove all but number
                //var fixed = this.value.replace(/[^0-9]/g, ''); //Decimal value not allowed
                /*var fixed = this.value.replace(/[^0-9\.]/g, ''); // allowed Decimal value
                if (fixed.charAt(0) === '.')                  //can't start with .
                    fixed = fixed.slice(1);

                var pos = fixed.indexOf(".") + 1;
                if (pos >= 0)               //avoid more than one .
                    fixed = fixed.substr(0, pos) + fixed.slice(pos).replace('.', '');

                if (this.value !== fixed) {
                    this.value = fixed;
                    this.selectionStart = position;
                    this.selectionEnd = position;
                }*/
      
   }


}

import {Directive, ElementRef, HostListener, Input, Output, Inject, PLATFORM_ID} from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';



declare var require: any;

import * as $ from 'jquery';

@Directive({
  selector: '[CommonSideMenu]'
})
export class CommonSideMenu {
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private route :Router, private activatedRoute : ActivatedRoute, private fullpageService : MnFullpageService){

  }

  @Input('type') type : string;


  @HostListener('click', ['$event']) onClick($event) {
   //this.highlight(this.highlightColor || 'red');
     this.onClickFunction($event);
       console.log('clicked');
 }

  private onClickFunction(event) {
     if (isPlatformBrowser(this.platformId)) {
           //Client only code.
            let $ = require('jquery');
    let scrollify = require('jquery-scrollify');


    event.stopPropagation();
    // this.fullpageService.destroy('all');

      switch(this.type){
        case 'right-menu':
        //added if conditions for parking menu
        if(document.getElementById("toll-menu")){
         document.getElementById("toll-menu").style.width = "20%";
            $("#overlay").removeClass('hide');
          }

          case 'toll-side-nav':
          //added if conditions for parking menu
          if(document.getElementById("toll-side-notification")){
           document.getElementById("toll-side-notification").style.width = "20%";
              // $("#overlay").removeClass('hide');
            }

          // if (document.getElementById("parking-menu")) {
          //   document.getElementById("parking-menu").style.width = "20%";
          //     $("#overlay").removeClass('hide');
          //  }
          if (document.getElementById("bus-menu")) {
            document.getElementById("bus-menu").style.width = "20%";
              $("#overlay").removeClass('hide');
           }
          if (document.getElementById("metro-menu")) {
            document.getElementById("metro-menu").style.width = "20%";
              $("#overlay").removeClass('hide');
           }

         //    $.scrollify.disable();
        break;

        case 'NHtoll':
        console.log("here------->");
          document.getElementById("NHtoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'SHtoll':
        console.log("here------->");
          document.getElementById("SHtoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'temples':
        console.log("here------->");
          document.getElementById("temples").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'tollfeatures':
        console.log("here------->");
          document.getElementById("tollfeatures").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'chainedtoll':
        console.log("here------->");
          document.getElementById("chainedtoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'interstatetoll':
        console.log("here------->");
          document.getElementById("interstatetoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

         // toll features ends

         case 'tollfaq':
          document.getElementById("toll-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'retail_faq':
          document.getElementById("retail-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'metro_faq':
          document.getElementById("metro-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'bus_faq':
          document.getElementById("bus-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'parking_faq':
          document.getElementById("parking-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'education_faq':
          document.getElementById("education-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'residence_faq':
          document.getElementById("residence-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;
         case 'office_faq':
          document.getElementById("office-faq").style.width = "50%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "52%";
         break;


         case 'tollreaderissues':
          document.getElementById("toll-renewel").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;



         //toll Side menu

          case 'toll-product':
          console.log(this.route.url);
          if(this.route.url == "/toll"){
           this.fullpageService.moveTo(3,1);
           $('.toll-side-menu-js').removeClass('active');
           $('.section3 .toll-side-menu-js').addClass('active');
           $('.right_side_menunav').width('0%');
           $('.tollmenunav').width('0%');
           $('.parkingmenunav').width('0%');
           $("#overlay").addClass('hide');
           $('.right-overlay-close').addClass('hide');
           $('.left-overlay-close').addClass('hide');

        }
        else{
          // this.route.navigate(["/toll"]);
          this.route.navigate(['/toll'], { queryParams: { sec: 'products'  } });
          $('.toll-side-menu-js').removeClass('active');
          $('.section3 .toll-side-menu-js').addClass('active');

        //  $.scrollify.move("#products");

          $("#overlay").addClass('hide');

        }
         break;

         case 'about-toll':

         if(this.route.url == "/toll"){
           this.fullpageService.moveTo(1,1);
           $('.toll-side-menu-js').removeClass('active');
           $('.section1 .toll-side-menu-js').addClass('active');
          $('.right_side_menunav').width('0%');
           $('.tollmenunav').width('0%');
           $('.parkingmenunav').width('0%');
           $("#overlay").addClass('hide');
           $('.right-overlay-close').addClass('hide');
           $('.left-overlay-close').addClass('hide');

        }
        else{
        //  this.route.navigate(["/toll"]);
      //    $.scrollify.move("#abouttoll");
      this.route.navigate(['/toll'], { queryParams: { sec: 'abouttoll'  } });
      $('.toll-side-menu-js').removeClass('active');
      $('.section1 .toll-side-menu-js').addClass('active');

          $("#overlay").addClass('hide');


        }
         break;

         case 'toll-features':

         if(this.route.url == "/toll"){
           this.fullpageService.moveTo(2,1);
           $('.toll-side-menu-js').removeClass('active');
           $('.section2 .toll-side-menu-js').addClass('active');
           $('.right_side_menunav').width('0%');
           $('.tollmenunav').width('0%');
           $('.parkingmenunav').width('0%');
           $("#overlay").addClass('hide');
           $('.right-overlay-close').addClass('hide');
           $('.left-overlay-close').addClass('hide');

        }
        else{
        //  this.route.navigate(["/toll"]);
        this.route.navigate(['/toll'], { queryParams: { sec: 'features'  } });
        $('.toll-side-menu-js').removeClass('active');
        $('.section2 .toll-side-menu-js').addClass('active');

        //  $.scrollify.move("#features");
          $("#overlay").addClass('hide');


        }
         break;

       case 'toll-faqs':

         if(this.route.url == "/toll"){
           this.fullpageService.moveTo(4,1);
           $('.toll-side-menu-js').removeClass('active');
           $('.section4 .toll-side-menu-js').addClass('active');
           $('.right_side_menunav').width('0%');
           $('.tollmenunav').width('0%');
           $('.parkingmenunav').width('0%');
           $("#overlay").addClass('hide');
           $('.right-overlay-close').addClass('hide');
           $('.left-overlay-close').addClass('hide');

        }
        else{
          // this.route.navigate(["/toll"]);
          // $.scrollify.move("#faq");
          // $("#overlay").addClass('hide');
          this.route.navigate(['/toll'], { queryParams: { sec: 'faq'  } });
          $('.toll-side-menu-js').removeClass('active');
          $('.section4 .toll-side-menu-js').addClass('active');

          $("#overlay").addClass('hide');


        }
         break;


         case "toll-readers":

             if(this.route.url == "/toll"){
              $.scrollify.move("#products");
              $("#overlay").addClass('hide');

            }
            else{
              // this.route.navigate(["/toll"]);
              // $.scrollify.move("#products");


              this.route.navigate(['/toll'], { queryParams: { sec: 'products'  } });
              $("#overlay").addClass('hide');


            }
         break;



     }
          }

  }

}

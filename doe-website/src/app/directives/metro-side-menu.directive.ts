import {Directive, ElementRef, HostListener, Input, Output, Inject, PLATFORM_ID} from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';



declare var require: any;

import * as $ from 'jquery';

@Directive({
  selector: '[MetroSideMenu]'
})
export class MetroSideMenu {
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private route :Router, private activatedRoute : ActivatedRoute, private fullpageService : MnFullpageService){

  }

  @Input('type') type : string;


  @HostListener('click', ['$event']) onClick($event) {
   //this.highlight(this.highlightColor || 'red');
     this.onClickFunction($event);
       console.log('clicked');
 }

  private onClickFunction(event) {
    if (isPlatformBrowser(this.platformId)) {
       //Client only code.
      let $ = require('jquery');
      let scrollify = require('jquery-scrollify');


      event.stopPropagation();
      // this.fullpageService.destroy('all');

      switch(this.type){

        case 'BMRCL':
        console.log("here------->retail");
          document.getElementById("BMRCL").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'CMRL':
        console.log("here------->");
          document.getElementById("CMRL").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'PMRDA':
        console.log("here------->");
          document.getElementById("PMRDA").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'DMRL':
        console.log("here------->");
          document.getElementById("DMRL").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'others':
        console.log("here------->");
          document.getElementById("others").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'interstatetoll':
          console.log("here------->");
          document.getElementById("interstatetoll").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

      }
    }

  }

}

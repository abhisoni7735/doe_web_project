
import {Directive, ElementRef, HostListener, Input, EventEmitter, Output} from '@angular/core';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


declare var require: any;

@Directive({
  selector: '[emmitcustomevent]'
})
export class EmmitCustomEvent {


  @Input('scrollFlag') scrollFlag : any;

     @HostListener('click', ['$event']) onInput(ev) {
       console.log(this.scrollFlag);
       switch(this.scrollFlag){
         case 'on' :
         console.log("sideMenuOn triggered---------------->");
         this.fullpageService.setAllowScrolling(false);
         this.fullpageService.setKeyboardScrolling(false);
         break;
         case 'off' :
         console.log(" sideMenuOFF triggered---------------->");
         this.fullpageService.setAllowScrolling(true);
         this.fullpageService.setKeyboardScrolling(true);
         break;
       }
     }
     constructor(private el: ElementRef, private fullpageService: MnFullpageService) {
     }

}

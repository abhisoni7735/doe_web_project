import {Directive, ElementRef, HostListener, Input, AfterViewInit,  Inject, PLATFORM_ID} from '@angular/core';
declare var require: any;
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Directive({
  selector: '[showfaqanswer]'
})
export class SideMenu implements AfterViewInit{
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (isPlatformBrowser(this.platformId)) {
      var $ = require('jquery');
      $('#page-loader').addClass('hide');
    }


  }

  @Input('type') type : string;

  @HostListener('click', ['$event']) onClick(event) {
   //this.highlight(this.highlightColor || 'red');
      this.onClickFunction(event);
       console.log('clicked');
 }

 @HostListener('reset') onReset(event) {
    this.onClickFunction(event);
  }


 ngAfterViewInit() {

 }

  private onClickFunction(event) {

    if (isPlatformBrowser(this.platformId)) {
      var $ = require('jquery');
    var scrollify = require('jquery-scrollify');

    event.stopPropagation();
      switch(this.type){
        case 'quickPayment':
         document.getElementById("quickPayment").style.width = "40%";
           $("#overlay").removeClass('hide');
           $('.right-overlay-close').removeClass('hide');
           $('.left-overlay-close').addClass('hide');
           document.getElementById("right-o-close").style.right= "42%";
        break;
        case 'loyalty':
         document.getElementById("loyalty").style.width = "40%";
           $("#overlay").removeClass('hide');
           $('.right-overlay-close').removeClass('hide');
           $('.left-overlay-close').addClass('hide');
           document.getElementById("right-o-close").style.right= "42%";
        break;
        case 'access':
         document.getElementById("access").style.width = "40%";
           $("#overlay").removeClass('hide');
           $('.right-overlay-close').removeClass('hide');
           $('.left-overlay-close').addClass('hide');
           document.getElementById("right-o-close").style.right= "42%";
        break;
        case 'cust-toll':
         document.getElementById("toll").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-metro':
         document.getElementById("metro").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-retail':
         document.getElementById("retail").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-parking':
         document.getElementById("parking").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-office':
         document.getElementById("office").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-bus':
         document.getElementById("bus").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-residency':
         document.getElementById("residency").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
        case 'cust-education':
         document.getElementById("education").style.width = "40%";
         $("#overlay").removeClass('hide');
         $('.right-overlay-close').removeClass('hide');
         $('.left-overlay-close').addClass('hide');
         document.getElementById("right-o-close").style.right= "42%";
         break;
         case 'cust-railways':
          document.getElementById("railways").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-residence':
          document.getElementById("access-residence").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-offices':
          document.getElementById("access-office").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-education':
          document.getElementById("access-education").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-hotels':
          document.getElementById("access-hotels").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-gated-community':
          document.getElementById("access-gated-community").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
         case 'cust-access-hospital':
          document.getElementById("access-hospitals").style.width = "40%";
          $("#overlay").removeClass('hide');
          $('.right-overlay-close').removeClass('hide');
          $('.left-overlay-close').addClass('hide');
          document.getElementById("right-o-close").style.right= "42%";
          break;
          case 'cust-loyalty-airlines':
           document.getElementById("loyalty-airlines").style.width = "40%";
           $("#overlay").removeClass('hide');
           $('.right-overlay-close').removeClass('hide');
           $('.left-overlay-close').addClass('hide');
           document.getElementById("right-o-close").style.right= "42%";
           break;
           case 'cust-loyalty-clubs':
            document.getElementById("loyalty-clubs").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
            break;
            case 'cust-loyalty-restuarent':
             document.getElementById("loyalty-restuarent").style.width = "40%";
             $("#overlay").removeClass('hide');
             $('.right-overlay-close').removeClass('hide');
             $('.left-overlay-close').addClass('hide');
             document.getElementById("right-o-close").style.right= "42%";
             break;
             case 'cust-loyalty-petrocards':
              document.getElementById("loyalty-petrocards").style.width = "40%";
              $("#overlay").removeClass('hide');
              $('.right-overlay-close').removeClass('hide');
              $('.left-overlay-close').addClass('hide');
              document.getElementById("right-o-close").style.right= "42%";
              break;
        case 'menu_back_btn':
           $('.right_side_menunav').width('0%');
           $('.tollmenunav').width('0%');
           $('.parkingmenunav').width('0%');
           $("#overlay").addClass('hide');
           $('.right-overlay-close').addClass('hide');
           $('.left-overlay-close').addClass('hide');
         break;
        case 'right-close':
             if(document.getElementById("guest-lost-card") || document.getElementById("guest-qk-recharge")){
               console.log("guest  card");
               $("#guest-lost-card").trigger("click");
               $("#guest-qk-recharge").trigger("click");
               document.getElementById("main").style.marginLeft = "0";
               document.getElementById("mySidenav").style.width = "0";
             }
            $('.sidenav').css('width','0%');
            $("#overlay").addClass('hide');
            $('.right-overlay-close').addClass('hide');
            $('.left-overlay-close').addClass('hide');
         break;
         case 'quickrecharge':
            document.getElementById("mySidenav").style.width = "40%";
            document.getElementById("main").style.marginLeft = "40%";
             //document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
              $("#overlay").removeClass('hide');
              $('.right-overlay-close').addClass('hide');
              $('.left-overlay-close').removeClass('hide');
              document.getElementById("left-o-close").style.left= "42%";
              $.scrollify.disable();
         break;
          case 'lostcard':
            document.getElementById("myRightSidenav").style.width = "40%";
              $("#overlay").removeClass('hide');
              $('.right-overlay-close').removeClass('hide');
              $('.left-overlay-close').addClass('hide');
              document.getElementById("right-o-close").style.right= "42%";
              $.scrollify.disable();
            break;
            case 'parking-side-notification':
            //added if conditions for parking menu
            if(document.getElementById("parking-notification")){
             document.getElementById("parking-notification").style.width = "25%";
                 $("#overlay").removeClass('hide');
              }
              $.scrollify.disable();
            break;

            case 'toll-notification-menu':
            //added if conditions for parking menu
            if(document.getElementById("toll-side-notification")){
             document.getElementById("toll-side-notification").style.width = "25%";
                //$("#overlay").removeClass('hide');
              }
             //    $.scrollify.disable();
            break;
            case 'about-toll':
                  console.log("indise the case --------->");
                  break;
            case 'menu':
                  //added if conditions for parking menu
                  if(document.getElementById("toll-menu")){
                   document.getElementById("toll-menu").style.width = "20%";
                      $("#overlay").removeClass('hide');
                    }
                    if (document.getElementById("parking-menu")) {
                      document.getElementById("parking-menu").style.width = "20%";
                        $("#overlay").removeClass('hide');
                     }
                    if (document.getElementById("retail-menu")) {
                      document.getElementById("retail-menu").style.width = "20%";
                        $("#overlay").removeClass('hide');
                     }
                   //    $.scrollify.disable();
              break;
              case 'parkingReaderIssues':
               document.getElementById("parking-renewel").style.width = "40%";
                 $("#overlay").removeClass('hide');
                 $('.right-overlay-close').removeClass('hide');
                 $('.left-overlay-close').addClass('hide');
                 document.getElementById("right-o-close").style.right= "42%";

              break;
              case 'retailReaderIssues':
               document.getElementById("retail-renewel").style.width = "40%";
                 $("#overlay").removeClass('hide');
                 $('.right-overlay-close').removeClass('hide');
                 $('.left-overlay-close').addClass('hide');
                 document.getElementById("right-o-close").style.right= "42%";

              break;
              case 'busReaderIssues':
              document.getElementById("parking-renewel").style.width = "40%";
                $("#overlay").removeClass('hide');
                $('.right-overlay-close').removeClass('hide');
                $('.left-overlay-close').addClass('hide');
                document.getElementById("right-o-close").style.right= "42%";
             break;
             case 'BusFeaturesSideBar':
             document.getElementById("retail-features").style.width = "40%";
               $("#overlay").removeClass('hide');
               $('.right-overlay-close').removeClass('hide');
               $('.left-overlay-close').addClass('hide');
               document.getElementById("right-o-close").style.right= "42%";
            break;
            case 'BusFaq':
            document.getElementById("guset-faq").style.width = "40%";
              $("#overlay").removeClass('hide');
              $('.right-overlay-close').removeClass('hide');
              $('.left-overlay-close').addClass('hide');
              document.getElementById("right-o-close").style.right= "42%";
           break;
           case 'video':
           $('#video-gallery').lightGallery({
                videojs: true
            });
            break;
     }
    }


  }

}

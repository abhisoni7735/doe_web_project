import {Directive, ElementRef, HostListener, Input, Output, Inject, PLATFORM_ID} from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';



declare var require: any;

import * as $ from 'jquery';

@Directive({
  selector: '[EducationSideMenu]'
})
export class EducationSideMenu {
  constructor(@Inject(PLATFORM_ID) private platformId: Object,private route :Router, private activatedRoute : ActivatedRoute, private fullpageService : MnFullpageService){

  }

  @Input('type') type : string;


  @HostListener('click', ['$event']) onClick($event) {
   //this.highlight(this.highlightColor || 'red');
     this.onClickFunction($event);
       console.log('clicked');
 }

  private onClickFunction(event) {
    if (isPlatformBrowser(this.platformId)) {
       //Client only code.
      let $ = require('jquery');
      let scrollify = require('jquery-scrollify');


      event.stopPropagation();
      // this.fullpageService.destroy('all');

      switch(this.type){

        case 'school':
        console.log("here------->retail");
          document.getElementById("school").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'college':
        console.log("here------->");
          document.getElementById("college").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'institution':
        console.log("here------->");
          document.getElementById("institution").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'universites':
        console.log("here------->");
          document.getElementById("universites").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

        case 'others':
        console.log("here------->");
          document.getElementById("others").style.width = "40%";
            $("#overlay").removeClass('hide');
            $('.right-overlay-close').removeClass('hide');
            $('.left-overlay-close').addClass('hide');
            document.getElementById("right-o-close").style.right= "42%";
         break;

      }
    }

  }

}

import { Component } from '@angular/core';
import { CommonService } from "../services/common.service";


@Component({
  templateUrl: './contactus.html'
})

export class ContactusComponent {


content:any;


  constructor(private commonService: CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
   }

  ngOnInit() { }
}

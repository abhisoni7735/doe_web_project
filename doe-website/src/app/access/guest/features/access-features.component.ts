import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'access-features',
  templateUrl: './access-features.component.html'

})

export class AccessFeaturesComponent {

  content:any;
  constructor(private commonService : CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

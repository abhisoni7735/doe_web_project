import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';


@Component({
  selector: 'access-footer',
  templateUrl: './access-footer.component.html',

})

export class AccessFooterComponent {
  tollmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoAccessMenu(type){
      switch(type){
        case 'ABOUTACCESS':
          if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/access']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/access'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/access'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/access'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

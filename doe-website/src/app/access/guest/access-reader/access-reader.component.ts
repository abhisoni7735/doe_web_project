import { Component, ViewChild, HostListener,  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonModal } from "../../../common/modal/modal.component";
import { CommonService } from "../../../services/common.service";
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, AbstractControl } from "@angular/forms";

declare var require: any;

@Component({

  templateUrl: './access-reader.component.html'

})

export class AccessReaderComponent {
  device:any;
  id: any;
  deviceinfo: any;
  routeparms :any;
  deviceImages : any;


  @ViewChild(CommonModal)
  private modal : CommonModal;



  constructor(private fullpageService: MnFullpageService,private fb: FormBuilder, private activatedroute: ActivatedRoute, private route : Router, private commonService : CommonService) {
    this.device = {
      "name" : "",
      "images" : []
    };
    this.deviceImages = {};
    this.deviceImages.showImageUrl = "";
    this.initialiseState();
    // this.activateroute.params.subscribe(params => {
    //   this.initialiseState(); // reset and set based on new parameter this time
    // });
  }



  initialiseState(){
    this.routeparms = this.activatedroute.snapshot.params['id'];
    this.id = this.routeparms ? this.routeparms : "";
    this.getAccessDevices();
    console.log("Store info here---------------------------->");
  }

  gotoAccess(page){
    switch(page){
      case 'ABOUT':
        if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
            this.fullpageService.moveTo(1,1);
        }else{
          this.route.navigate(['/access']);
        }
      break;
      case 'FEATURES':
        if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
            this.fullpageService.moveTo(2,1);
        }else{
          this.route.navigate(['/access'],{ queryParams: { sec: 'FEATURES' }});
        }
      break;
      case 'PRODUCTS':
        if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
            this.fullpageService.moveTo(3,1);
        }else{
          this.route.navigate(['/access'],{ queryParams: { sec: 'PRODUCTS' }});
        }
      break;
      case 'FAQ':
        if(this.activatedroute.routeConfig.component.name == "AccessComponent"){
            this.fullpageService.moveTo(4,1);
        }else{
          this.route.navigate(['/access'],{ queryParams: { sec: 'FAQ' }});
        }
      break;
    }
  }

   getAccessDevices(){
    this.commonService.apiGetDevicesfromJson()
    .subscribe(
      response => {
        this.deviceinfo = response;
        this.device = this.deviceinfo[this.id];
        this.deviceImages.showImageUrl = this.device.images[0].originalImage;
        console.log("device info");
        console.log(this.device);
      },
      error =>{
        this.modal.show();
        console.log(error);
    });
  }

  changeImage(index){
    console.log("index === > " + index);
    this.deviceImages.showImageUrl = this.device.images[index].originalImage;
  }



  ngOnInit() {

  }



}

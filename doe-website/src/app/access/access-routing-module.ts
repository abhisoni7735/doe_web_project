import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { AccessComponent }   		      from './guest/access.component';
import { AccessReaderComponent }   		from './guest/access-reader/access-reader.component';


const routes: Routes = [
  { path: '',                    component: AccessComponent },
  { path: 'reader/:id',          component: AccessReaderComponent },


];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class AccessRoutingModule {}

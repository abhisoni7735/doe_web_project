import { NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { DirectivesModule }     from './../directives/directives.module';
import { MerchantModule     }         from './../merchant/merchant.module';
import { CommonBaseModule }     from './../common/common.module';

import { CommonModal }            from "./../common/modal/modal.component";


import {AccessComponent}                  from './guest/access.component';
import {AccessFeaturesComponent}          from './guest/features/access-features.component';
import {AccessReaderComponent}            from './guest/access-reader/access-reader.component';
import {AccessFooterComponent}            from './guest/access-footer/access-footer.component';


import {AccessRoutingModule}               from './access-routing-module';
import {PipesModule}                     from '../pipes/pipes.module';
import { ReactiveFormsModule }               from '@angular/forms';

import { MnFullpageModule } from "ngx-fullpage";




@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        AccessRoutingModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        ReactiveFormsModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      AccessComponent,
      AccessFeaturesComponent,
      AccessReaderComponent,
      AccessFooterComponent
    ],
    exports: [
    ]
})
export class AccessModule {

}

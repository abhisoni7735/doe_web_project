import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'office-footer',
  templateUrl: './office-footer.component.html',

})

export class OfficeFooterComponent {
  Retailmenu: any;

  constructor(private fullpageService: MnFullpageService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoOfficeMenu(type){
      switch(type){
        case 'ABOUTOFFICE':
          if(this.activatedroute.routeConfig.component.name == "OfficeGuestComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/office']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "OfficeGuestComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/office'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "OfficeGuestComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/office'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "OfficeGuestComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/office'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

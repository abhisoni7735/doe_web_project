import { NgModule }                            from '@angular/core';
import { RouterModule, Routes }                from '@angular/router';

import { OfficeGuestComponent}               from './guest/office.component';
import { OfficeReaderComponent }             from './guest/office-reader/office-reader.component';
import { OfficeMerchantComponent }           from './guest/office-merchant/office-merchant.component';



const routes: Routes = [
  { path: '',                   component: OfficeGuestComponent },
  { path: 'merchant',           component: OfficeMerchantComponent },
  { path: 'reader/:id',         component:OfficeReaderComponent}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class OfficeRoutingModule {}

import { NgModule } 					             from '@angular/core';
import { FormsModule }    				         from '@angular/forms';
import { CommonModule } 				           from '@angular/common';
import { DirectivesModule }                from '../directives/directives.module';
import { MnFullpageModule }                from "ngx-fullpage";


import { CommonBaseModule } 				       from '../common/common.module';
import { MerchantModule }                  from '../merchant/merchant.module';
import {PipesModule}                       from '../pipes/pipes.module';

import { OfficeRoutingModule } 			       from './office-routing-module';

//guest components
import { OfficeGuestComponent }             from './guest/office.component';
import { OfficeReaderComponent }            from './guest/office-reader/office-reader.component';

import { OfficeFeaturesSideBarComponent }   from './guest/office-features-side-bar/office-features-side-bar.component';
import { OfficeMerchantComponent }          from './guest/office-merchant/office-merchant.component';
import { OfficeFooterComponent }            from './guest/office-footer/office-footer.component';



@NgModule({
    imports: [
    	  CommonModule,
        OfficeRoutingModule,
        DirectivesModule,
        FormsModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        MnFullpageModule.forRoot()

    ],
    declarations: [
      //guest
      OfficeGuestComponent,
      OfficeReaderComponent,
      OfficeMerchantComponent,
      OfficeFeaturesSideBarComponent,
      OfficeFooterComponent
    ],
    exports: []
})
export class OfficeModule {
}

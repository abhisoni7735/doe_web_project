import { Component } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { TollService } from "../../../services/toll.service";


@Component({
  selector: 'toll-footer',
  templateUrl: './toll-footer.component.html',

})

export class TollFooterComponent {
  tollmenu: any;

  constructor(private fullpageService: MnFullpageService,private tollService :TollService, private route : Router, private activatedroute: ActivatedRoute) {

    }

    gotoTollMenu(type){
      switch(type){
        case 'ABOUTTOLL':
          if(this.activatedroute.routeConfig.component.name == "TollComponent"){
              this.fullpageService.moveTo(1,1);
          }else{
            this.route.navigate(['/toll']);
          }
        break;
        case 'FEATURES':
          if(this.activatedroute.routeConfig.component.name == "TollComponent"){
              this.fullpageService.moveTo(2,1);
          }else{
            this.route.navigate(['/toll'],{ queryParams: { sec: 'FEATURES' }});
          }
        break;
        case 'PRODUCTS':
          if(this.activatedroute.routeConfig.component.name == "TollComponent"){
              this.fullpageService.moveTo(3,1);
          }else{
            this.route.navigate(['/toll'],{ queryParams: { sec: 'PRODUCTS' }});
          }
        break;
        case 'FAQ':
          if(this.activatedroute.routeConfig.component.name == "TollComponent"){
              this.fullpageService.moveTo(4,1);
          }else{
            this.route.navigate(['/toll'],{ queryParams: { sec: 'FAQ' }});
          }
        break;
      }
    }




  ngOnInit() { }

}

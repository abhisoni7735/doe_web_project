import { Component } from '@angular/core';
import { CommonService } from "../../../services/common.service";


@Component({
  selector: 'toll-features',
  templateUrl: './toll-features.component.html'

})

export class TollFeaturesComponent {

  content:any;
  constructor(private commonService : CommonService) {
    this.commonService.getStaticContent()
    .subscribe(
      response =>{
        this.content = response;
      },
      error => {
        console.log(error);
      });
  }


  ngOnInit() { }

}

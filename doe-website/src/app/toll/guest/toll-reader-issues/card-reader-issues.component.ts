import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { TollService } from "./../../../services/toll.service";

import { Base64Service } from './../../../services/base64.service';

@Component({
  selector: 'toll-reader-issues',
  templateUrl: './card-reader-issues.component.html'

})

export class TollReaderIssuesComponent {

  form : FormGroup;
  branchid : any;
  serverError: any;
  sub: any;
  ticketNumber: any;

  formErrors = {
  'companyName' : '',
  'branchName' : '',
  'address' : '',
  'contactPersonName' : '',
  'contactPersonMobileNumber' : '',
  'readerName' : '',
  'productId' : '',
  'description' : ''
  }

  validationMessages = {
    'companyName' : {
      required: 'Company Name is required',
      pattern: 'enter valid Company Name'
    },
    'branchName' : {
      required: 'Branch Name is required',
      pattern: 'enter valid Branch Name'
    },
    'address' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    },
    'contactPersonName' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    },
    'contactPersonMobileNumber' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    },
    'readerName' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    },
    'productId' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    },
    'description' : {
      required: 'readeNumber is required',
      pattern: 'enter valid readeNumber'
    }
  }

  buildForm() {
    this.form = this.fb.group({
      'companyName': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,50}')]],
      'branchName': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,50}')]],
      'address': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,500}')]],
      'contactPersonName': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,50}')]],
      'contactPersonMobileNumber': ['', [Validators.required, Validators.pattern('[0-9]{8,20}')]],
      'readerName': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,32}')]],
      'productId': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,32}')]],
      'description': ['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]{1,500}')]],
    })
    this.form.valueChanges.subscribe(data => this.validateForm());
    this.validateForm();
  }

  validateForm() {
    console.log('validate form reached')
    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let input = this.form.get(field);
      console.log(' let input = this.form.get(field);')
      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
          console.log('error assigned')
        }
      }
    }
  }

  validateFormOnSubmit() {
    console.log('validate form reached')
    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let input = this.form.get(field);
      console.log(' let input = this.form.get(field);')
      if (input.invalid) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
          console.log('error assigned')
        }
      }
    }
  }

  constructor(private base64Service: Base64Service, private fb : FormBuilder, private activatedroute: ActivatedRoute, private tollService : TollService){
  }

  fetchValues(){
    if(this.form.valid){
    let rqObj = {
      "category": "SUPPORT",
      "subCategory": "READER_ISSUE",
      "subject": "READER=" +  this.form.value.productId + " & READER NAME="+ this.form.value.readerName,
      "description": this.form.value.description,
      "type": "ISSUE",
      "source": "WEBSITE",
      "email": "support@doecards.com",
      "reportedByOnBehalf": this.form.value.contactPersonName
    };
    console.log(rqObj);
    this.createTicket(rqObj);
  }else{
    console.log("INVALID FORM", this.form);
    this.validateFormOnSubmit();
  }
  }

  createTicket(rqObj){
    this.tollService.apiCreateTicket(rqObj)
    .subscribe(response=>{
        console.log(response);
        this.ticketNumber = response['id'];
        this.serverError = false;
    },error =>{
      console.log( error.message);
      this.serverError = error.error.message;
    });
  }
  ngOnInit() {
  this.buildForm();
}
}

import { Component } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { MnFullpageOptions, MnFullpageService } from 'ngx-fullpage';
import { TollService } from "../../../services/toll.service";



@Component({
  selector: 'toll-menu',
  templateUrl: './toll-side-menu.component.html',

})

export class TollSideMenuComponent {
  tollmenu: any;

  constructor(private tollService :TollService, private route : Router) {
  		this.tollmenu = {};
  		this.tollmenu.cartinfo = {};
	    this.tollmenu.userinfo = this.tollService.getStoreData("TOLL");
	    console.log("accesstoken---------------------->");
	    console.log(this.tollmenu.userinfo);
    }

  tollLogout(){
  	this.tollmenu.userinfo = {
        "accesstoken" : "",
        "refreshtoken" : "",
        "cartid" : "",
        "usertype" : ""
    };
    this.tollService.setStoreData(this.tollmenu.userinfo, "TOLL");
    this.route.navigate(['/toll/login']);
  }


  ngOnInit() { }

}

import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { TollComponent }   		      from './guest/toll.component';
import { BeMerchantComponent }   		from './guest/be-merchant/be-merchant.component';
import { TollReaderComponent }   		from './guest/toll-reader/toll-reader.component';


const routes: Routes = [
  { path: '',                    component: TollComponent },
  { path: 'be-merchant',         component: BeMerchantComponent },
  { path: 'reader/:id',          component: TollReaderComponent },


];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class TollRoutingModule {}

import { NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { DirectivesModule }     from './../directives/directives.module';
import { MerchantModule     }         from './../merchant/merchant.module';
import { CommonBaseModule }     from './../common/common.module';

import { CommonModal }            from "./../common/modal/modal.component";


import {TollComponent}                  from './guest/toll.component';
import {TollSideMenuComponent}          from './guest/toll-side-menu/toll-side-menu.component';
import {BeMerchantComponent}            from './guest/be-merchant/be-merchant.component';
import {TollFeaturesComponent}          from './guest/features/toll-features.component';
import {TollReaderIssuesComponent}      from './guest/toll-reader-issues/card-reader-issues.component';
import {TollReaderComponent}            from './guest/toll-reader/toll-reader.component';
import {TollFooterComponent}            from './guest/toll-footer/toll-footer.component';


import {TollRoutingModule}               from './toll-routing-module';
import {PipesModule}                     from '../pipes/pipes.module';
import { ReactiveFormsModule }               from '@angular/forms';

import { MnFullpageModule } from "ngx-fullpage";




@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        DirectivesModule,
        TollRoutingModule,
        MerchantModule,
        PipesModule,
        CommonBaseModule,
        ReactiveFormsModule,
        MnFullpageModule.forRoot()
    ],
    declarations: [
      TollComponent,
      TollSideMenuComponent,
      BeMerchantComponent,
      TollFeaturesComponent,
      TollReaderIssuesComponent,
      TollReaderComponent,
      TollFooterComponent
    ],
    exports: [
    ]
})
export class TollModule {

}

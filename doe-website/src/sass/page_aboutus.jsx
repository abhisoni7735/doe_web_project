import React, { Component } from "react";
import Header from "./header.jsx";
import { connect } from 'react-redux';
import Carousel from 'react-bootstrap/Carousel';

class Aboutus extends Component {



  render(){
    return(


          <div className="aboutsus__page">
              <Header/>
            <div className="page_layout__container">
              <div className="aboutus_bgimage">
                <img src="./../assets/point-of-sale-systems.png" alt="point-of-sale-systems"/>
                <h3>Doe Cards Solutions Pvt.Ltd</h3>
              </div>
              <div className="aboutus__content">
                <div className="skwed">
                  About Us
                </div>
                <div className="aboutus__content__image">
                  <img className="" src="./../assets/doe-card-bg.png" alt="doe-card-bg"/>
                    <div className="aboutus__content__text">
                      <h4>Ipsum in aspernatur ut possim</h4>
                      <p>
                        Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea.
                        Quas molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                         Numquam mollitia recusandae non ad at et Ad vitae recusandae odit possimus.
                         Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                         Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum. Ad vitae recusandae odit possimus.
                         Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                        Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                      </p>
                      <p>
                        Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti.
                        Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                       Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                      </p>
                    </div>
                </div>
              </div>
              <div className="aboutus__scroll">
                <h3>Our Projects</h3>
                <div className="aboutus__scroll_section">
                  <Carousel controls={false}>
                    <Carousel.Item>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <img src="./../assets/pos.png" alt="corusel-img"/>
                        </div>
                        <h1> Toll POS ......</h1>
                          <p>
                            Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea.
                            Quas molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                             Numquam mollitia recusandae non ad at et Ad vitae recusandae odit possimus.
                             Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                             Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum. Ad vitae recusandae odit possimus.
                             Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                            Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                          </p>
                          <p>
                            Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti.
                            Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                           Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                          </p>
                      </div>
                    </Carousel.Item>
                    <Carousel.Item>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <img src="./../assets/pos.png" alt="corusel-img"/>
                        </div>
                        <h1> - Easy way to pay</h1>
                        <p>
                          Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea.
                          Quas molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                           Numquam mollitia recusandae non ad at et Ad vitae recusandae odit possimus.
                           Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                           Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum. Ad vitae recusandae odit possimus.
                           Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                          Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                        </p>
                        <p>
                          Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti.
                          Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                         Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                        </p>
                      </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div className="corusel_content">
                          <div className="corusel_image_slider">
                            <img src="./../assets/pos.png" alt="corusel-img"/>
                          </div>
                          <h1> - Easy way to pay</h1>
                          <p>
                            Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea.
                            Quas molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe.
                             Numquam mollitia recusandae non ad at et Ad vitae recusandae odit possimus.
                             Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                             Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum. Ad vitae recusandae odit possimus.
                             Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                            Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                          </p>
                          <p>
                            Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti.
                            Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                           Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                          </p>
                        </div>
                    </Carousel.Item>
                  </Carousel>
                </div>
              </div>
              <div className="our_team_section">
                <h3>Our Team</h3>
                <p className="our_team_contnet">
                  Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti.
                  Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis.
                 Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
                </p>
                <div className="our_team__scroll">
                  <Carousel controls={false}>
                    <Carousel.Item>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Carousel.Item>

                    <Carousel.Item>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Carousel.Item>

                    <Carousel.Item>
                      <div className="corusel_content">
                        <div className="corusel_image_slider">
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                          <div className="team_member_profile">
                            <div className="team__corusel_image">
                              <div className="team_member_image">
                                <img src="./../assets/dummy-image.jpg" />
                              </div>
                            </div>
                            <div className="member_profile_details">
                              <h4>XXXXXXXX XXXXX</h4>
                              <p>XXXXXXXX XXXXX</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Carousel.Item>



                  </Carousel>
                </div>
              </div>

            </div>
          </div>


    )
  }
}


export default Aboutus;

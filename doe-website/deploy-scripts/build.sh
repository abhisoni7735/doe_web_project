-#!/bin/sh
ssh ubuntu@3.6.42.92 <<EOF
 cd /home/ubuntu/doe-website
 git pull origin doe-launch
 node -v
 sudo npm install
 npm run build:ssr
 pm2 restart app
 exit
EOF

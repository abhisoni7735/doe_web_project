-#!/bin/sh
ssh ubuntu@35.154.100.216 <<EOF
 cd /home/ubuntu/doe-website
 git pull origin doe-launch
 node -v
 node node_modules/@angular/cli/bin/ng build --prod --aot=false
 exit
EOF
